<?php 
return [
    'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="active page-item"><a class="page-link" href="#">{{text}}</a></li>',
    'nextActive' => '<li class="page-item"><a class="page-link" aria-label="Next" href="{{url}}">{{text}}</a></li>',
    'nextDisabled' => '<li class="next disabled page-item"><a class="page-link" aria-label="Next"><span aria-hidden="true">»</span></a></li>',
    'prevActive' => '<li class="page-item"><a class="page-link" aria-label="Previous" href="{{url}}">{{text}}</a></li>',
    'prevDisabled' => '<li class="prev disabled page-item"><a class="page-link" aria-label="Previous"><span aria-hidden="true">«</span></a></li>'
];