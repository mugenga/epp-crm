<div class="info-view-wrapper">
    <div class="domain_search_form">
        <h5>Domain name search</h5>
        <!-- //send to search -->
        <?= $this->Form->create(null,['url'=>['controller'=>'Domains', 'action'=>'domain-search']],  array('class' => 'registration'))?>
        
            <?= $this->Form->input('search_term', array('class' => 'form-control search_term', 'label' => false, 'placeholder' => 'Type the one you want here.'));?>
            <select name="type" class="form-control">
               <option value="local">Local</option> 
               <option value="foreign">international</option>
            </select>
            <?= $this->Form->button('Submit', array('class' =>'searchdomainbtn'))?>
        <?= $this->Form->end()?>

        <h5>Domain availability</h5>
        <table id="stripped-table-domain-av" class="stripped-table">
    </table>
    <tr>
            <td></td>
            <td></td>
            <td  class="spinner">
              
            </td>
            <td></td>
       </tr>

    </div>  
</div>
<script>
    $('.searchdomainbtn').click(function(){

        //event.preventDefault();

        var searchTerm = $('.search_term').val();
		var type = $("input[name=type]").val();
        var ajaxdata = $(".registration").serializeArray();
        console.log(ajaxdata)
        $.ajax({
            url: "/epp_crm/domains/domainSearch",
            type:"POST",
            dataType: 'json',
            data : ajaxdata,
            beforeSend: function() {
                // setting a timeout
                $('.spinner').html('loading');
                $('#stripped-table-domain-av').html('');
            
            },
            success: function(data){
                    $('.spinner').html('')
                    console.log(data)
                    let response = JSON.parse(JSON.stringify(data))
                    console.log(response)
                    var x=document.getElementById('stripped-table-domain-av').insertRow(0);
                        var y = x.insertCell(0);
                        var z = x.insertCell(1);
                        var row3 = x.insertCell(2);
                        var row4 = x.insertCell(3);
                        y.innerHTML='Domain';
                        z.innerHTML='Price';
                        row3.innerHTML = "Availability";
                        row4.innerHTML = "Check<";

                    let i;
    
                    for(i = 0; i < response['results'].length; i++){

                        var x=document.getElementById('stripped-table-domain-av').insertRow(i+1);
                        var y = x.insertCell(0);
                        var z = x.insertCell(1);
                        var row3 = x.insertCell(2);
                        var row4 = x.insertCell(3);
                        y.innerHTML=response['results'][i]['domain_name'];
                        z.innerHTML=response['results'][i]['price'];
                        if(response['results'][i]['is_available']){
                            row3.innerHTML = "Available";
                            row4.innerHTML = "<input type='checkbox'>";
                        }else{
                            row3.innerHTML = "Taken";
                            row4.innerHTML = "";
                        }
                        
                    
                    console.log(response['results'][i]['domain_name']);
                    }
            }
            
        });

        let response = JSON.parse('{"domainsearchresults":[{"domain_name":"testdomainingggggggggg.rw","is_available":true,"price":"400.00"},{"domain_name":"testdomainingggggggggg.co.rw","is_available":true,"price":"300.00"},{"domain_name":"testdomainingggggggggg.org.rw","is_available":true,"price":"300.00"},{"domain_name":"testdomainingggggggggg.net.rw","is_available":true,"price":"500.00"},{"domain_name":"testdomainingggggggggg.ac.rw","is_available":true,"price":"500.00"}]}')
        //console.log(response['domainsearchresults'][1]);

        
       
});

</script>