<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Domains',
            ['controller' => 'Domains', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Domain Search</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <div class="animated fadeIn">
			<div class="row">
			      <div class="col-md-2 col-sm-2 col-xs-12">
			          <?= ''//$this->element('cart') ?>
			      </div>
				<div class="col-sm-8 col-md-8 col-xs-12">
			          <?php if(!empty($searchResults)) { ?>
			          <section class="search-header">
			              <h3 class="text-center">

			                  <?php if($searchResults[0]['is_available']): ?>
			                      <i class="fa fa-check search-icon" aria-hidden="true"></i>
			                  <?php endif; ?>

			                  <?php if(!$searchResults[0]['is_available']): ?>
			                      <i class="fa fa-times search-icon" aria-hidden="true"></i>
			                  <?php endif; ?>
			                  <span><?= $searchResults[0]["domain_name"]; ?></span>
			              </h3>
			              <hr />
			          </section>
			          <div class="search-message">
			              <?php if($searchResults[0]['is_available']): ?>
			                  <span class="feedback">This domain is available for purschase!</span>
			                  <span class="pull-right">
			                      <div class="row">
			                          <div class="col-md-8">
			                              <h6 style="padding: 2px;"><?= $this->Number->currency($searchResults[0]["price"], "RWF") ?>/year</h6>
			                          </div>
			                          <div class="col-md-4">
			                              <?= $this->Form->create(null, ['url'=>['controller'=>'Domains', 'action'=>'registration']]) ?>
			                                  <input type="hidden" name="domain_name" value="<?= $searchResults[0]["domain_name"] ?>" />
			                                  <input type="hidden" name="price" value="<?= $searchResults[0]["price"] ?>" />
			                                  <button type="submit" class="btn bg-default addCart list-button" style="padding:0; ">
			                                      <i class="fa fa-cart-plus" aria-hidden="true"></i>
			                                  </button>
			                              </form>
			                          </div>
			                      </div>
			                  </span>
			              <?php endif; ?>

			              <?php if(!$searchResults[0]['is_available']): ?>
			                  <span class="feedback">This domain is not available!</span>
			              <?php endif; ?>
			          </div>
			          <?php } ?>
					<?= $this->Form->create(null, ['url'=>['controller'=>'Domains', 'action'=>'domain-search']]) ?>
			              <div class="input-group">
			                  <input type="text" name="search_term" class="form-control domain-search" placeholder="Search for..." aria-label="Search for...">
			                  <select name="type" class="form-control">
			                      <option value="local">Local</option>
			                      <option value="foreign">international</option>
			                  </select>
			                  <span class="input-group-btn">
			                      <button class="btn btn-secondary bg-dark search-button" type="submit">
			                          <i class="fa fa-search fa-2x search-icon" aria-hidden="true"></i>
			                      </button>
			                  </span>
			          	</div>

			  		<?= $this->Form->end() ?>
			          <span class="loader text-center" style="padding-top:1rem;align-content: center;display:none">
			              <i class="fa fa-spinner fa-2x fa-pulse" aria-hidden="true"></i>
			              <span class="sr-only">Loading...</span>
			          </span>
			          <div class="result-list">
			              <ul class="nav nav-tabs" role="tablist">
			                  <li class="nav-item">
			                      <a class="nav-link active" href="#all" role="tab" data-toggle="tab">
			                          Popular
			                      </a>
			                  </li>
			              </ul>

			              <!-- Tab panes -->
			              <div class="tab-content">
			                  <div role="tabpanel" class="tab-pane active in" id="all">
			                      <?= $this->element('domains', ['list'=>$searchResults]) ?>
			                  </div>
			              </div>

			          </div>
				</div>
			      <div class="col-md-2 col-sm-2 col-xs-12">
			          <?= ''//$this->element('cart') ?>
			      </div>
			</div>
		</div>
	</div>
</main>

<?= $this->Html->script("hotsnackbar") ?>
<?= $this->Html->css("hotsnackbar") ?>
