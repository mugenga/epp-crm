<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Domains',
            ['controller' => 'Domains', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Domains</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <?php
              if(empty($invoice)){
                      echo"<div class='alert alert-warning' role='alert'>
                      Warning!! No invoice created yet
                      </div>";
              }

      ?>
      <div class="row">
        <div class="col-sm-8">
          <div class="card">
                <div class="card-header">Domain info</div>
                <div class="card-body">
                  <table class="vertical-table">
                    <?php
                    $package_name = "";
                    if(empty($domain->package_name)){
                            $package_name = "Unavailable";
                    }
                    else{
                            $package_name = $domain->package_name;
                    }
                    echo "<tr><td><strong>Domain id:</strong></td> <td>".$domain->id."</td></tr>";
                    echo "<tr><td><strong>Domain name: </strong></td> <td>".$domain->domain_name."</td></tr>";
                    echo "<tr><td><strong>Status: </strong></td> <td> ".$domain->status."</td></tr>";
                    echo "<tr><td><strong>Package Name: </strong></td> <td> ". $package_name."</td></tr>";
                    $expiringinNdays = date('Y-m-d', strtotime('+2 month'));
                    $yellownotice = '';
                    if( $domain->expiration_date->format("Y-m-d") <= $expiringinNdays && $domain->expiration_date->format("Y-m-d") > date("Y-m-d")  ){
                            $yellownotice = '#FF8C00;';
                    }else if($domain->expiration_date->format("Y-m-d") < date("Y-m-d") ){
                            $yellownotice = 'red;';
                    }
                    echo "<tr><td><strong>Expiration date:</strong></td> <td style ='color: ". $yellownotice."'> ".$domain->expiration_date->format("d F Y")."</td></tr>";
                    echo "<tr><td><strong>Customer name: </strong></td> <td> ".$domain->order->user->first_name." ".$domain->order->user->last_name."</td></tr>";
                    echo "<tr><td><strong>Email: </strong></td> <td> ".$domain->order->user->email."</td></tr>";
                    ?>
                  </table>
                </div>
          </div>
          <div class="card">
                <div class="card-header">Nameservers</div>
                <div class="card-body">
                  <table class="table table-responsive-sm">
                    <thead>
                      <tr>
                        <th>Nameservers</th>
                        <th>IP Address</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($nameservers as $nameserver){
                    echo "<tr><td><strong>".$nameserver->server_name."</strong></td> <td>".$nameserver->ip_address."</td></tr>";
                    }
                    ?>
                    </tbody>
                  </table>
                  <!-- Button trigger modal -->
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editAddNameservers">
                   Edit/ Add New
                  </button>
                </div>
          </div>
          <div class="card">
                <div class="card-header">Billing Info</div>
                <div class="card-body">
                    <?php foreach($addresses as $address){ ?>
                    <div class="col-sm-4">
                    <h6><?= $address->address_type->address_type?></h6>
                    <table class="stripped-table">
                            <?php
                            echo "<tr><td><strong>Company:</strong></td> <td>".$address->company."</td></tr>";
                            echo "<tr><td><strong>Address Line 1: </strong></td> <td>".$address->address_line_1."</td></tr>";
                            echo "<tr><td><strong>Address Line 2: </strong></td> <td> ".$address->address_line_2."</td></tr>";
                            echo "<tr><td><strong>Country: </strong></td> <td> ".$address->country."</td></tr>";
                            echo "<tr><td><strong>City: </strong></td> <td> ". $address->city."</td></tr>";
                            echo "<tr><td><strong>State/Province/Region: </strong></td> <td> ".$address->state."</td></tr>";
                            echo "<tr><td><strong>Postal/Zip Code: </strong></td> <td> ".$address->zip_code."</td></tr>";
                            echo "<tr><td><strong>Phone:  </strong></td> <td> ".$address->phone."</td></tr>";
                            ?>
                    </table>
                    </div>
                    <?php }?>
              </div>
          </div>


        </div>
        <div class="col-sm-4">
          <div class="card">
             <div class="card-header">
               <i class="fa fa-align-justify"></i> Actions
             </div>
             <div class="card-body">
               <nav class="nav flex-column">
                <a href="#" data-toggle="modal" data-target="#renewDomainModel" class="nav-link">Renew </a>
                <?= $this->Html->link(__('Transfer'), ['action' => 'index'], ['class' => 'nav-link']) ?>
                <?= $this->Html->link(__('Delete'), ['controller' => 'Posts', 'action' => 'add'], ['class' => 'nav-link']) ?>
                <?= ($createinvoice) ?$this->Html->link(__('Create Invoice'), ['controller' => 'Domains', 'action' => 'createinvoice', $domain->id], ['class' => 'nav-link']):"<a class='nav-link' href='#popup1'>Invoice</a>" ?>
                <?= $this->Html->link('Invoicing History', ['controller'=>'Invoices', 'action'=>'domain', $domain->id], ['class' => 'nav-link']) ?>

               </nav>
             </div>
          </div>
        </div>
      </div>


      <div id="popup1" class="overlay">
      	<div class="popup">
      		<h5>Invoice</h5>
      		<a class="close" href="#">&times;</a>
      		<div class="content">
                              <div class="invoice-logo">
                                      <?= $this->Html->image('imaginet_logo.png')?>
                              </div>
                              <div class="invoice-header">
                                      <div class="company-info">
                                              <strong><h5><?= $invoiceAddressDetails[0]->company_name ?></h5></strong>
                                              <strong><h5>TIN No:<?= $invoiceAddressDetails[0]->tin_no ?></h5></strong>
                                              <h5><?= $invoiceAddressDetails[0]->address ?></h5>
                                              <!-- <h5>TELE 10 BLDG, GISHUSHU - REMERA</h5> -->
                                              <h5>Phone <?= $invoiceAddressDetails[0]->phone ?>   <?= $invoiceAddressDetails[0]->email ?></h5>
                                      </div>
                                      <div class="invoice-info">
                                              <h5>INVOICE</h5>
                                              <h5>Invoice date: <?php if(!empty($invoice->created)) echo $invoice->created->format('M d Y'); ?></h5>
                                              <h5>Invoice Due Date: <?php if(!empty($invoice->due)) echo $invoice->due->format('M d Y'); ?></h5>
                                              <h5>Invoice No: <?= $invoice->id ?></h5>
                                              <h5>Invoice Status: <?= $invoice->paid ? 'Paid':'Not Paid' ?></h5>
                                      </div>
                              </div>
                              <div class="invoice-receiver-info">
                                      <p>Bill to: <?= $billingAddress->company ?></p>
                                      <p>Attn: Admin</p>
                                      <p>Email: <?= $domain->order->user->email ?></p>
                                      <p> <?= $billingAddress->phone ?></p>
                              </div>
                              <div class="invoice-body">
                                      <table class="stripped-table">
                                              <tr>
                                                      <th>Description</th>
                                                      <th>Amount(Rwf)</th>
                                              </tr>
                                              <?php
                                                      $domainWithNoVatPrice = intval(($domain->price * 18 / 118));
                                                      $packageWithNovatPrice = 0;
                                                      $packagePricing = 0;
                                                      echo "<tr>";
                                                      echo "<td><strong>Domain name: ".$domain->domain_name ."</strong></td>";
                                                      echo "<td>". ($domain->price - $domainWithNoVatPrice) ."</td>";
                                                      echo "</tr>";
                                              ?>

                                               <?php
                                               if (!empty($package)) {
                                                      $packagePricing = $package->pricing;
                                                      $packageWithNovatPrice = intval($package->pricing * 18 / 118);
                                                      echo "<tr>";
                                                      echo "<td><strong>Hosting package: ".$package->name."</strong></td>";
                                                      echo "<td>".($package->pricing - $packageWithNovatPrice)."</td>";
                                                      echo "</tr>";
                                               }
                                              ?>
                                              <tr>
                                                      <td><strong>VAT ( 18% )</strong></td>
                                                      <td><?= $domainWithNoVatPrice + $packageWithNovatPrice ?></td>
                                              </tr>
                                              <tr>
                                                      <th>Total</th>
                                                      <th><?= $domain->price + $packagePricing?></th>
                                              </tr>
                                      </table>
                              </div>
                              <div class="invoice-payment-details">
                                      <strong><h5>PAYMENT DETAILS</h5></strong>
                                      <p><?= $bankdetails[0]->bank_name ?></p>
                                      <p>Account Number: <?= $bankdetails[0]->acc_number ?></p>
                                      <p>Swift code: <?= $bankdetails[0]->swift_code ?></p>
                                      <p>Mobile Money: <?= $bankdetails[0]->mobile_money ?></p>
                                      <p>Contact: <?= $bankdetails[0]->contact ?></p>
                                      <strong><h5>THANK YOU FOR YOUR BUSINESS</h5></strong>
                              </div>
                      </div>
                      <button class="dark-grey-large-btn"><?= $this->Html->link(__('Send Invoice'), ['controller' => 'Invoices', 'action' => 'InvoiceEmail', $invoice->id]) ?></button>
                      <button class="dark-grey-large-btn"><?= $this->Html->link(__('Download PDF'), ['controller' => 'Invoices', 'action' => 'downloadInvoiceAsPDF', $invoice->id]) ?></button>
      	</div>
      </div>

      <!-- Edit nameservers button -->
      <div class="modal fade" id="editAddNameservers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Add/Edit nameservers</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?= $this->Form->create(null,['url' => ['controller' => 'Domains', 'action' => 'addNameservers']])?>
            <div class="modal-body" style="position:relative; height: 100px">
              <div style=" width:80%; position:absolute;left:10%;top:5%;">
                      <input type='hidden' name='domain_name' value='<?= $domain->domain_name ?>'>
                      <input type='hidden' name='domain_id' value='<?= $domain->id ?>'>
                      <input type='hidden' name='id_1' value='<?= (isset($nameservers[0]->id))? $nameservers[0]->id:'' ?>'>
                      <input type='hidden' name='id_2' value='<?= (isset($nameservers[1]->id))? $nameservers[1]->id:'' ?>'>

                      Nameserver 1: <input type='text' name='server_name_1' value='<?= (isset($nameservers[0]->server_name))? $nameservers[0]->server_name:'' ?>'>
                      IP Address 1: <input type='text' name='ip_address_1' value='<?= (isset($nameservers[0]->ip_address))? $nameservers[0]->ip_address:'' ?>'>
                      <br><br>
                      Nameserver 2: <input type='text' name='server_name_2' value='<?= (isset($nameservers[1]->server_name))? $nameservers[1]->server_name:'' ?>'>
                      IP Address 2: <input type='text' name='ip_address_2' value='<?= (isset($nameservers[1]->ip_address))? $nameservers[1]->ip_address:'' ?>'>
              </div>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Renew domain button -->
      <div class="modal fade" id="renewDomainModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Renew </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?= $this->Form->create(null,['url' => ['controller' => 'Domains', 'action' => 'handleDomainRenewal']])?>
            <div class="modal-body" style="position:relative; height: 100px">
              <div style=" width:80%; position:absolute;left:10%;top:5%;">
                      <input type='hidden' name='order_id' value='<?= $domain->order_id ?>'>
                      <input type='hidden' name='orderDetails' value='<?= $domain->id ?>'>
                      <h4>Renew <?=$domain->domain_name?> for</h4>
                      <select name="renewal_years" style="height:100%; width:60%;">
                              <option value="1">1 year</option>
                              <option value="2">2 years</option>
                              <option value="3">3 years</option>
                              <option value="4">4 years</option>
                              <option value="5">5 years</option>
                              <option value="6">6 years</option>
                              <option value="7">7 years</option>
                              <option value="8">8 years</option>
                              <option value="9">9 years</option>
                              <option value="10">10 years</option>
                      </select>
              </div>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Renew</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
