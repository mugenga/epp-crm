<?php
class xtcpdf extends TCPDF {

}

//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
mb_internal_encoding('UTF-8');
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Imaginet LTD');
$pdf->SetTitle($title);
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
//$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$html = '';
$html .= '<style>'.file_get_contents('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' ).'</style>';
$html .= '<style>'.file_get_contents(WWW_ROOT.'css\home.css' ).'</style>';

$html .= <<<EOD

<div class="invoice_view popup">
        <h5>Invoice</h5> 
        <div class="content">
                <div class="invoice-logo">
                        <?= this->Html->image('imaginet_logo.png')?>
                </div>
                <div class="invoice-header">
                        <div class="company-info">
                                <p>IMAGINET LTD</p>
                                <p>TIN No:101917017</p>
                                <p>KG 230 ST</h5>
                                <p>TELE 10 BLDG, GISHUSHU - REMERA</p>
                                <p>Phone 0788305200   Email info@imaginet.rw</p>
                        </div>
                        <div class="invoice-info">
                                <p>INVOICE</p>
                                <p>Invoice date: <?php if(!empty(invoice->created)) echo invoice->created->format('M d Y'); ?></p>
                                <p>Invoice Due Date: <?php if(!empty(invoice->due)) echo invoice->due->format('M d Y'); ?></p>
                                <p>Invoice No: <?= invoice->invoice_no ?></p>
                                <p>Invoice Status: <?= invoice->paid ? 'Paid':'Not Paid' ?></p>
                        </div>
                </div>
                <div class="invoice-receiver-info">
                        <p>Bill to: <?= billingAddress->company ?></p>
                        <p>Attn: Admin</p>
                        <p>Email: <?= domain->order->user->email ?></p>
                        <p> <?= billingAddress->phone ?></p>
                </div>
                <div class="invoice-body">
                        <table class="stripped-table">
                                <tr>
                                        <th style="text-align:left">Description</th>
                                        <th>Amount(Rwf)</th>
                                </tr>
                                <?php 
                                        domainWithNoVatPrice = intval((domain->price * 18 / 118));
                                        packageWithNovatPrice = 0;
                                        packagePricing = 0;
                                        echo "<tr>";
                                        echo "<td style="text-align:left"><strong>Domain name: ".domain->domain_name ."</strong></td>";
                                        echo "<td>". (domain->price - domainWithNoVatPrice) ."</td>";
                                        echo "</tr>";
                                ?>

                                        <?php 
                                        if (!empty(package)) {
                                        packagePricing = package->pricing;
                                        packageWithNovatPrice = intval(package->pricing * 18 / 118);
                                        echo "<tr>";
                                        echo "<td style="text-align:left"><strong>Hosting package: ".package->name."</strong></td>";
                                        echo "<td>".(package->pricing - packageWithNovatPrice)."</td>";
                                        echo "</tr>";
                                        }
                                ?>
                                <tr>
                                        <td style="text-align:left">VAT ( 18% )</td>
                                        <td><?= domainWithNoVatPrice + packageWithNovatPrice ?></td>
                                </tr>
                                <tr>
                                        <th style="text-align:left; padding:100px">Total</th>
                                        <th><?= domain->price + packagePricing ?></th>
                                </tr>
                        </table>
                </div>
                <div class="invoice-payment-details">
                        <strong><h5>PAYMENT DETAILS</h5></strong>
                        <p>Access Bank</p>
                        <p>Account Number: 700210010133641</p>
                        <p>Swift code: BKORRWRW</p>
                        <p>Mobile Money: 0781520124</p>
                        <p>Contact: BKORRWRW</p>
                        <strong><h5>THANK YOU FOR YOUR BUSINESS</h5></strong>
                </div>
        </div>
</div>
EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
 $filename= "Invoice.pdf";  
$foldername = date('F-Y');

if (!file_exists(WWW_ROOT.'invoices/'.$foldername)) {
mkdir(WWW_ROOT.'invoices/'.$foldername, 0777, true);
}
$filelocation = WWW_ROOT.'invoices/'.$foldername; 

$fileNL = $filelocation."\\".$filename;
$pdf->Output($fileNL,'F');





 //$this->redirect(array('action'=>'index'), null, true);
 $pdf->Output($filename,'I');

//============================================================+
// END OF FILE
//============================================================+