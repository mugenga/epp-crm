<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Domains',
            ['controller' => 'Domains', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Domains</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <div class="animated fadeIn">
    <section class="page-header">

        <div class="page-title">
            <h4>Domains</h4>
        </div>
        <div class="page-top-buttons">
            <button class="dark-grey-large-btn">
                <?= $this->Html->link('Add Domain', ['controller' => 'domains', 'action' => 'domain-search'], ['escape'=>false])?>
            </button>
            <button class="dark-grey-large-btn">Export list</button>
        </div>
        <?= $this->Form->create(null,array('url'=>['controller'=>'Domains', 'action'=>'index'], 'class' =>'page-filtering'))?>

            <div class="name">
                    <label class="check">Domain:</label>
                    <input type="text" name="domain_name">
            </div>
            <div class="name">
                    <label class="exp_date"> Expiry From:</label>
                    <input type="date" name="expiration_from">
            </div>
            <div class="name">
                    <label class="exp_date"> Expiry To:</label>
                    <input type="date" name="expiration_to">
            </div>
            <div class="select-style">
                <select name="domain_status">
                    <option value="">All</option>
                    <option value="ACTIVE">Active</option>
                    <option value="EXPIRED">Expired</option>
                    <option value="PENDING">Pending</option>
                </select>
            </div>
            <div class="btn">
                <button type="submit">Search</button>
            </div>

        </form>

    </section>
      <div class="card">
        <div class="card-header">
          <i class="fa fa-align-justify"></i> List of Locations</div>
        <div class="card-body">


        <table class="table table-responsive-sm">
          <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Year</th>
                <th>Expiring on</th>
                <th>Status</th>
                <th>Deleted after</th>
            </tr>
          </thead>
          <tbody>
            <?php
                foreach($domains as $domain):
                    if(strtotime($domain->expiration_date) < time()) {
                        $colorclass = 'expireddomainlink';
                    }
                    elseif( (strtotime($domain->expiration_date) > time()) && (strtotime($domain->expiration_date) < strtotime("+60 day")) ) {
                        $colorclass = 'expiresoonlink';
                    }
                    else {
                        $colorclass = 'okaydomainlink';
                    }
                    echo "<tr>";
                    echo "<td><a href='/epp_crm/domains/view/".$domain->id."'>".$domain->domain_name."</a></td>";
                    echo "<td>".$domain->price."</td>";
                    echo "<td>".$domain->quantity."</td>";
                    echo "<td class= ". $colorclass.">".$domain->expiration_date->format("d F Y")."</td>";
                    echo "<td>".$domain->status."</td>";
                    echo "<td>".date('d F Y', strtotime($domain->expiration_date. ' + 2 months'))."</td>";
                    echo "</tr>";

                endforeach
            ?>
          </tbody>
        </table>
    <nav>
      <ul class="pagination">
        <?= $this->Paginator->prev(__('previous') . ' >') ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
      </ul>
    </nav>
  </div>
</div>
</div>
  </div>
</main>
