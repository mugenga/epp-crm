<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Domains',
            ['controller' => 'Domains', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Create Invoice</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <div class="info-view-wrapper">
          <div class="home-table-left">
              <h5>Domain info</h5>
              Domain name: <?= $domain->domain_name?><br>
              Expiry date: <?= $domain->expiration_date->format('d M, Y')?><br>
              Amount: <?= $domain_price->price ?> Rwf
              <br>

              <h5>Hosting info</h5>
              <?php
                  $package_name = "";
                  if(empty($domain->package_name)){
                      $package_name = "Unavailable";
                     }
                  else{
                      $package_name = $domain->package_name;
                     }

              ?>

              Package : <?= $package_name ?><br>
              Package price : <?= $package_price->pricing ?><br>

              <h5> Change Package </h5><br>
              <?= $this->Form->create(null,['url' => ['controller' => 'Domains', 'action' => 'updateHosting']])?>

              <?= $this->Form->hidden('order_details_id', array('value'=> $domain->id)) ?>
              <select name="package_name" id="" class="form-control">
                  <?php
                  foreach($packages as $package){
                      echo "<option value='".$package->name."'>".$package->name."</option>";
                         }
                  ?>
              </select><br>
              <?= $this->Form->button('Submit', array('class' =>'dark-grey-large-btn'))?>
              <?= $this->Form->end()?>
          </div>
          <div class="home-table-right">
              <h5>Billing address</h5>
              <?= $this->Form->create("Registrations",array('url'=>'/domains/createinvoice/'.$domain->id))?>
              <?= $this->Form->hidden('order_details_id', array('value'=> $domain->id)) ?>
              <?= $this->Form->hidden('due', array('value'=> $domain->expiration_date)) ?>
              <?= $this->form->hidden('domain_price', array('value' => $domain_price->price)) ?>
              <?= $this->form->hidden('hosting_price', array('value' => $package_price->pricing)) ?>

              <p>Bill to</p>
              <select name="address_id" id="" class="form-control">
                  <?php
                  foreach($address as $info){
                      echo "<option value='".$info->id."'>".$domain->order->user->first_name." ".$domain->order->user->last_name." - ".$info->address_type->address_type."</option>";
                         }
                  ?>
              </select><br>
              <?= $this->Form->button('Submit', array('class' =>'dark-grey-large-btn'))?>
              <?= $this->Form->end()?>
          </div>
      </div>
    </div>
  </div>
</main>
