<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Domains',
            ['controller' => 'Domains', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Domain Search',
            ['controller' => 'Domains', 'action' => 'domainSearch'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Registration</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
		<?= $this->Flash->render() ?>
    <div class="animated fadeIn">
			<div class="row">
		        <div class="col-md-2 col-sm-2 col-xs-12">
		            <?=''// $this->element('cart') ?>
		        </div>
				<div class="col-sm-8 col-md-8 col-xs-12">
		            <?= $this->element("billing") ?>
				</div>
		        <div class="col-md-2 col-sm-2 col-xs-12">
		            <?=''// $this->element('cart') ?>
		        </div>
			</div>
  	</div>
	</div>
</main>
