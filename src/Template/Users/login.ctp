<?php
$this->layout = false;
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>

    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= " "//$this->Html->css('base.css') ?>
    <?= ""//$this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>


    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
     <!-- $this->Html->meta('icon')  -->
      <!-- $this->Html->css('base.css')
      $this->Html->css('style.css')  -->
     <?= $this->Html->css('home.css')   ?>]
     <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">


</head>
<body class="loginbody">
<div class="login_container">
	<div class="login_box">
		<?php echo $this->Html->link($this->Html->image('imaginet_logo2.png'), '/', array('class' => 'login_logo', 'escape' => false)) ?>
		<h5 class="text-center">Imaginet secure login</h5>
		<?= $this->Form->create(); ?>
             <?= $this->Flash->render() ?>
			<div class="form-group">
				<?= $this->Form->control('email', array('class' =>'form-control')); ?>
			</div>

			<div class="form-group">
				<?= $this->Form->control('password', array('type' => 'password', 'class' => 'form-control')); ?>
			</div>
			<?= $this->Form->submit('Login', array('class' => 'imaginet_gold_login')); ?>
		<?= $this->Form->end(); ?>
	</div>
</div>
</body>
</html>
