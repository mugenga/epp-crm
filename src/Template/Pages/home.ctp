<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <a href="#">Admin</a>
    </li>
    <li class="breadcrumb-item active">Dashboard</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-primary">
            <div class="card-body pb-0">
              <div class="btn-group float-right">
                <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="icon-settings"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="/epp_crm/domains">View</a>
                </div>
              </div>
              <div class="text-value"><?=$allcount?></div>
              <div>TOTAL DOMAINS</div>
            </div>
            <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
              <canvas class="chart" id="card-chart1" height="70"></canvas>
            </div>
          </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-info">
            <div class="card-body pb-0">
              <div class="btn-group float-right">
                <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="icon-settings"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="/epp_crm/domains?q=expiring-7-days">View</a>
                </div>
              </div>
              <div class="text-value"><?= $expiringIn7Nbr ?></div>
              <div>EXP. IN 7 DAYS</div>
            </div>
            <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
              <canvas class="chart" id="card-chart2" height="70"></canvas>
            </div>
          </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-warning">
            <div class="card-body pb-0">
              <div class="btn-group float-right">
                <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="icon-settings"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="/epp_crm/domains?q=expiring-30-days">VIew</a>
                </div>
              </div>
              <div class="text-value"><?= $expiringIn30Nbr ?></div>
              <div>EXP. IN 30 DAYS</div>
            </div>
            <div class="chart-wrapper mt-3" style="height:70px;">
              <canvas class="chart" id="card-chart3" height="70"></canvas>
            </div>
          </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-danger">
            <div class="card-body pb-0">
              <div class="btn-group float-right">
                <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="icon-settings"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="/epp_crm/domains?q=expired">View</a>
                </div>
              </div>
              <div class="text-value"><?= $expirednbr ?></div>
              <div>Expired</div>
            </div>
            <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
              <canvas class="chart" id="card-chart4" height="70"></canvas>
            </div>
          </div>
        </div>

        <!-- /.col-->
      </div>
      <!-- /.row-->
      <div class="card">
        <div class="card-body">
          <!-- /.row-->
          <div class="chart-wrapper" style="height:300px;margin-top:40px;">
            <canvas id="myChart" height="300"></canvas>
          </div>
        </div>
      </div>
      <!-- /.row-->
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">Expiring in < 30 days</div>
            <div class="card-body">
              <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                  <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Exp. on</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Del. after</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                      foreach($expiring as $domain):
                          echo "<tr>";
                          echo "<td class='text-center'><a class='okaysoonlink' href='/epp_crm/domains/view/".$domain->id."'>".$domain->domain_name."</a></td>";
                          echo "<td>".$domain->expiration_date->format("d M Y")."</td>";
                          echo "<td>".$domain->status."</td>";
                          echo "<td>".date('d M Y', strtotime($domain->expiration_date. ' + 2 months'))."</td>";
                          echo "</tr>";
                      endforeach
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- /.row-->
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">Expired Domains</div>
            <div class="card-body">
              <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                  <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Exp. on</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Deleted after</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                      foreach($expired as $domain):
                          echo "<tr>";
                          echo "<td><a href='/epp_crm/domains/view/".$domain->id."'>".$domain->domain_name."</a></td>";
                          echo "<td class='expireddomainlink'>".$domain->expiration_date->format("d M Y")."</td>";
                          echo "<td>".$domain->status."</td>";
                          echo "<td>".date('d M Y', strtotime($domain->expiration_date. ' + 2 months'))."</td>";
                          echo "</tr>";

                      endforeach
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>


    </div>
  </div>
</main>

<script>
$.ajax({
		url: "pages/bargraph",
		type:"GET",
		success: function(result){
			array = json2array(result);
            myfunction(array)
 }});

 function json2array(json){
    var result = [];
    var keys = Object.keys(json);
    keys.forEach(function(key){
        result.push(json[key]);
    });
    return result;
}


 function myfunction(array){
     var label = [];
     var data = [];

for(i = 0 ; i < array.length ; i++){
    //for (var k in array[i]) {
    if(array[i]['month'] == "1"){label[i] = "january"}else if(array[i]['month'] == "2"){label[i] = "february"}else if(array[i]['month'] == "3"){label[i] = "march"}
    else if(array[i]['month'] == "4"){label[i] = "april"}else if(array[i]['month'] == "5"){label[i] = "may"}else if(array[i]['month'] == "6"){label[i] = "june"}
    else if(array[i]['month'] == "7"){label[i] = "july"}else if(array[i]['month'] == "8"){label[i] = "august"}else if(array[i]['month'] == "9"){label[i] = "september"}
    else if(array[i]['month'] == "10"){label[i] = "october"}else if(array[i]['month'] == "11"){label[i] = "november"}else if(array[i]['month'] == "12"){label[i] = "december"}

    data[i]= array[i]['count'];
    console.log(array[i]['count']);
   // }
}
console.log(label)
console.log(data)
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: label,
        datasets: [{
            label: '# Domains per registration month',
            data: data,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

 }
</script>
