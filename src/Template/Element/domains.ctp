<div class="table-responsive">

	<table class="table table-striped">
		<tbody class="search-body">
			<?php if(!is_null($list)): ?>
			<?php $domainCounter = 0; ?>
			<?php foreach($list as $domain): ?>
			<?php if($domain["is_available"] !== 'error'): ?>
				<?php $domainCounter++; ?>
				<tr>
					<td class="width-70"><?= $domain["domain_name"] ?></td>
					<td class="width-30">
						<span class="pull-right">
							<?php if($domain["is_available"]): ?>
								<div class="row">
									<div class="col-md-8">
										<h6 style="padding: 2px;"><?= $this->Number->currency($domain["price"], "RWF") ?>/year</h6>
									</div>
									<div class="col-md-4">
										<?= $this->Form->create(null, ['url'=>['controller'=>'Domains', 'action'=>'registration']]) ?>
											<input type="hidden" name="domain_name" value="<?= $domain["domain_name"] ?>" />
											<input type="hidden" name="price" value="<?= $domain["price"] ?>" />
											<button type="submit" class="btn bg-default addCart list-button" style="padding:0; ">
												<i class="fa fa-cart-plus" aria-hidden="true"></i>
											</button>
										</form>
									</div>
								</div>
							<?php endif; ?>

							<?php if(!$domain["is_available"]): ?>
								Taken !
							<?php endif; ?>
						</span>	
					</td>
				</tr>
			<?php endif ?>
			<?php endforeach; ?>
		<?php endif ?>
		</tbody>
	</table>
	<?php if($domainCounter == 0 || count($list) == 0): ?>
		<div class="alert alert-danger">
			No results return from your domain search. Please do refine your domain name and try again
		</div>
	<?php endif; ?>
</div>