<div  style="margin-top:1rem;margin-bottom:2rem">
           <?= $this->Form->create(null, ['url'=>['controller'=>$items['controller'], 'action'=>$items['action']]]) ?>
           <div class="form-group row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="input-group">
                    <input type="text" name="search_term" class="form-control" placeholder="Search for..." aria-label="Search for...">
                
            </div>
        </div>
        <?php if($this->request->session()->read()['Auth']['User']['user_type'] === 'ADMIN'): ?>
        <div class="col-md-6 col-xs-12">
            <div class="input-group input-daterange">
                <input  type="date" class="form-control" name="from" placeholder="Date From">
                <div class="input-group-addon">to</div>
                <input  type="date" class="form-control" name="to" placeholder="Date To">
                 <span class="input-group-btn">
                        <button class="btn btn-secondary" type="submit" id="search">Search</button>
                </span>
            </div>  
        </div>

        <div class="col-md-2">
            <?php $action = $this->request->action;  ?>

          
            <?php if($action === 'mmPayments' || $action === 'ccPayments'): ?>
                <?= $this->Html->link('Export CSV', ['action'=>'export-transaction', $action],['class'=>'btn btn-info btn-sm pull-right']) ?>
            <?php endif; ?>
           

        </div>
      
        <?php endif; ?>
    </div>
    <?= $this->Form->end() ?>
</div>