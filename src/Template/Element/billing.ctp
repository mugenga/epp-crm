<div class="billing">
<?= $this->Form->create('', ['url'=>['controller'=>'Domains', 'action'=>'handleDomainRegistration'], 'id'=>'billing-form']) ?>

    <input type="hidden" name="domain_name" value="<?=$domain_name?>"/>
    <input type="hidden" name="amount" value="<?=$price?>"/>

    <div class="card">
          <div class="card-header">Domain registration</div>
          <div class="card-body">
            Price :<?=$price?>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-xs-12">
                   <h6>Domain Name: <?=$domain_name?></h6>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <a href="#" class="badge badge-danger float-right">cancel registration</a>
                </div>
            </div>
          </div>
    </div>
    <div class="card">
          <div class="card-header">Registration period </div>
          <div class="card-body">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-xs-12">
                    <select name="quantity" style="height:100%; width:90%;">
                        <option value="1">1 year</option>
                        <option value="2">2 years</option>
                        <option value="3">3 years</option>
                        <option value="4">4 years</option>
                        <option value="5">5 years</option>
                        <option value="6">6 years</option>
                        <option value="7">7 years</option>
                        <option value="8">8 years</option>
                        <option value="9">9 years</option>
                        <option value="10">10 years</option>
                    </select>
                </div>
            </div>
          </div>
    </div>
    <div class="card">
          <div class="card-header">Nameservers</div>
          <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-xs-4">
                            <small>Nameserver 1:</small> <input value="ns1.imaginetrwone.com" type='text' name='server_name_1'>
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-8">
                            <small>IP Address 1:</small> <input type='text' name='ip_address_1'>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-xs-4">
                            <small>Nameserver 2:</small> <input value="ns2.imaginetone.rw" type='text' name='server_name_2'>
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-8">
                            <small>IP Address 2:</small> <input type='text' name='ip_address_2'>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>

    <div class="card">
          <div class="card-header">Registrant</div>
          <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-xs-4">
                            <small style=" margin-left:40%; color: #5EC16C; font-size:1.2em; cursor: pointer">Create:</small>
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-8">
                            <small style=" margin-left:40%; color: #5EC16C; font-size:1.2em; cursor: pointer" data-toggle="modal" data-target="#editAddNameservers">Find:</small>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>

    <div class="card">
          <div class="card-header">Contact</div>
          <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                  <div class="contact">


                  </div>
                </div>
            </div>
          </div>
    </div>
    <div class="card">
          <div class="card-header">Hosting</div>
          <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                  <select class="" name="package_name">
                    <option value="NULL">No Hosting</option>
                    <?php
                    foreach($packages as $package){
                        echo "<option value='".$package->name."'>".$package->name."</option>";
                           }
                    ?>
                  </select>
                </div>
            </div>
          </div>
    </div>


    <div class="form-group" style="margin-top: 50px">
        <?= $this->Form->button(__('<i class="fa fa-hand-o-right"></i> Register'), ['class'=>'dark-grey-large-btn col-sm-6 col-md-6 col-xs-12']); ?>
    </div>

<?= $this->Form->end(); ?>

</div>

<!-- Edit nameservers button -->
<div class="modal fade" id="editAddNameservers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Search for existing address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form>
      <div class="modal-body" style="position:relative;">
        <div style=" width:50%; position:absolute;left:30%;top:5%;">
            <div class="row">
                <div class="col-sm-3">Name:</div>
                <div class="col-sm-9"><input class="name" type='text' name='name'></div>
            </div>
            <div class="row">
                <div class="col-sm-3">Email:</div>
                <div class="col-sm-9"><input class="email" type='text' name='email'></div>
            </div>
            <div class="row">
                <div class="col-sm-3">Organisation:</div>
                <div class="col-sm-9"><input class="organisation" type='text' name='organisation'></div>
            </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary dismissresults" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary existingDomainSearch">Search</button>
      </div>
      </form>
    </div>

  </div>
</div>
<script>
    function dismissHTML(){
      var form = '<div style=" width:50%; position:absolute;left:30%;top:5%;">';
          form+= '<div class="row"><div class="col-sm-3">Name:</div>';
          form+= '<div class="col-sm-9"><input class="name" type="text" name="name"></div></div>';
          form+= '<div class="row"><div class="col-sm-3">Email:</div><div class="col-sm-9"><input class="email" type="text" name="email"></div></div>';
          form+= '<div class="row"><div class="col-sm-3">Organisation:</div>';
          form+= '<div class="col-sm-9"><input class="organisation" type="text" name="organisation"></div></div></div>';
      return form;
    }

    $('.dismissresults').click(function(){
      $('.modal-body').html(dismissHTML);
    });

    $('.close').click(function(){
      $('.modal-body').html(dismissHTML);
    });

    $('.existingDomainSearch').click(function(){
        event.preventDefault();

        var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;

        var clientname = $('.name').val();
		    var email = $(".email").val();
        var organisation = $(".organisation").val();
        $('.modal-body').html('<div class="text-center" style="margin-top:5%">Searching user. Please wait...</div>');
        $.ajax({
            headers: {
                'X-CSRF-Token': csrfToken
            },
            url: "/epp_crm/domains/existingDomainSearch",
            type:"POST",
            data : {
                    clientname : clientname,
                    email: email,
                    organisation : organisation
            } ,
            success: function(data){
                array = json2array(data);
                console.log(array);

                 var table ='<table class="table table-responsive-sm"><thead><tr><th>Address Id</th><th>Name</th><th>Company</th><th>Email</th><th>Address Line</th><th>Choose </th></tr></thead><tbody>';
                 for(var i = 0; i < array.length; i++){
                    console.log(i);
                    table+= '<tr>';
                    table+= '<td>'+ array[i]['id'] +'</td>';
                    table+= '<td>'+ array[i]['user']['first_name'] +'</td>';
                    table+= '<td>'+ array[i]['company'] +'</td>';
                    table+= '<td>'+ array[i]['user']['email'] +'</td>';
                    table+= '<td>'+ array[i]['address_line_1'] +'</td>';
                    table+='<td><button onclick="handlechoosingBillingAddress('+array[i]['id']+')" data-dismiss="modal"><input type="radio"> </button></td>';
                    table+= '</tr>';
                 }
                 table+= '</tbody></table>';
                 $('.modal-body').html(table);

            }
        });

    });

    function json2array(json){
            var result = [];
            var keys = Object.keys(json);
            keys.forEach(function(key){
                result.push(json[key]);
            });
            return result;
        }

    function handlechoosingBillingAddress(addressid){

        var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
        $.ajax({
            headers: {
                'X-CSRF-Token': csrfToken
            },
            url: "/epp_crm/domains/handleGettingSingleAddress",
            type:"POST",
            data : {
                addressid : addressid,
            } ,
            success: function(data){
                array = json2array(data);
                console.log(array);
                var addressHTML = '<h5>Registrant Address</h5><b>Company: </b><span class="company">'+data['company']+'</span><br />'
                addressHTML+= '<b>Address Line 1: </b><span class="address-line-1">'+data['address_line_1']+'</span><br />'
                addressHTML+= '<b>Country: </b><span class="country">'+data['country']+'</span><br />'
                addressHTML+= '<b>City: </b><span class="city">'+data['city']+'</span><br />'
                addressHTML+= '<b>Phone: </b><span class="phone">'+data['phone']+'</span><br />'
                addressHTML+= '<b>Email: </b><span>'+data['user']['email']+' </span>'
                addressHTML+='<input type="hidden" name="user_id" value="'+data['user']['id']+'">';
                addressHTML+='<input type="hidden" name="address_id" value="'+data['id']+'">';

                $('.contact').html(addressHTML);

            }
        });
    }
</script>
