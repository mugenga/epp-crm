<h2 align="center" style="background:orange;color:#000;border:1px solid #ccc;padding:14px">Imaginet Limited</h2>
<p style="line-height:180%">
	Dear <?= $fullname; ?>,<br />

	<?php if(!$failed): ?>
		Thank you for your purchase. We are processing your request and your domain name will be ready for use within the next 24 hours. In case of any inquiries or concerns, kindly contact Support. <br />Thank you once again, and welcome to the Imaginet Familiy.
	<?php endif; ?>

	<?php if($failed): ?>
		Thank you for your purchase. There has been a problem while processing your request. We are looking
		into it and we shall contact you within 8 hours about your request. Kindly contact Support if you have
		any concerns or inquiries. Thank you once again, and welcome to the Imaginet Familiy.
	<?php endif; ?>
	
</p>
<h3 style="">Your Domain Order Details</h3>
<hr style="color:#ccc" />

<p style="line-height: 180%">Order Date: <?= $order_date ?></p>

<div class="table-responsive">
	
			
		<?php foreach($cart as $product): ?>
			<p style="padding:10px;background:#f7f7f9;line-height:180%">
				Domain: <b><?= $product["name"] ?></b><br />
				No. of years: <b><?= $product["year"] ?></b><br />
				Total: <b><?= $this->Number->currency($product["year"] * $product["price"], "RWF") ?></b>
			</p>	

			<?php if(count($product["pack"]) > 0): ?>
           <p style="padding:10px;background:#f7f7f9;line-height:180%">
              Hosting Package: <em><b><?= $product["pack"]["name"] ?></b></em><br />
              Cost:  <?= $this->Number->currency($product["pack"]["price"], "RWF") ?>
          	</p>
          <?php endif; ?>

		<?php endforeach; ?>
	

	<p style="line-height: 180%">
		Discount: <b><?= $this->Number->currency($discount, "RWF") ?></b><br />
		Subtotal: <b><?= $this->Number->currency($total, "RWF") ?></b>
	</p>
	
	<p style="">
		<h4>Contact Us</h4>
		<em>
			Imaginet Limited<br />
			P. O. Box 448789<br />
			1st Floor Tele 10 Building<br />
			Airport Road, Gishushu <br />
			Kigali - Rwanda<br />
			Email:info@imaginet.rw<br />
			Phone: +250 788 305 200/+250 788 920 481/+256 778 591 026 
		</em>
	</p>
</div>
