<?php
$this->layout = false;
?>
<html>
<head>
<!-- Styling our invoice email -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">  
<style>

.stripped-table {border-collapse: collapse;width: 100%;}
.stripped-table th{color:#CF772B;text-align: left;padding: 5px; font-weight: lighter; font-size: 15px}
.stripped-table td {text-align: left;padding: 10px;color:#343A40;font-size:0.8em;} 
.stripped-table tr:nth-child(odd) {background-color: #fff;}
.stripped-table th a, .stripped-table tr td a{text-decoration: none; color:#343A40}
.stripped-table tr td .expiringdomainlink{color:greenyellow}
.stripped-table .okaydomainlink{color: green}
.stripped-table .expireddomainlink{color:rgb(199, 51, 51)}
.stripped-table .expiresoonlink{color:rgb(234, 182, 26)}

.box {
    width: 40%;
    margin: 0 auto;
    background: rgba(255,255,255,0.2);
    padding: 35px;
    border: 2px solid #fff;
    border-radius: 20px/50px;
    background-clip: padding-box;
    text-align: center;
  }
  
.button {
  font-size: 1em;
  padding: 10px;
  color: #fff;
  border: 2px solid #06D85F;
  border-radius: 20px/50px;
  text-decoration: none;
  cursor: pointer;
  transition: all 0.3s ease-out;
}
.button:hover {background: #06D85F;}
.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlay:target {visibility: visible;opacity: 1;}
.popup {
  margin: 0px auto;
  /* padding: 20px; */
  background: #fff;
  border-radius: 5px;
  width: 60%;
  /*height: 630px;*/
  position: relative;
  transition: all 5s ease-in-out;
}

.invoice_view{
  margin-top:70px;
  width: 80%;
}

.popup h5, .invoice_view h5 {margin-top: 0;color: #333;font-family: Tahoma, Arial, sans-serif;text-align: center; font-size:2em;}
.popup .close {
  position: absolute;
  top: 5px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {color: #06D85F;}
.popup .content, .invoice_view .content {
  margin-left: 0%;
  /*max-height: 85%;*/
  overflow: auto;
  display: block;
  width: 100%;
  padding:1em 2em;
  margin-top: 10px;
  margin-bottom: 10px;
}    

.popup .content .invoice-logo img{width:10%;background:black;margin-left: 47%;}
.popup .content .invoice-header{display: flex;}
.popup .content .invoice-header .company-info{width: 50%;justify-content: left; }
.popup .content .invoice-header .invoice-info {float:right;width: 50%}
.popup .content .invoice-header .company-info h5{font-size:0.8em;line-height: 1px;text-align: left; margin-top: 20px;margin-left:0;}
.popup .content .invoice-header .invoice-info h5{font-size:0.8em;line-height: 1px;margin-left: 40%;text-align: right;margin-top: 20px}
.popup .content .invoice-body, .popup .content .invoice-payment-details, .invoice-receiver-info{margin-top:50px;}
.popup .content .invoice-body .stripped-table th,.popup .content .invoice-body .stripped-table td{text-align: right;}
.popup .content .invoice-body .stripped-table th:first-child,.popup .content .invoice-body .stripped-table td:first-child{text-align: left;}
.popup .content .invoice-body .stripped-table th{ background-color:#5a57572f}
.popup .content .invoice-payment-details h5, .popup .content .invoice-payment-details p, .popup .content .invoice-receiver-info p
{font-size:0.8em;line-height: 5px;}
.popup .content .invoice-receiver-info h5{font-size:0.8em;line-height: 1px;}
@media screen and (max-width: 700px){.box{width: 70%;}.popup{width: 70%;}}

    
</style>
</head>
<body>
<div class="invoice_view popup">
        <h5>Invoice</h5> 
        <div class="content">
                <div class="invoice-logo">
                        <img src="<?= WWW_ROOT.'img/imaginet_logo.png'?>"/>
                </div>
                <div class="invoice-header">
                        <div class="company-info">
                                <strong><h5>IMAGINET LTD</h5></strong>
                                <strong><h5>TIN No:101917017</h5></strong>
                                <h5>KG 230 ST</h5>
                                <h5>TELE 10 BLDG, GISHUSHU - REMERA</h5>
                                <h5>Phone 0788305200   Email info@imaginet.rw</h5>
                        </div>
                        <div class="invoice-info">
                                <h5>INVOICE</h5>
                                <h5>Invoice date: <?php if(!empty($invoice->created)) echo $invoice->created->format('M d Y'); ?></h5>
                                <h5>Invoice Due Date: <?php if(!empty($invoice->due)) echo $invoice->due->format('M d Y'); ?></h5>
                                <h5>Invoice No: <?= $invoice->invoice_no ?></h5>
                                <h5>Invoice Status: <?= $invoice->paid ? 'Paid':'Not Paid' ?></h5>
                        </div>
                </div>
                <div class="invoice-receiver-info">
                        <p>Bill to: <?= $billingAddress->company ?></p>
                        <p>Attn: Admin</p>
                        <p>Email: <?= $domain->order->user->email ?></p>
                        <p> <?= $billingAddress->phone ?></p>
                </div>
                <div class="invoice-body">
                        <table class="stripped-table">
                                <tr>
                                        <th>Description</th>
                                        <th>Amount(Rwf)</th>
                                </tr>
                                <?php 
                                        $domainWithNoVatPrice = intval(($domain->price * 18 / 118));
                                        $packageWithNovatPrice = 0;
                                        $packagePricing = 0;
                                        echo "<tr>";
                                        echo "<td><strong>Domain name: ".$domain->domain_name ."</strong></td>";
                                        echo "<td>". ($domain->price - $domainWithNoVatPrice) ."</td>";
                                        echo "</tr>";
                                ?>

                                        <?php 
                                        if (!empty($package)) {
                                        $packagePricing = $package->pricing;
                                        $packageWithNovatPrice = intval($package->pricing * 18 / 118);
                                        echo "<tr>";
                                        echo "<td><strong>Hosting package: ".$package->name."</strong></td>";
                                        echo "<td>".($package->pricing - $packageWithNovatPrice)."</td>";
                                        echo "</tr>";
                                        }
                                ?>
                                <tr>
                                        <td><strong>VAT ( 18% )</strong></td>
                                        <td><?= $domainWithNoVatPrice + $packageWithNovatPrice ?></td>
                                </tr>
                                <tr>
                                        <th>Total</th>
                                        <th><?= $domain->price + $packagePricing ?></th>
                                </tr>
                        </table>
                </div>
                <div class="invoice-payment-details">
                        <strong><h5>PAYMENT DETAILS</h5></strong>
                        <p>Access Bank</p>
                        <p>Account Number: 700210010133641</p>
                        <p>Swift code: BKORRWRW</p>
                        <p>Mobile Money: 0781520124</p>
                        <p>Contact: BKORRWRW</p>
                        <strong><h5>THANK YOU FOR YOUR BUSINESS</h5></strong>
                </div>
        </div>
</div>
</body>
</html>