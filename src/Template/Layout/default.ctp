<?php

$cakeDescription = 'CakePHP: the rapid development php framework';
?>

</head>

<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.15
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
  <head>

    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>CoreUI Free Bootstrap Admin Template</title>
    <!-- Icons-->




        <?= ''//$this->fetch('meta') ?>
        <?= ''//$this->fetch('css') ?>
        <?= ''//$this->fetch('script') ?>

        <?= $this->Html->script("https://use.fontawesome.com/38cb3be8bc.js") ?>


     <!-- $this->Html->meta('icon')  -->
          <!-- $this->Html->css('base.css')
          $this->Html->css('style.css')  -->
         <?= ''//$this->Html->css('home.css')   ?>]
         <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
      <!-- $this->Html->meta('icon')  -->




    <!-- <link href="https://coreui.io/demo/vendors/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="https://coreui.io/demo/vendors/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="https://coreui.io/demo/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://coreui.io/demo/vendors/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"> -->


    <!-- <link rel="icon" type="image/ico" href="./img/favicon.ico" sizes="any" /> -->

    <!-- Main styles for this application-->
    <?= $this->Html->css('style.css') ?>
    <link href="https://coreui.io/demo/3.0-alpha.1/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
    <script src="https://coreui.io/demo/3.0-alpha.1/vendors/jquery/js/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>

  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <?php echo $this->Html->link(
        $this->Html->image('imaginet_logo.png',
          array('class' => 'navbar-brand-full','width'=>'89', 'height'=>'25', 'escape' => false)),
          '/',
          array('class' => 'navbar-brand', 'escape' => false)
        )?>
      <!-- <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="img/brand/logo.svg" width="89" height="25" alt="CoreUI Logo">
        <img class="navbar-brand-minimized" src="img/brand/sygnet.svg" width="30" height="30" alt="CoreUI Logo">
      </a> -->
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Dashboard</a>
        </li>
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Users</a>
        </li>
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Settings</a>
        </li>
      </ul>
      <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img class="img-avatar" src="https://coreui.io/demo/3.0-alpha.1/assets/img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Account</strong>
            </div>
            <a class="dropdown-item" href="#">
              <i class="fa fa-bell-o"></i> Updates
              <span class="badge badge-info">42</span>
            <a class="dropdown-item" href="#">
              <i class="fa fa-lock"></i> Logout</a>
          </div>
        </li>
      </ul>
      <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
      </button>
    </header>
    <div class="app-body">
      <div class="sidebar">
        <nav class="sidebar-nav">
          <ul class="nav">
            <li class="nav-item">
              <?= $this->Html->link(
                    $this->Html->image('dashboard_icon.png',array('class' => 'nav-icon icon-speedometer', 'escape' => false)).' Dashboard',
                    ['controller' => 'Pages', 'action' => 'index'],
                    ['class' => 'nav-link', 'escape'=>false]
                   )
              ?>
            </li>
            <li class="nav-item">
              <?= $this->Html->link(
                    $this->Html->image('domain_icon.png', array('class' => 'nav-icon', 'escape' => false)).' Domains',
                    ['controller' => 'Domains', 'action' => 'index'],
                    ['class' => 'nav-link', 'escape'=>false]
                   )
              ?>
            </li>
            <li class="nav-item">
              <?= $this->Html->link(
                    $this->Html->image('clients_icon.png', array('class' => 'nav-icon', 'escape' => false)).' Clients',
                    ['controller' => 'clients', 'action' => 'index'],
                    ['class' => 'nav-link', 'escape'=>false]
                   )
              ?>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <?= $this->Html->image('settings_icon.png', array('class' => 'nav-icon', 'escape' => false))?>
                Settings
              </a>
                <ul class="nav-dropdown-items">
                  <li class="nav-item">
                    <?= $this->Html->link(
                          '<span class="nav-tree-title">User profiles</span>',
                          ['controller' => 'settings', 'action' => 'profiles'],
                          ['class' => 'nav-link', 'escape'=>false]
                         )
                    ?>
                  </li>
                  <li class="nav-item">
                    <?= $this->Html->link(
                          '<span class="nav-tree-title">Tlds</span>',
                          ['controller' => 'settings', 'action' => 'tlds'],
                          ['class' => 'nav-link', 'escape'=>false]
                         )
                    ?>
                  </li>
                  <li class="nav-item">
                    <?= $this->Html->link(
                          '<span class="nav-tree-title">Bank details</span>',
                          ['controller' => 'settings', 'action' => 'bank-details'],
                          ['class' => 'nav-link', 'escape'=>false]
                         )
                    ?>
                  </li>
                  <li class="nav-item">
                    <?= $this->Html->link(
                          '<span class="nav-tree-title">Imaginet Address</span>',
                          ['controller' => 'settings', 'action' => 'imaginet-address'],
                          ['class' => 'nav-link', 'escape'=>false]
                         )
                    ?>
                  </li>
                </ul>
            </li>

            <li class="nav-item">
              <?= $this->Html->link(
                    $this->Html->image('report_icon.png', array('class' => 'nav-icon ', 'escape' => false)).' Reporting',
                    ['controller' => 'PpParkingProducts', 'action' => 'index'],
                    ['class' => 'nav-link', 'escape'=>false]
                   )
              ?>
            </li>
            <li class="nav-item">
              <?= $this->Html->link(
                    $this->Html->image('logout_icon.png', array('class' => 'nav-icon ', 'escape' => false)).' Logout',
                    ['controller' => 'Users', 'action' => 'logout'],
                    ['class' => 'nav-link', 'escape'=>false]
                   )
              ?>
            </li>

            <!-- <li class="divider"></li> -->
          </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
      </div>

    <?= $this->fetch('content') ?>

  </div>
  <footer class="app-footer">
    <div>
      <a href="https://coreui.io">EPP</a>
      <span>&copy; 2019 Imaginet Ltd.</span>
    </div>

  </footer>
  <!-- CoreUI and necessary plugins-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
   integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
   crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="https://coreui.io/demo/3.0-alpha.1/vendors/pace-progress/js/pace.min.js"></script>
  <!-- <script src="https://coreui.io/demo/3.0-alpha.1/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js"></script> -->
  <script src="https://coreui.io/demo/2.0/vendors/@coreui/coreui-pro/js/coreui.min.js"></script> -->


  <!-- Plugins and scripts required by this view-->
  <!-- <script src="node_modules/chart.js/dist/Chart.min.js"></script>
  <script src="node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js"></script> -->
  <?= ''//$this->Html->script("main.js") ?>
  <script>
  //* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
  var dropdown = document.getElementsByClassName("dropdown-btn");
  var i;

  for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
      //  this.classList.toggle("active");
      var dropdownContent = this.getElementsByClassName('sidebar-tree-menu');
      console.log(dropdownContent);
      if (dropdownContent[0].style.display == "block") {
        dropdownContent[0].style.display = "none";
        dropdown[0].style.paddingBottom = "0.5em";
      } else {
        dropdownContent[0].style.display = "block";
        dropdown[0].style.paddingBottom = "0";
      }
    });
  }

  window.onload = function(){
      var dropdow = document.getElementsByClassName("lists");
      var aObj = document.getElementsByClassName("link");
      var i;
          for(i=0;i<aObj.length;i++) {

              if(document.location.href.indexOf(aObj[i].href)>=0) {
                  if(document.location.href === aObj[i].href){
              dropdow[i].className = "active";
              //aObj[i].className='active';
                  }

              }
              console.log("class:" + dropdow[i])
          }
      }


  </script>
</body>
</html>
