<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Clients',
            ['controller' => 'Clients', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Add New</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <section class="page-body">
        <?= $this->Form->create("Registrations",array('url'=>'/clients/add'))?>

        <h4>Account Information</h4><br>
        <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <?= $this->Form->email('email', array('class' => 'form-control', 'placeholder' => 'Email'));?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('first_name', array('class' => 'form-control', 'placeholder' => 'First name'));?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('last_name', array('class' => 'form-control', 'placeholder' => 'Last name'));?>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <?= $this->Form->password('password', array('class' => 'form-control', 'placeholder' => 'Password'));?>
        </div>

        <h4>Billing Information</h4><br>

        <div class="form-group">
            <?= $this->Form->control('company', array('class' => 'form-control', 'placeholder' => 'Organisation'));?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('address_line_1', array('class' => 'form-control', 'placeholder' => 'Address 1'));?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('address_line_2', array('class' => 'form-control', 'placeholder' => 'Address 2'));?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('city', array('class' => 'form-control', 'placeholder' => 'City'));?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('state', array('class' => 'form-control', 'placeholder' => 'State'));?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('zip_code', array('class' => 'form-control', 'placeholder' => 'Zip code'));?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('phone', array('class' => 'form-control', 'placeholder' => 'Phone Number'));?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('country', array('class' => 'form-control', 'placeholder' => 'country'));?>
        </div>

        <?= $this->Form->button('Submit', array('class' => 'btn btn-primary'))?>
        <?= $this->Form->end()?>
      </section>
    </div>
  </div>
</main>
