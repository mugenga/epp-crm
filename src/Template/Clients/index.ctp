<?php
$paginator = $this->Paginate;
?>
<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Clients',
            ['controller' => 'Clients', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Clients</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <section class="page-header">
          <div class="page-title">
              <h4>Customers</h4>
          </div>
          <div class="page-top-buttons">
              <button class="dark-grey-large-btn"><a href="/epp_crm/clients/add/"> Add new</a></button>
              <button class="dark-grey-large-btn">Export list</button>
          </div>
          <?= $this->Form->create(null,array('url'=>['controller'=>'Clients', 'action'=>'index'], 'class' =>'page-filtering'))?>

          <div class="name">
                  <label class="check"> First name:</label>
                  <input type="text" name="first_name">
          </div>
          <div class="name">
                  <label class="exp_date"> Last name:</label>
                  <input type="text" name="last_name">
          </div>
          <div class="name">
                  <label class="exp_date"> Email:</label>
                  <input type="text" name="email">
          </div>
          <div class="btn">
              <button type="submit">Search</button>
          </div>

          </form>
      </section>
      <div class="card">
        <div class="card-header">
          <i class="fa fa-align-justify"></i> List of Customers</div>
        <div class="card-body">
        <table class="table table-responsive-sm">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <!-- <th>Company</th> -->
              <th>Linked Domains</th>
            </tr>
          </thead>
          <tbody>
            <?php
                foreach($clients as $client):

                    echo "<tr>";
                    echo "<td><a href='/epp_crm/clients/view/".$client['id']."'>".$client['id']."</a></td>";
                    echo "<td><a href='/epp_crm/clients/view/".$client['id']."'>".$client['first_name']." ".$client['last_name']."</a></td>";
                    echo "<td><a href='/epp_crm/clients/view/".$client['id']."'>".$client['domains']."</a></td>";
                    echo "</tr>";

                endforeach
            ?>
          </tbody>
        </table>
        <nav>
          <ul class="pagination">
            <?= $this->Paginator->prev(__('previous') . ' >') ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
          </ul>
        </nav>
        </div>
      </div>
    </div>
  </div>
</main>
