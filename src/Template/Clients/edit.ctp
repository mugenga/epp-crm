<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Clients',
            ['controller' => 'Clients', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Edit</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <section class="page-body">

          <?= $this->Form->create("Registrations",array('url'=>'/clients/edit/'.$client->id))?>

          <h4>Account Information</h4><br>
          <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <?= $this->Form->email('email', array('class' => 'form-control', 'placeholder' => 'Email', 'value' => $client->email));?>
          </div>

          <div class="form-group">
              <?= $this->Form->input('first_name', array('class' => 'form-control', 'placeholder' => 'First name', 'value' => $client->first_name));?>
          </div>

          <div class="form-group">
              <?= $this->Form->input('last_name', array('class' => 'form-control', 'placeholder' => 'Last name', 'value' => $client->last_name));?>
          </div>

          <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <?= $this->Form->password('password', array('class' => 'form-control', 'placeholder' => 'Password'));?>
          </div>

          <h4>Billing Information</h4><br>

          <div class="form-group">
              <?= $this->Form->input('company', array('class' => 'form-control', 'placeholder' => 'Organisation', 'value' => $address->company));?>
          </div>

          <div class="form-group">
              <?= $this->Form->input('address_line_1', array('class' => 'form-control', 'placeholder' => 'Address 1', 'value' => $address->address_line_1));?>
          </div>

          <div class="form-group">
              <?= $this->Form->input('address_line_2', array('class' => 'form-control', 'placeholder' => 'Address 2', 'value' => $address->address_line_2));?>
          </div>

          <div class="form-group">
              <?= $this->Form->input('city', array('class' => 'form-control', 'placeholder' => 'City', 'value' => $address->city));?>
          </div>

          <div class="form-group">
              <?= $this->Form->input('state', array('class' => 'form-control', 'placeholder' => 'State', 'value' => $address->state));?>
          </div>

          <div class="form-group">
              <?= $this->Form->input('zipcode', array('class' => 'form-control', 'placeholder' => 'Zip code', 'value' => $address->zip_code));?>
          </div>

          <div class="form-group">
              <?= $this->Form->input('phone', array('class' => 'form-control', 'placeholder' => 'Phone Number', 'value' => $address->phone));?>
          </div>

          <div class="form-group">
              <?= $this->Form->input('country', array('class' => 'form-control', 'placeholder' => 'country', 'value' => $address->country));?>
          </div>
          <?= $this->Form->hidden('address_id', array('value' => $address->id));?>

          <?= $this->Form->button('Submit', array('class' => 'btn btn-primary'))?>
          <?= $this->Form->end()?>
      </section>
    </div>
  </div>
</main>
