<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Clients',
            ['controller' => 'Clients', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Client Info</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-8">
          <div class="card">
                <div class="card-header">Client info</div>
                <div class="card-body">
                  <table class="vertical-table">
                    <?php
                    echo "<tr><td><strong>Client id:</strong></td> <td>".$client->id."</td></tr>";
                    echo "<tr><td><strong>Email: </strong></td> <td>".$client->email."</td></tr>";
                    echo "<tr><td><strong>First name:</strong></td> <td>".$client->first_name."</td></tr>";
                    echo "<tr><td><strong>Last name:</strong></td> <td>".$client->last_name."</td></tr>";
                    echo "<tr><td><strong>Registered:</strong></td> <td>".$client->created->format('d M Y')."</td></tr>";
                    ?>
                  </table>
                </div>
          </div>
          <div class="card">
              <div class="card-header">
                <i class="fa fa-align-justify"></i> Billing Info</div>
              <div class="card-body">
                <table class="vertical-table">
                  <?php
                  echo "<tr><td><strong>Organisation:</strong></td> <td>".$address->company."</td></tr>";
                  echo "<tr><td><strong>Email:</strong></td> <td>".$client->email."</td></tr>";
                  echo "<tr><td><strong>Phone:</strong></td> <td>".$address->phone."</td></tr>";
                  echo "<tr><td><strong>Country:</strong></td> <td>".$address->country."</td></tr>";
                  echo "<tr><td><strong>City:</strong></td> <td>".$address->city."</td></tr>";
                  echo "<tr><td><strong>Address:</strong></td> <td>".$address->address_line_1."</td></tr>";

                  ?>
                </table>
              </div>
            </div>
            <div class="card">
               <div class="card-header">
                 <i class="fa fa-align-justify"></i> Linked Domains
               </div>
               <div class="card-body">
                 <table class="table table-responsive-sm">
                     <?php
                     if(count($domains) == 0){
                         echo"<tr><th>NO DOMAINS REGISTERED</th><tr>";
                     }
                         else{
                     ?>
                     <tr>
                         <th>Name</th>
                         <th>Expiring</th>
                         <th>Status</th>
                     </tr>
                     <?php
                         foreach($domains as $domain):
                             echo "<tr>";
                             echo "<td><a href='/epp_crm/domains/view/".$domain->id."'>".$domain->domain_name."</a></td>";
                             echo "<td>".$domain->expiration_date->format("d m y")."</td>";
                             echo "<td>".$domain->status."</td>";
                             echo "</tr>";

                         endforeach;


                         }
                     ?>
                 </table>
               </div>
            </div>
        </div>
        <div class="col-sm-4">
          <div class="card">
             <div class="card-header">
               <i class="fa fa-align-justify"></i> Actions
             </div>
             <div class="card-body">
               <nav class="nav flex-column">
                <?= $this->Html->link(__('Edit info'), ['controller' => 'Clients', 'action' => 'edit', $client->id] , ['class' => 'nav-link']) ?>
                <?= $this->Html->link('Add Domain', ['controller' => 'domains', 'action' => 'domain-search'], ['class' => 'nav-link'])?>
               </nav>
             </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
