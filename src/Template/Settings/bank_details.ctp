<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Settings</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Bank Details',
            ['controller' => 'Settings', 'action' => 'bankDetails'],
            ['escape'=>false]
           )
      ?>
    </li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <section class="container backwhite" style="padding-top:2rem">

      <div><?= $this->Flash->render() ?></div>

      <div class="table-responsive">
          <table class="table table-striped">
              <thead class="">
                  <tr>
                      <th scope="col"><?= $this->Paginator->sort('Account Name') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('Account Number') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('Swift Code') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('Contact') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('Mobile Money') ?></th>
                      <th>Actions</th>
                  </tr>
              </thead>

              <tbody>

                      <tr>
                          <td class=""><?= $bankdetails[0]->bank_name ?></td>
                          <td class=""><?= $bankdetails[0]->acc_number ?></td>
                          <td class=""><?= $bankdetails[0]->swift_code ?></td>
                          <td class=""><?= $bankdetails[0]->contact ?></td>
                          <td class=""><?= $bankdetails[0]->mobile_money ?></td>

                          <td>
                              <?= $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit-bank-details', $bankdetails[0]->id], ['class'=>'btn btn-sm btn-info', 'escape'=>false])?>
                          </td>
                      </tr>

              </tbody>
          </table>
      </div>
      </section>
    </div>
  </div>
</main>
