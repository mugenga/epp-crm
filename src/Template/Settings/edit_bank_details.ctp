<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Settings</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Bank Details',
            ['controller' => 'Settings', 'action' => 'bankDetails'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item">Edit </li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <div>
      <h3 class="text-center">Edit Bank Details</h3>
      <hr />

      <section class="container backwhite" style="padding-top:2rem">

      <div class="account-error col-sm-6 col-md-6 col-xs-12">
          <?= $this->Flash->render(); ?>
      </div>
      <?= $this->Form->create($bankDetail,['url' => ['controller' => 'settings', 'action' => 'editBankDetails'], $bankDetail->id]) ?>

          <div class="form-group row">
              <div class="col">
                      <?= $this->Form->control("bank_name", ['class' =>"form-control"])?>
              </div>

              <div class="col">
                      <?= $this->Form->control("acc_number", ['class'=>"form-control"]) ?>
              </div>
              <div class="col">
                      <?= $this->Form->control("swift_code", ['class'=>"form-control"]) ?>
              </div>
              <div class="col">
                      <?= $this->Form->control("contact", ['class'=>"form-control"]) ?>
              </div>
              <div class="col">
                      <?= $this->Form->control("mobile_money", ['class'=>"form-control"]) ?>
              </div>

          </div>

          <div class="form-group">
              <?= $this->Form->button(__('<i class="fa fa-save"></i> Save Changes'), ['class'=>'dark-grey-large-btn col-sm-4 col-md-4 col-xs-12']); ?>
          </div>
      <?= $this->Form->end() ?>

      </section>

      </div>
    </div>
  </div>
</main>
