<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Settings</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Profiles',
            ['controller' => 'Settings', 'action' => 'Profiles'],
            ['escape'=>false]
           )
      ?>
    </li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <section class="page-header">
          <div class="page-title">
              <h4>Users</h4>
          </div>
          <div class="page-top-buttons">
              <button class="dark-grey-large-btn">
                  <?= $this->Html->link('Add User', ['controller' =>'users', 'action' => 'add'], ['escape'=>false])?>
              </button>
          </div>
      </section>

      <section class="container backwhite" style="padding-top:2rem">

          <div><?= $this->Flash->render() ?></div>

          <div class="table-responsive">
              <table class="table table-responsive-sm">
                  <thead class="">
                      <tr>
                          <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                          <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                          <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                          <th scope="col"><?= $this->Paginator->sort('user_type') ?></th>
                          <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                          <th>Actions</th>
                      </tr>
                  </thead>

                  <tbody>
                      <?php foreach ($users as $user): ?>
                          <tr>
                              <td><?= h($user->first_name) ?></td>
                              <td><?= h($user->last_name) ?></td>
                              <td><?= h($user->email) ?></td>
                              <td><?= h($user->user_type) ?></td>
                              <td><?= h($user->created) ?></td>
                              <td class="actions">
                                  <?= $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit-profile',  $user->id], ['class'=>'btn btn-sm btn-info', 'escape'=>false])?>
                                  <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                              </td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
          </div>

          <div class="row">
              <div class="col-sm-12">
                  <div class="paginator" style="text-align: center;">
                      <center>
                          <div class="pagination" >
                              <?= $this->Paginator->prev('< ' . __('previous')) ?>
                              <?= $this->Paginator->numbers() ?>
                              <?= $this->Paginator->next(__('next') . ' >') ?>
                          </div>
                      </center>
                  <p><?= $this->Paginator->counter() ?></p>
              </div>
          </div>
          </div>

      </section>
    </div>
  </div>
</main>
