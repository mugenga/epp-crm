<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Settings</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Tlds',
            ['controller' => 'Settings', 'action' => 'tlds'],
            ['escape'=>false]
           )
      ?>
    </li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <section class="container backwhite" style="padding-top:2rem">

      <div><?= $this->Flash->render() ?></div>

      <div class="table-responsive">
          <table class="table table-striped">
              <thead class="">
                  <tr>
                      <th scope="col"><?= $this->Paginator->sort('tld') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('price') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('f_price') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                      <th>Actions</th>
                  </tr>
              </thead>

              <tbody>
                  <?php foreach($tlds as $tld): ?>
                      <tr>
                          <td class=""><?= $tld->tld ?></td>
                          <td class=""><?= $this->Number->currency($tld->price, "RWF") ?></td>
                          <td class=""><?= $this->Number->currency($tld->f_price, "USD") ?></td>
                          <td class=""><?= $tld->type ?></td>

                          <td>
                              <?= $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'editTld', $tld->id], ['class'=>'btn btn-sm btn-info', 'escape'=>false])?>
                          </td>
                      </tr>
                  <?php endforeach; ?>
              </tbody>
          </table>
      </div>

      <div class="row">
          <div class="col-sm-12">
              <div class="paginator" style="text-align: center;">
                  <center>
                      <div class="pagination" >
                          <?= $this->Paginator->prev('< ' . __('previous')) ?>
                          <?= $this->Paginator->numbers() ?>
                          <?= $this->Paginator->next(__('next') . ' >') ?>
                      </div>
                  </center>
              <p><?= $this->Paginator->counter() ?></p>
          </div>
      </div>
      </div>

      </section>
    </div>
  </div>
</main>
