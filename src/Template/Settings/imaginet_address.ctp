<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Settings</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Imaginet Address',
            ['controller' => 'Settings', 'action' => 'ImaginetAddress'],
            ['escape'=>false]
           )
      ?>
    </li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <section class="container backwhite" style="padding-top:2rem">
      <div class="table-responsive">
          <table class="table table-responsive-sm">
              <thead class="">
                  <tr>
                      <th scope="col"><?= $this->Paginator->sort('Company Name') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('Tin no') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('Address') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('Phone') ?></th>
                      <th scope="col"><?= $this->Paginator->sort('Email') ?></th>
                      <th>Actions</th>
                  </tr>
              </thead>

              <tbody>

                      <tr>
                          <td class=""><?= $address_details[0]->company_name ?></td>
                          <td class=""><?= $address_details[0]->tin_no ?></td>
                          <td class=""><?= $address_details[0]->address ?></td>
                          <td class=""><?= $address_details[0]->phone ?></td>
                          <td class=""><?= $address_details[0]->email ?></td>

                          <td>
                              <?= $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit-address-details', $address_details[0]->id], ['class'=>'btn btn-sm btn-info', 'escape'=>false])?>
                          </td>
                      </tr>

              </tbody>
          </table>
      </div>
      </section>
    </div>
  </div>
</main>
