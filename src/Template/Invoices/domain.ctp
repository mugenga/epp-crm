<?php
//echo $domains[0]['id'];
?>
<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Domains',
            ['controller' => 'Domains', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Invoice</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <div class="animated fadeIn">
      <section class="page-header">
          <div class="page-title">
              <h4>Invoices</h4>
          </div>
      </section>

      <section class="page-body">
          <table class="table table-responsive-sm">
            <thead>
              <tr>
                  <th>Invoice no</th>
                  <th>Billing period</th>
                  <th>Bill created on</th>
                  <th>Due on</th>
                  <th>Payment status</th>
                  <!-- <th>Deleted after</th> -->
              </tr>
            </thead>
            <tbody>
              <?php
              //echo print_r($invoices);
                  foreach($invoices as $invoice):
                      echo "<tr>";
                      echo "<td><a href='/epp_crm/invoices/view/". $invoice->id."'>".$invoice->id ."</a></td>";
                      echo "<td>".date('d F Y', strtotime($invoice->due))." - ". date('d F Y', strtotime('+1 years', strtotime($invoice->due))) ."</td>";
                      echo "<td>". $invoice->created->format("d F Y") ."</td>";
                      echo "<td>". $invoice->due->format("d F Y") ."</td>";
                      echo ($invoice->paid) ? "<td>Paid</td>" : "<td>Unpaid</td>";
                      echo "</tr>";

                  endforeach
              ?>
            </tbody>
          </table>

      </div>
      </section>
    </div>
  </div>
</main>
