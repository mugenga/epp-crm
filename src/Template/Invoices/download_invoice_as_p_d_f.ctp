<?php

class xtcpdf extends TCPDF {

}

    
        // // check if domain has a hosting package
        // // If yes get package info
        // if ($domain->package == "NULL") {
        //     $package = "";
        // } else {
        //     $package = $this->Packages->find('all')
        //     ->where(['name'=>$domain->package_name])->first();
        // }

        // // LOGIC Variables to go in the HTML
        $header_image = WWW_ROOT.'img\imaginet_logo.png';
        $stamp = WWW_ROOT.'img\imaginetstamp.png';
        $invoice_id = $invoice->id;
        $paid = ($invoice['paid']) ? 'Paid':'Not Paid';
        $domain_user_email = $domain->order->user->email;


        $invoice_created =  (!empty($invoice->created)) ? $invoice->created->format('M d Y') :'';
        $invoice_due = (!empty($invoice->due)) ? $invoice->due->format('M d Y') : '';

        $domainWithNoVatPrice = intval(($domain['price'] * 18 / 118));
        $packageWithNovatPrice = 0;
        $packagePricing = 0;

        $amount = $domain->price - $domainWithNoVatPrice;

        $packageHTML = '';
        // If the domain has a hosting package associated
        if (!empty($package)) {
             $packagePricing = $package->pricing;
             $packageWithNovatPrice = intval($package->pricing * 18 / 118);

             $packageHTML .= '<tr>';
             $packageHTML .= '<td style="text-align:left"><strong>Hosting package: '.$package->name.'</strong></td>';
             $packageHTML .= '<td>';
             $packageHTML .= $package->pricing - $packageWithNovatPrice;
             $packageHTML .= '</td>';
             $packageHTML .= '</tr>';
         }

         //Calculate VAT
         $vat = $domainWithNoVatPrice + $packageWithNovatPrice;
         //Calculate total
         $total = $domain->price + $packagePricing;



        // create new PDF document
        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        mb_internal_encoding('UTF-8');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Imaginet LTD');
        $pdf->SetTitle("Invoice");


        // Add a page
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->AddPage();

        // LOAD CSS
        $html = '';
        $html .= '<style>'.file_get_contents('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' ).'</style>';
        $html .= '<style>'.file_get_contents(WWW_ROOT.'css\home.css' ).'</style>';

        // HTML to be vonverted in PDF
        $html .= <<<EOD

<div class="invoice_view popup">
        <h2 style="text-align:center">Invoice</h2> 
        <div class="content">
                <div class="invoice-logo">
                    <img src="$header_image" alt="$header_image" width="100" height="70" style=" border:1px solid red;">
                </div>
                <table border="0" style="padding-left: 10px; padding-bottom: 10px;">
                <tr style="font-size:0.7em;">
                <td style=""> 
                    <p>IMAGINET LTD</p>
                    <p>TIN No:101917017</p>
                    <p>KG 230 ST</h5>
                    <p>TELE 10 BLDG, GISHUSHU - REMERA</p>
                    <p>Phone 0788305200   Email info@imaginet.rw</p>
                </td>
                <td style="text-align: right"> 
                    <p>INVOICE</p>
                    <p>Invoice date: $invoice_created </p>
                    <p>Invoice Due Date: $invoice_due</p>
                    <p>Invoice No: $invoice_id</p>
                    <p>Invoice Status: $paid </p>
                </td>
                </tr>
                </table>
                <table border="0" style="padding-left: 10px; padding-bottom: 15px;">
                <tr style="font-size:0.8em;">
                    <td style=""> 
                    <p>Bill to:  $billingAddress->company </p>
                    <p>Attn: Registrant</p>
                    <p>Email: $domain_user_email  </p>
                    <p>$billingAddress->phone </p>
                </td>
                </tr>
                </table>

                <div class="invoice-body">
                    <table class="stripped-table" style="padding:3px;">
                            <tr style="font-size:0.5em;">
                                <th style="text-align:left">Description</th>
                                <th>Amount(Rwf)</th>
                            </tr>

                            
                            <tr>
                                <td style="text-align:left"><strong>Domain name: $domain->domain_name </strong></td>
                                <td>$amount</td>
                            </tr>
                            $packageHTML
        
                            <tr>
                                <td style="text-align:left">VAT ( 18% )</td>
                                <td> $vat </td>
                            </tr>
                            <tr>
                                <th style="text-align:left">Total</th>
                                <th>$total</th>
                            </tr>
                    </table>
                </div>
                <table border="0" style="padding-left: 10px; padding-bottom: 1px;">
                <tr style="font-size:0.7em;">
                <td style=""> 
                    <div class="invoice-payment-details">
                        <strong><h5>PAYMENT DETAILS</h5></strong>
                        <p>Access Bank</p>
                        <p>Account Number: 700210010133641</p>
                        <p>Swift code: BKORRWRW</p>
                        <p>Mobile Money: 0781520124</p>
                        <p>Contact: BKORRWRW</p>
                    </div>
                </td>
                <td style="text-align: left"> 
                     <img src="$stamp" alt="$stamp" width="150" height="150"/>
                </td>
                </tr>
                </table>
                <div class="invoice-payment-details">
                        <strong><h5>THANK YOU FOR YOUR BUSINESS</h5></strong>
                </div>
        </div>
</div>
EOD;

            // Print text using writeHTMLCell()
            $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            // ---------------------------------------------------------

            // Close and output PDF document
           
            $pdf->Output(WWW_ROOT. 'invoices\\'. $invoice->location,'F');