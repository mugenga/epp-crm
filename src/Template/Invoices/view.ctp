<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
      <?= $this->Html->link(
            'Domains',
            ['controller' => 'Domains', 'action' => 'index'],
            ['escape'=>false]
           )
      ?>
    </li>
    <li class="breadcrumb-item active">Domains</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
        <a class="btn" href="#">
          <i class="icon-speech"></i>
        </a>
        <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
        <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
      </div>
    </li>
  </ol>

  <div class="container-fluid">
    <?= $this->Flash->render() ?>
    <div class="animated fadeIn">
      <div class="invoice_view popup">
              <h5>Invoice</h5>
              <div class="content">
                      <div class="invoice-logo">
                              <?= $this->Html->image('imaginet_logo.png')?>
                      </div>
                      <div class="invoice-header">
                              <div class="company-info">
                                      <strong><h5><?= $invoiceAddressDetails[0]->company_name ?></h5></strong>
                                      <strong><h5>TIN No:<?= $invoiceAddressDetails[0]->tin_no ?></h5></strong>
                                      <h5><?= $invoiceAddressDetails[0]->address ?></h5>
                                      <!-- <h5>TELE 10 BLDG, GISHUSHU - REMERA</h5> -->
                                      <h5>Phone <?= $invoiceAddressDetails[0]->phone ?>   <?= $invoiceAddressDetails[0]->email ?></h5>
                              </div>
                              <div class="invoice-info">
                                      <h5>INVOICE</h5>
                                      <h5>Invoice date: <?php if(!empty($invoice->created)) echo $invoice->created->format('M d Y'); ?></h5>
                                      <h5>Invoice Due Date: <?php if(!empty($invoice->due)) echo $invoice->due->format('M d Y'); ?></h5>
                                      <h5>Invoice No: <?= $invoice->id ?></h5>
                                      <h5>Invoice Status: <?= $invoice->paid ? 'Paid':'Not Paid' ?></h5>
                              </div>
                      </div>
                      <div class="invoice-receiver-info">
                              <p>Bill to: <?= $billingAddress->company ?></p>
                              <p>Attn: Admin</p>
                              <p>Email: <?= $domain->order->user->email ?></p>
                              <p> <?= $billingAddress->phone ?></p>
                      </div>
                      <div class="invoice-body">
                          <table class="table table-responsive-sm">
                            <thead>
                              <tr>
                                <th>Description</th>
                                <th>Amount(Rwf)</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
                              $domainWithNoVatPrice = intval(($invoice->domain_price * 18 / 118));
                              $packageWithNovatPrice = 0;
                              $packagePricing = 0;
                              echo "<tr>";
                              echo "<td>Domain name: <strong>".$domain->domain_name ."</strong></td>";
                              echo "<td>". ($invoice->domain_price - $domainWithNoVatPrice) ."</td>";
                              echo "</tr>";
                            ?>

                            <?php
                              if (!empty($package)) {
                                $packagePricing = $invoice->hosting_price;
                                $packageWithNovatPrice = intval($invoice->hosting_price * 18 / 118);
                                echo "<tr>";
                                echo "<td>Hosting package: <strong>".$package->name."</strong></td>";
                                echo "<td>".($invoice->hosting_price- $packageWithNovatPrice)."</td>";
                                echo "</tr>";
                              }
                            ?>
                            <tr>
                              <td>sub-total</td>
                              <td> <?php
                              echo ($invoice->domain_price - $domainWithNoVatPrice) + ($invoice->hosting_price- $packageWithNovatPrice );
                              ?></td><br>
                            </tr>
                            <tr>
                              <td>VAT ( 18% )</td>
                              <td><?= $domainWithNoVatPrice + $packageWithNovatPrice ?></td>
                            </tr>
                            <tr>
                              <th>Total</th>
                              <th><?= $invoice->domain_price + $invoice->hosting_price?></th>
                            </tr>
                          </table>
                      </div>
                      <div class="invoice-payment-details">
                              <strong><h5>PAYMENT DETAILS</h5></strong>
                              <p><?= $bankdetails[0]->bank_name ?></p>
                              <p>Account Number: <?= $bankdetails[0]->acc_number ?></p>
                              <p>Swift code: <?= $bankdetails[0]->swift_code ?></p>
                              <p>Mobile Money: <?= $bankdetails[0]->mobile_money ?></p>
                              <p>Contact: <?= $bankdetails[0]->contact ?></p>
                              <strong><h5>THANK YOU FOR YOUR BUSINESS</h5></strong>
                      </div>
              </div>
              <button class="dark-grey-large-btn"><?= $this->Html->link(__('Send Invoice'), ['controller' => 'Invoices', 'action' => 'InvoiceEmail', $invoice->id]) ?></button>
              <button class="dark-grey-large-btn"><?= $this->Html->link(__('Download PDF'), ['controller' => 'Invoices', 'action' => 'downloadInvoiceAsPDF', $invoice->id]) ?></button>
              <button class="dark-grey-large-btn"><?= $this->Html->link(__('Mark As Paid'), ['controller' => 'Invoices', 'action' => 'MarkAsPaid', $invoice->id]) ?></button>
      </div>
    </div>
  </div>
</main>
