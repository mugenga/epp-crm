<?php
namespace App\View\Helper;

use Cake\View\Helper;

class PaginateHelper extends Helper
{
    public $helpers = [
        'Paginator' => ['templates' => 'paginator-templates']
    ];
}