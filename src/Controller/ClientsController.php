<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
/**
 * Users Controller
 *
 *
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClientsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public $components = array('Paginator');
    public function index()
    {

        $filters = [];
        // Domain filter conditions

        // Check if they are filters are set
        if($this->request->is('post')){
            if($this->request->getData('first_name')){
               $filters['first_name LIKE'] = '%'.$this->request->getData('first_name').'%';
            }
            if($this->request->getData('last_name')){
                $filters['last_name LIKE'] = '%'.$this->request->getData('last_name').'%';
            }
            if($this->request->getData('email')){
                $filters['email LIKE'] = '%'.$this->request->getData('email').'%';
            }

        }

        $this->paginate = array(
            'limit' => 8,
            'order' => array('id' => 'asc'),
            'where' => 'Users.id = 1',
            'join' => array(
                 array(
                    'table' => 'orders',
                    'alias' => 'domains',
                    'type' => 'LEFT',
                    'conditions' => 'Users.id = domains.user_id '
                 )
            ),
            'fields' => array(
                'Users.id',
                'Users.first_name',
                'Users.last_name',
                'domains' => 'COUNT(domains.user_id)'


            ),
            'group' => 'Users.id',
            'conditions' =>  $filters

        );
        $rows = $this->paginate('Users');
        $this->set('clients',$rows);
    }


    public function add()
    {

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            //check user email address already exist
            $exist = $this->Users->find("all")->where(["email"=>$this->request->data["email"]])->count();
            if($exist > 0) {
                $this->Flash->error("Sorry, the email address your provided is already registered with our system");
                $this->set(compact('user'));
                return;
            }
            $this->request->data['activation_code'] = 'null';
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if($this->Users->save($user)) {

                $this->request->data["user_id"] =  $user['id'];
                $this->request->data["order_details_id"] =  -1;
                $address = $this->Address->newEntity();
                $address = $this->Address->patchEntity($address, $this->request->getData());

                if(!$this->Address->save($address)){
                    $this->Flash->error("Sorry, an error occured while updating your default address");
                }
                else{

                    $this->request->data["address_id"] =  $address['id'];
                    $this->request->data["address_type"] =  "default";

                    $addressTypes = $this->AddressTypes->newEntity();
                    $addressTypes = $this->AddressTypes->patchEntity($addressTypes, $this->request->getData());
                    $this->Flash->success("Your  address has been succesfully updated");

                    if($this->AddressTypes->save($addressTypes)){
                    $this->redirect(array("controller" => "Clients",
                    "action" => "view",
                    $user['id']
                    ));
                    }
                }

            }
            else {
            $this->Flash->error(__('Could not create account. Please make sure your passwords meet the below criteria and are the same.'));
            return $this->redirect(['action'=>'add']);
            }
        }

    }



    public function view($client_id){
        $client = TableRegistry::get('users');
        $address = TableRegistry::get('address');
        //$domains = TableRegistry::get('orders');

        $client = $client->find()->where(['id' => $client_id])->first();
        $address = $address->find()->where(['user_id' => $client_id])->first();
        //$domains = $domains->find()->where(['user_id' => $client_id])->all();

        $domains = $this->OrderDetails->find('all')
        ->contain(['Orders', 'Orders.Users'])
        ->Where('Orders.user_id = '.$client_id)
        ->toArray();

        $this->set('client', $client)->set('address', $address)->set('domains', $domains);
    }

    public function edit($client_id){
        if($this->request->is('post')){
              $existinguser = $this->Users->get($client_id);
              $user = $this->Users->patchEntity($existinguser, $this->request->getData());

              if($this->Users->save($user)) {

                $this->request->data["user_id"] =  $user['id'];
                $this->request->data["order_details_id"] =  -1;
                $address_id = $this->request->data('address_id');
                $existingaddress = $this->Address->get($address_id);

                $address = $this->Address->patchEntity( $existingaddress, $this->request->getData());

                if(!$this->Address->save($address)){
                    $this->Flash->error("Sorry, an error occured while updating your default address");
                }
                else{
                    $this->Flash->success("Customer info updated successfully");
                    $this->redirect(array("controller" => "Clients",
                    "action" => "view",
                    $user['id']
                    ));
                }

            }

        }
        else{
        $client = TableRegistry::get('users');
        $address = TableRegistry::get('address');

        $client = $client->find()->where(['id' => $client_id])->first();
        $address = $address->find()->where(['user_id' => $client_id])->first();

        $this->set('client', $client)->set('address', $address);
        }

    }


}
