<?php
    namespace App\Controller;

    use App\Controller\AppController;
    use Cake\ORM\TableRegistry;
    use Cake\Mailer\Email;

    class InvoicesController extends AppController
    {

        public function view($invoice_id) {
            //$invoice = $this->Invoices->find('all')
            //->where(['invoice_id' => $invoice_id]);

            $invoice = TableRegistry::get('invoices');
            $invoice = $invoice->find()->where(['id'=> $invoice_id])->first();

            // get invoice info
            $domain = $this->OrderDetails->find('all')->contain(['Orders', 'Orders.Users'])
            ->Where(['OrderDetails.id'=>$invoice->order_details_id])->first();

            // ->andWhere('DATEDIFF(due, NOW()) < 60')
            // ->andWhere('DATEDIFF(NOW(), due) < 300')->order(['due' => 'DESC'])
            // ->first();
            // Get billing address if there is an invoice
            $billingAddress = "";
            if (!empty($invoice)) {
                $billingAddress = $this->Address->find('all')
                ->where(['id'=>$invoice->address_id])->first();
            }
            // check if domain has a hosting package
            // If yes get package info
            if ($domain->package == "NULL") {
                $package = "";
            } else {
                $package = $this->Packages->find('all')
                ->where(['name'=>$domain->package_name])->first();
            }

             // Get address for invoice
            $invoiceAddressDetails = $this->ImaginetAddress->find('all')->toArray();
            $bankdetails = $this->Bankdetails->find('all')->toArray();

            $this->set('invoice', $invoice)->set('billingAddress', $billingAddress)->set('domain', $domain)->set('package', $package)
            ->set('invoiceAddressDetails', $invoiceAddressDetails)->set('bankdetails', $bankdetails);
        }

        public function domain($domain_id) {

            $invoices = $this->Invoices->find('all')
            //->contain('Invoices')
            ->where(['order_details_id'=>$domain_id])
            ->order(['created' => 'DESC'])
            ->toArray();

            $this->set('invoices', $invoices);
        }



        //This function sends an invoice to the user, it gets an invoice id as argument
       public function InvoiceEmail($invoice_id){

            // Get invoice to be sent from the database
            $invoice = TableRegistry::get('invoices');
            $invoice = $invoice->find()->where(['id'=> $invoice_id])->first();

            // get domain info of the selected invoice
            $domain = $this->OrderDetails->find('all')->contain(['Orders', 'Orders.Users'])
            ->Where(['OrderDetails.id'=>$invoice->order_details_id])->first();

            // get billing address related to the invoice
            $billingAddress = "";
            if (!empty($invoice)) {
                $billingAddress = $this->Address->find('all')
                ->where(['id'=>$invoice->address_id])->first();
            }

            // Check if domain has a hosting package
            // If yes get package info
            if ($domain->package == "NULL") {
                $package = "";
            } else {
                $package = $this->Packages->find('all')
                ->where(['name'=>$domain->package_name])->first();
            }

            // Send email to customer
            $email = new Email('default');
            $email->setAttachments(WWW_ROOT. 'invoices/'. $invoice->location);
            $email->setFrom(['info@imaginet.rw' => 'Imaginet Limited'])
            ->setTo($domain->order->user->email)
            ->addTo("info@imaginet.rw")
            ->setSubject('Imaginet Ltd: Your yearly invoice for:  '.$domain->domain_name);
            $email->viewBuilder()->setTemplate('invoice_mail')
                    ->setVars([
                            'invoice' => $invoice,
                            'billingAddress' => $billingAddress,
                            'domain'=> $domain,
                            'package'=> $package
                    ]);
            $email->setEmailFormat('html');
            if($email->send()){
                $this->Flash->success("Email sent successfully");
                $this->redirect(array("controller" => "Domains",
                        "action" => "view",
                        $domain->id
                        ));
            }
            else{
                $this->Flash->error("Sending email failed");
            }

            //
            $this->autoRender = false;
        }

        public function downloadInvoiceAsPDF($invoice_id){

            $invoice = TableRegistry::get('invoices');
            $invoice = $invoice->find()->where(['id'=> $invoice_id])->first();
          // echo $invoice->location;
            //$filePath = WWW_ROOT .'invoices'. DS . $invoice->location;
            //echo $filePath;
            $file = explode("/",$invoice->location);
            $filePath = WWW_ROOT. DS .'invoices'. DS .$invoice->location;
            //echo $filePath;
        //     //die();
             $response = $this->response->withFile($filePath,
                 ['download' => true,'name' => $file[1]]
             );
            return $response;
        }

       public function MarkAsPaid($invoice_id){
         $this->request->data["paid"] = true;
         $invoices = $this->Invoices->get($invoice_id);
         $invoices = $this->Invoices->patchEntity($invoices, $this->request->getData());

         if(!$this->Invoices->save($invoices)){
               $this->Flash->error("Sorry, an error occured while updating invoice");
         }else{
               $this->Flash->success("Invoice info updated successfully");
               $this->redirect(array("controller" => "Invoices",
               "action" => "view",
               $invoices['id']
               ));
         }
       }

    }
