<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\ORM\TableRegistry;

use Cake\Mailer\Email;

use Cake\Core\App;

require(ROOT . '/vendor' . DS  . 'tecnickcom' . DS . 'tcpdf' . DS . 'tcpdf.php');

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DomainsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public function index() {
        $filters = [];
        // Domain filter conditions

        // Check if they are filters are set
        if($this->request->is('post')){
            if($this->request->getData('domain_name')){
               $filters['domain_name LIKE'] = '%'.$this->request->getData('domain_name').'%';
            }
            if($this->request->getData('expiration_from')){
               $datefrom = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $this->request->getData('expiration_from'))));
               $filters['expiration_date >'] = $datefrom;
            }
            if($this->request->getData('expiration_to')){
               $dateto = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $this->request->getData('expiration_to'))));
               $filters['expiration_date <'] = $dateto;
            }
            if($this->request->getData('domain_status')){
                $filters['OrderDetails.status ='] = $this->request->getData('domain_status');
            }
        }
        if($this->request){
            $q = $this->request->getQuery('q');
        }else{
            $condition = "";
        }
        if($q == "expiring-7-days") $condition = "DATEDIFF(expiration_date, NOW()) <=7 and DATEDIFF(expiration_date, NOW()) >0";
        elseif($q == "expiring-30-days") $condition = "DATEDIFF(expiration_date, NOW()) <=30 and DATEDIFF(expiration_date, NOW()) >0";
        elseif($q == "expired") $condition = 'expiration_date < NOW()';
        elseif($q == "pending") $condition = 'is_registered  = 0';
        else $condition = "";

        $domains = $this->OrderDetails->find('all')->contain(['Orders', 'Orders.Users'])
        ->where($condition)
        ->andWhere($filters)
        ->order(['OrderDetails.expiration_date'=>'ASC']);
        $domains = $this->paginate($domains);
        $this->set('domains',$domains);
    }

    public function search() {

    }

    public function view($domain_id) {

        // Get domain info
        $domain = $this->OrderDetails->find('all')->contain(['Orders', 'Orders.Users'])
        ->Where(['OrderDetails.id'=>$domain_id])->first();

        // Invoice Status

        // get invoice info
        $invoice = TableRegistry::get('invoices');
        // $invoice = $invoice->find()->where(['order_details_id'=> $domain_id])
        // ->andWhere('DATEDIFF(due, NOW()) < 60')
        // ->andWhere('DATEDIFF(NOW(), due) < 300')->order(['due' => 'DESC'])
        // ->first();

        $invoice = $invoice->find()->where(['order_details_id'=> $domain_id])
        ->order(['due' => 'DESC'])
        ->first();

        $createinvoice = false;
        $now = time(); // or your date as well
        $your_date = strtotime($invoice['due']);
        $datediff = $your_date - $now;

        if(!$invoice)
        {
          $createinvoice = true;
        }
        else if($invoice && round($datediff / (60 * 60 * 24)) < 30 && $invoice['paid'])
        {
          $createinvoice = true;
        }
        else $createinvoice = false;

        // Get billing address if there is an invoice
        $billingAddress = "";
        if (!empty($invoice)) {
            $billingAddress = $this->Address->find('all')
            ->where(['id'=>$invoice->address_id])->first();
        }
        // check if domain has a hosting package
        // If yes get package info
        if ($domain->package == "NULL") {
            $package = "";
        } else {
            $package = $this->Packages->find('all')
            ->where(['name'=>$domain->package_name])->first();
        }
        // Get domain nameservers
        $nameservers = $this->NameServers->find("all")->where(['domain_name'=>$domain->domain_name])->order(['id'=>'ASC'])->toArray();
        //Get the domain addresses here
        $addresses = $this->Address->find("all")
        ->contain('AddressTypes')
        ->where([
            'user_id'=>$domain->order->user->id,
            'Address.id'=>$domain->order->address_id
            //'AddressTypes.address_type <>'=>'default',
            //'Address.order_details_id' => $domain_id
        ]);
        //we check to see if addresses are old onces. In that case they wouldn't have domain IDs and the above
        //query will return account of zero so we switch to get the old addresses which has domain ids of 0
        if ($addresses->count() == 0) {
        $addresses = $this->Address->find("all")
                ->contain('AddressTypes')
                ->where([
                    'user_id'=>$domain->order->user->id,
                    //'AddressTypes.address_type <>'=>'default',
                    'Address.order_details_id' => 0
                ]);
        }

        // Get address for invoice
        $invoiceAddressDetails = $this->ImaginetAddress->find('all')->toArray();
        $bankdetails = $this->Bankdetails->find('all')->toArray();



        $this->set('domain',$domain)->set('nameservers', $nameservers)->set('addresses', $addresses)->set('invoice', $invoice)->set('billingAddress', $billingAddress)
        ->set('package', $package)->set('invoiceAddressDetails', $invoiceAddressDetails)->set('bankdetails', $bankdetails)
        ->set('createinvoice', $createinvoice);
    }

    public function createinvoice($domain_id) {
        // Check if user submitted

        if ($this->request->is('post')) {

            // Get post data
            $order_details_id = $this->request->getData('order_details_id');
            $address_id = $this->request->getData('address_id');
            $domain_price = $this->request->getData('domain_price');
            $hosting_price = $this->request->getData('hosting_price');
            $due = $this->request->getData('due');
            $timestamp = strtotime($due);

            // Create new invoice
            $invoicing = TableRegistry::get('invoices');
            $invoice = $invoicing->newEntity();
            $invoice->order_details_id = $order_details_id;
            $invoice->domain_price = $domain_price;
            $invoice->hosting_price = $hosting_price;
            $invoice->address_id = $address_id;
            $invoice->due = date("Y-m-d H:i:s", $timestamp);

            // If invoice created redirect
            if ($invoicing->save($invoice)) {

                $filename= "invoice_".$invoice['id'].".pdf";
                $foldername = date('F-Y');

                if (!file_exists(WWW_ROOT.'invoices/'.$foldername)) {
                mkdir(WWW_ROOT.'invoices/'.$foldername, 0777, true);
                }
                $filelocation = WWW_ROOT.'invoices/'.$foldername;

                $invoice->location = $foldername.'/'.$filename;
                if ($invoicing->save($invoice)) {
                    $fileNL = $filelocation."/".$filename;
                }

                $this->pdfView($invoice->id, $fileNL);

                $this->Flash->success("Invoice created successfully");
                $this->redirect(array("controller" => "Domains",
                        "action" => "view",
                        $domain_id
                        ));
                }else{
                    $this->Flash->error("Sorry, an error occured while creating invoice");
                }
        }

        // Else
        else {
         // Get domain info
         $domain = $this->OrderDetails->find('all')->contain(['Orders', 'Orders.Users'])
         ->Where(['OrderDetails.id'=>$domain_id])->first();

         // Search for domain Price
         $domain_extension = explode(".", $domain->domain_name);
         $domain_price = $this->TopLevelDomain->find('all')->where(['tld'=>".".$domain_extension[1]])->first();
         //search for hosting price
         $package_price = $this->Packages->find('all')->Where(['name'=>$domain->package_name])->first();
         // Get available hosting packages
         $packages = $this->Packages->find('all');

        // Get billing addresses
        $address = $this->Address->find('all')->contain(['Users', 'AddressTypes'])
        ->Where(['Users.id' => $domain->order->user->id]);
        //we check to see if addresses are old onces. In that case they wouldn't have domain IDs and the above
        //query will return account of zero so we switch to get the old addresses which has domain ids of 0
        if ($address->count() == 0) {
        $address = $this->Address->find("all")
                ->contain('AddressTypes')
                ->where([
                    'user_id'=>$domain->order->user->id,
                    'Address.order_details_id' => 0
                ]);
        }

        $this->set('domain', $domain)->set('address', $address)->set('packages', $packages)
             ->set('domain_price', $domain_price)->set('package_price', $package_price);
        }
    }

    public function domainSearch() {
        set_time_limit(0);
    	$searchResults = [];
        if($this->request->is('post') || $this->request->is('ajax')){

            $type = $this->request->getData('type');

            $data['requesttype'] = "domainsearch";

            $auth[0]['useremail'] = "rockyyves@gmail.com";
            $auth[0]['password'] = "Clovene!312#";
            $auth[0]['timestamp'] = "20140812";

            $parameters[0]['domainname'] =  $this->request->getData('search_term');
            $parameters[0]['domaintype'] =  $type;

            $data['authentication'] = $auth;
            $data['parameters'] = $parameters;

            $datajson = json_encode($data);

            $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"http://imaginet.rw/secureapi/thirdpartyapi/");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $datajson);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec ($ch);
            curl_close($ch);

            $response = json_decode($response, true);
            $searchResults = $response['domainsearchresults'];

        }
        $this->set(compact('searchResults'));

    }

    public function registration() {

        $packages = $this->Packages->find('all');

        if(!$this->request->getSession()->read("domain_name")) {
            if($this->request->is('post')) {
            $domain_name = $this->request->getData('domain_name');
            $price = $this->request->getData('price');
            $this->set('domain_name', $domain_name)->set('price', $price)->set('packages', $packages);
            }
        }
        else {

            $domain_name = $this->request->getSession()->read("domain_name");
            $price = $this->request->getSession()->read("price");
            $this->request->getSession()->delete('domain_name');
            $this->request->getSession()->delete('price');
            $this->set('domain_name', $domain_name)->set('price', $price)->set('packages', $packages);
        }
    }

    public function handleDomainRegistration(){

        if($this->request->is('post')) {

            $domain_name = $this->request->getData('domain_name');
            $price = $this->request->getData('amount');
            $address_id = $this->request->getData('address_id');

            if(!$this->request->getData('address_id')) {
                $this->Flash->error('Please add a registrant address');
                $this->request->session()->write('domain_name', $domain_name);
                $this->request->session()->write('price', $price);
                return $this->redirect(["action" => "registration"]);
            }

            if(!$this->request->getData('server_name_1') && !$this->request->getData('server_name_2')) {
                $this->Flash->error("Nameservers can't be empty");
                $this->request->session()->write('domain_name', $domain_name);
                $this->request->session()->write('price', $price);
                return $this->redirect(["action" => "registration"]);
            }

            //check if domain_name arleady ordered
            $exist = $this->OrderDetails->find("all")->where(["domain_name"=>$domain_name])->count();
            if($exist > 0) {
                $this->Flash->error("Domain already ordered");
                $this->request->getSession()->write('domain_name', $domain_name);
                $this->request->getSession()->write('price', $price);
                return $this->redirect(["action" => "registration"]);
            }

            $this->request->data['discount'] = 0;
            $orderEntity = $this->Orders->newEntity();
            $orderEntity = $this->Orders->patchEntity($orderEntity, $this->request->getData());

            if($this->Orders->save($orderEntity)){
                $this->request->data['order_id'] = $orderEntity['id'];
                $this->request->data['price'] = $this->request->data['amount'];
                $this->request->data['package_name'] = 'bronze';
                $this->request->data['is_registered'] = '0';
                $this->request->data['expiration_date'] =  date('Y-m-d', strtotime('+'.$this->request->data['quantity'].' years'));

                $orderDetailsEntity = $this->OrderDetails->newEntity();
                $orderDetailsEntity = $this->OrderDetails->patchEntity($orderDetailsEntity, $this->request->data());

                if($this->OrderDetails->save($orderDetailsEntity)){
                    //update nameservers
                    // $orderEntity = $this->Orders->newEntity();
                    // $orderEntity = $this->Orders->patchEntity($orderEntity, $this->request->getData());
                    // if($this->Orders->save($orderEntity)){
                        $nameServersEntity = $this->NameServers->newEntity();
                        $nameservers['server_name'] = $this->request->getData('server_name_1');
                        $nameservers['ip_address'] = $this->request->getData('ip_address_1');
                        $nameservers['domain_name'] = $this->request->getData('domain_name');
                        $nameServer1Entity = $this->NameServers->patchEntity($nameServersEntity, $nameservers);

                        $save1 = $this->NameServers->save($nameServer1Entity);

                        $nameServersEntity2 = $this->NameServers->newEntity();
                        $nameserver2['server_name'] = $this->request->getData('server_name_2');
                        $nameserver2['ip_address'] = $this->request->getData('ip_address_2');
                        $nameserver2['domain_name'] = $this->request->getData('domain_name');
                        $nameServer2Entity = $this->NameServers->patchEntity($nameServersEntity2, $nameserver2);

                        $save2 = $this->NameServers->save($nameServer2Entity);
                        if($save1 && $save2) {
                            //echo handle registration here

                            $data['requesttype'] = "registerdomain";

                            $auth[0]['useremail'] = "rockyyves@gmail.com";
                            $auth[0]['password'] = "Clovene!312#";
                            $auth[0]['timestamp'] = "20140812";

                            $parameters[0]['domainname'] = $domain_name;
                            $parameters[0]['user_id'] =   $this->request->data['user_id'];
                            $parameters[0]['domaintype'] = 'local';

                            $data['authentication'] = $auth;
                            $data['parameters'] = $parameters;

                            $datajson = json_encode($data);

                           $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL,"http://imaginet.rw/secureapi/thirdpartyapi/");
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $datajson);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            $response = curl_exec ($ch);
                            curl_close($ch);

                            $response = json_decode($response, true);


                            if($response[0]['status'] == 1) {
                                // If registration successfull change domain status to active

                                $orders= TableRegistry::get('order_details');
                                $order = $orders->get($orderDetailsEntity['id']);
                                $order->status = 'ACTIVE';

                                $orders->save($order);

                                $this->Flash->success($response[0]['message']);
                                $this->request->session()->write('domain_name', $domain_name);
                                $this->request->session()->write('price', $price);
                                //return $this->redirect(["action" => "registration"]);
                                $this->redirect(array("controller" => "Domains",
                                "action" => "view",
                                $orderDetailsEntity['id']
                                ));
                            }
                            else {

                                // delete records saved earlier
                                $orderdetails = $this->OrderDetails->get($orderDetailsEntity['id']);
                                $order = $this->Orders->get($orderEntity['id']);

                                if ($this->OrderDetails->delete($orderdetails)) {
                                    $this->Orders->delete($order);
                                    $this->NameServers->deleteAll(['NameServers.domain_name'=>$domain_name]);
                                }


                                $this->Flash->error($response[0]['message']);
                                $this->request->session()->write('domain_name', $domain_name);
                                $this->request->session()->write('price', $price);
                                return $this->redirect(["action" => "registration"]);
                            }
                        }

                   // }
                }

                }
                else {

                }


            $this->set('domain_name', $domain_name);
         }

    }

    public function handleDomainRenewal(){
        if($this->request->is('post')){
            $orderDetails_id = $this->request->getData('orderDetails');
            $order_id = $this->request->getData('order_id');
            $renewal_years = $this->request->getData('renewal_years');
            // Get domain user id
            $order = $this->Orders->find('all')
            ->Where(['id' => $order_id])->first();

            $user_id = $order->user_id;

            $data['requesttype'] = "renewal";

            $auth[0]['useremail'] = "ymugenga12@gmail.com";
            $auth[0]['password'] = "Clovene!312#";
            $auth[0]['timestamp'] = "20140812";

            $parameters[0]['order_id'] = $order_id;
            $parameters[0]['user_id'] = $user_id;
            $parameters[0]['renewal_years'] = $renewal_years;
            $parameters[0]['domaintype'] = 'local';

            $data['authentication'] = $auth;
            $data['parameters'] = $parameters;

            $datajson = json_encode($data);

            $ch = curl_init("http://localhost/secure_api/thirdpartyapi/");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER,array(
                "Content-Type: application/json",
                "accept-encoding: gzip, deflate",
                "accept: application/json",
                "host: exp.host"
            ));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $datajson);

            $response = curl_exec($ch);

            $response = json_decode($response, true);
            if($response[0]['status'] == "1") {
                // If registration successfull change domain status to active
                $orders= TableRegistry::get('order_details');
                $order = $orders->get($orderDetails_id);
                $order->status = 'ACTIVE';
                $order->expiration_date = $response[0]['expiration_date'];

                $orders->save($order);

                $this->Flash->success($response[0]['message']);
                $this->redirect(array("controller" => "Domains",
                "action" => "view",
                $orderDetails_id
                ));
            }
            else {
                $this->Flash->error($response[0]['message']);
                $this->redirect(array("controller" => "Domains",
                "action" => "view",
                $orderDetails_id
                ));
            }



        }
    }

    public function existingDomainSearch(){

        $filters = [];
        // Domain filter conditions

        // Check if they are filters are set
        if($this->request->is('post')){
            if($this->request->getData('clientname')){
                $filters['Users.first_name LIKE'] = '%'.$this->request->getData('clientname').'%';
            }
            if($this->request->getData('email')){
                $filters['Users.email LIKE'] = '%'.$this->request->getData('email').'%';
            }
            if($this->request->getData('organisation')){
                $filters['Address.company LIKE'] = '%'.$this->request->getData('organisation').'%';
            }

            $domain = $this->Address->find('all')->contain(['Users'])
            ->Where($filters);

            $this->set([
                'my_response' => $domain->toArray(),
                '_serialize' => 'my_response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }
    }

    public function handleGettingSingleAddress() {
        if($this->request->is('post')) {
            $addressid = $this->request->getData('addressid');

            $address = $this->Address->find('all')->contain(['Users'])
            ->Where(['Address.id' => $addressid])->first();
        }

        $this->set([
            'my_response' => $address->toArray(),
            '_serialize' => 'my_response',
        ]);
        $this->RequestHandler->renderAs($this, 'json');
    }


    /**
    * A method to update hosting plan before creating invoice
    * @param $domain_id
    * @return void
    **/
    function updateHosting() {
      if ($this->request->is('post')) {
          $orderid = $this->request->data["order_details_id"];
          $update = $this->OrderDetails->get($orderid);
          $update = $this->OrderDetails->patchEntity($update, $this->request->getData());

          if(!$this->OrderDetails->save($update)){
                $this->Flash->error("Sorry, an error occured while updating hosting package");
          }else{
                $this->Flash->success("Invoice info updated successfully");
          }
          $this->redirect(array("controller" => "Domains",
          "action" => "createinvoice",
          $orderid
          ));
      }
    }

     /**
    * A method to add or edit nameservers
    **/

    function addNameservers() {
        if ($this->request->is('post')) {

            $exist1 = $this->NameServers->find("all")->where(["id"=>$this->request->data["id_1"]])->count();
            if($exist1 > 0) {
                $nameserver1 = $this->NameServers->get($this->request->data["id_1"]);
                $nameserver1->server_name = $this->request->data["server_name_1"];
                $nameserver1->ip_address = $this->request->data["ip_address_1"];
                $nameserver1->domain_name = $this->request->data["domain_name"];

                if($this->NameServers->save($nameserver1)) {
                    $this->Flash->success("Nameserver 1 updated");
                }
                else{
                    $this->Flash->error("Failed updating nameserver 1");
                }
            }
            // Else create new one
            else{
                $nameserver1 = $this->NameServers->newEntity();
                $nameserver1->server_name = $this->request->data["server_name_1"];
                $nameserver1->ip_address = $this->request->data["ip_address_1"];
                $nameserver1->domain_name = $this->request->data["domain_name"];
                if($this->NameServers->save($nameserver1)) {
                    $this->Flash->success("Nameserver 1 Added");
                }
                else{
                    $this->Flash->error("Failed adding nameserver 1");
                }
            }

            $exist2 = $this->NameServers->find("all")->where(["id"=>$this->request->data["id_2"]])->count();
            if($exist2 > 0) {
                $nameserver2 = $this->NameServers->get($this->request->data["id_2"]);
                $nameserver2->server_name = $this->request->data["server_name_2"];
                $nameserver2->ip_address = $this->request->data["ip_address_2"];
                $nameserver2->domain_name = $this->request->data["domain_name"];

                if($this->NameServers->save($nameserver2)) {
                    $this->Flash->success("Nameserver 2 updated");
                }
                else{
                    $this->Flash->error("Failed updating nameserver 2");
                }
            }
            // Else create new one
            else{
                $nameserver2 = $this->NameServers->newEntity();
                $nameserver2->server_name = $this->request->data["server_name_1"];
                $nameserver2->ip_address = $this->request->data["ip_address_1"];
                $nameserver2->domain_name = $this->request->data["domain_name"];
                if($this->NameServers->save($nameserver2)) {
                    $this->Flash->success("Nameserver 2 Added");
                }else{
                    $this->Flash->error("Failed adding nameserver 2");
                }
            }
            $this->redirect(array("controller" => "Domains",
            "action" => "view",
            $this->request->data["domain_id"]
            ));

            $this->autoRender = false;
        }
    }

    public function pdfView($invoice_id, $fileNL){

        //Queries
        $invoice = TableRegistry::get('invoices');
        $invoice = $invoice->find()->where(['id'=> $invoice_id])->first();

        // get invoice info
        $domain = $this->OrderDetails->find('all')->contain(['Orders', 'Orders.Users'])
        ->Where(['OrderDetails.id'=>$invoice->order_details_id])->first();

        // Get billing address if there is an invoice
        $billingAddress = "";
        if (!empty($invoice)) {
            $billingAddress = $this->Address->find('all')
            ->where(['id'=>$invoice->address_id])->first();
        }
        // check if domain has a hosting package
        // If yes get package info
        if ($domain->package == "NULL") {
            $package = "";
        } else {
            $package = $this->Packages->find('all')
            ->where(['name'=>$domain->package_name])->first();
        }
         // Get address for invoice
         $invoiceAddressDetails = $this->ImaginetAddress->find('all')->toArray();
         $bankdetails = $this->Bankdetails->find('all')->toArray();

        // LOGIC Variables to go in the HTML
        $header_image = WWW_ROOT.'img/imaginet_logo.png';
        $stamp = WWW_ROOT.'img/imaginetstamp.png';
        $invoice_id = $invoice->id;
        $paid = ($invoice['paid']) ? 'Paid':'Not Paid';
        $domain_user_email = $domain->order->user->email;


        $invoice_created =  (!empty($invoice->created)) ? $invoice->created->format('M d Y') :'';
        $invoice_due = (!empty($invoice->due)) ? $invoice->due->format('M d Y') : '';

        $domainWithNoVatPrice = intval(($invoice->domain_price  * 18 / 118));
        $packageWithNovatPrice = 0;
        $packagePricing = 0;

        $amount = $invoice->domain_price - $domainWithNoVatPrice;

        $packageHTML = '';
        // If the domain has a hosting package associated
        if (!empty($package)) {
            $packagePricing = $invoice->hosting_price;
            $packageWithNovatPrice = intval($invoice->hosting_price * 18 / 118);

            $packageHTML .= '<tr>';
            $packageHTML .= '<td style="text-align:left">Hosting package: <strong>'.$package->name.'</strong></td>';
            $packageHTML .= '<td>';
            $packageHTML .= $invoice->hosting_price - $packageWithNovatPrice;
            $packageHTML .= '</td>';
            $packageHTML .= '</tr>';
        }

        $packageHTML .= '<tr>';
        $packageHTML .= '<td style="text-align:left">sub-total</td>';
        $packageHTML .= '<td>';
        $packageHTML .= ($invoice->domain_price - $domainWithNoVatPrice) + ($invoice->hosting_price- $packageWithNovatPrice );
        $packageHTML .= '</td></tr>';
        //Calculate VAT
        $vat = $domainWithNoVatPrice + $packageWithNovatPrice;
        //Calculate total
        $total = $invoice->domain_price + $invoice->hosting_price;

        // Payement details variables
        $bank_name = $bankdetails[0]->bank_name;
        $acc_number = $bankdetails[0]->acc_number;
        $swift_code = $bankdetails[0]->swift_code;
        $mobile_money = $bankdetails[0]->mobile_money;
        $contact = $bankdetails[0]->contact;

        // Company address variables
        $company_name = $invoiceAddressDetails[0]->company_name;
        $tin_no = $invoiceAddressDetails[0]->tin_no;
        $address = $invoiceAddressDetails[0]->address;
        $phone = $invoiceAddressDetails[0]->phone;
        $email = $invoiceAddressDetails[0]->email;


        // create new PDF document
        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        mb_internal_encoding('UTF-8');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Imaginet LTD');
        $pdf->SetTitle("Invoice");


        // Add a page
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->AddPage();

        // LOAD CSS
        $html = '';
        $html .= '<style>
        .popup {
  margin: 0px auto;
  /* padding: 20px; */
  background: #fff;
  border-radius: 5px;
  width: 60%;
  height: 630px;
  position: relative;
  transition: all 5s ease-in-out;
}

.invoice_view{
  margin-top:70px;
  width: 80%;
}

.popup h5, .invoice_view h5 {margin-top: 0;color: #333;font-family: Tahoma, Arial, sans-serif;text-align: center}
.popup .close {
  position: absolute;
  top: 5px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {color: #06D85F;}
.popup .content, .invoice_view .content {
  margin-left: 0%;
  max-height: 85%;
  overflow: auto;
  display: block;
  width: 100%;
  padding:0.3em 2em;
  margin-top: 10px;
  margin-bottom: 10px;
}
.popup .content .invoice-logo img{width:10%;background:black;margin-left: 47%;}
.popup .content .invoice-header{display: flex;}
.popup .content .invoice-header .company-info{width: 50%;justify-content: left; }
.popup .content .invoice-header .invoice-info {float:right;width: 50%}
.popup .content .invoice-header .company-info h5{font-size:0.8em;line-height: 1px;text-align: left; margin-top: 20px;margin-left:0;}
.popup .content .invoice-header .invoice-info h5{font-size:0.8em;line-height: 1px;margin-left: 40%;text-align: right;margin-top: 20px}

.popup .content .invoice-header .company-info p{font-size:0.6em;text-align: left;margin-left:0;line-height: 5px;}
.popup .content .invoice-header .invoice-info p{font-size:0.6em;line-height: 5px;margin-left: 40%;text-align: right;}


.popup .content .invoice-body, .popup .content .invoice-payment-details, .invoice-receiver-info{margin-top:50px;}
.popup .content .invoice-body .stripped-table tr th:first-child,.popup .content .invoice-body .stripped-table tr td:first-child
{text-align: left !important;
  }
.popup .content .invoice-body .stripped-table th,.popup .content .invoice-body .stripped-table td{text-align: right;}
.popup .content .invoice-body .stripped-table tr th{ background-color:rgb(76, 80, 85); color:#ffffff;}
.popup .content .invoice-payment-details h5, .popup .content .invoice-payment-details p, .popup .content .invoice-receiver-info p
{font-size:0.8em;line-height: 5px;}
.popup .content .invoice-receiver-info h5{font-size:0.8em;line-height: 1px;}
@media screen and (max-width: 700px){.box{width: 70%;}.popup{width: 70%;}}


        </style>';

        // HTML to be vonverted in PDF
        $html .= <<<EOD

<div class="invoice_view popup">
        <h2 style="text-align:center">Invoice</h2>
        <div class="content">
                <div class="invoice-logo">
                    <img src="$header_image" alt="$header_image" width="100" height="70" style=" border:1px solid red;">
                </div>
                <table border="0" style="padding-left: 10px; padding-bottom: 10px;">
                <tr style="font-size:0.7em;">
                <td style="">
                    <p> $company_name </p>
                    <p>TIN No: $tin_no</p>
                    <p> $address</p>
                    <!-- <h5>TELE 10 BLDG, GISHUSHU - REMERA</p> -->
                    <p>Phone $phone  $email </p>
                </td>
                <td style="text-align: right">
                    <p>INVOICE</p>
                    <p>Invoice date: $invoice_created </p>
                    <p>Invoice Due Date: $invoice_due</p>
                    <p>Invoice No: $invoice_id</p>

                </td>
                </tr>
                </table>
                <table border="0" style="padding-left: 10px; padding-bottom: 15px;">
                <tr style="font-size:0.8em;">
                    <td style="">
                    <p>Bill to:  $billingAddress->company </p>
                    <p>Attn: Registrant</p>
                    <p>Email: $domain_user_email  </p>
                    <p>$billingAddress->phone </p>
                </td>
                </tr>
                </table>

                <div class="invoice-body">
                    <table class="stripped-table" style="padding:3px;">
                            <tr>
                                <th style="text-align:left">Description</th>
                                <th>Amount(Rwf)</th>
                            </tr>


                            <tr>
                                <td style="text-align:left">Domain name:<strong> $domain->domain_name </strong></td>
                                <td>$amount</td>
                            </tr>
                            $packageHTML

                            <tr>
                                <td style="text-align:left">VAT ( 18% )</td>
                                <td> $vat </td>
                            </tr>
                            <tr>
                                <th style="text-align:left">Total</th>
                                <th>$total</th>
                            </tr>
                    </table>
                </div>
                <table border="0" style="padding-left: 10px; padding-bottom: 1px;">
                <tr style="font-size:0.7em;">
                <td style="">
                    <div class="invoice-payment-details">
                        <strong><h5>PAYMENT DETAILS</h5></strong>
                        <p> $bank_name </p>
                        <p>Account Number: $acc_number </p>
                        <p>Swift code: $swift_code </p>
                        <p>Mobile Money: $mobile_money </p>
                        <p>Contact: $contact </p>
                    </div>
                </td>
                <td style="text-align: left">
                     <img src="$stamp" alt="$stamp" width="150" height="150"/>
                </td>
                </tr>
                </table>
                <div class="invoice-payment-details">
                        <strong><h5>THANK YOU FOR YOUR BUSINESS</h5></strong>
                </div>
        </div>
</div>
EOD;

            // Print text using writeHTMLCell()
            $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            // ---------------------------------------------------------

            // Close and output PDF document

            $pdf->Output($fileNL,'F');

    }




}
