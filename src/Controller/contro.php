<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Utility\Security;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Log\Log;
use Cake\Utility\Xml;
use Cake\Http\Client;
use Cake\Http\Client\FormData;
use Cake\Mailer\Email;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Session;

//add files for mastercard payment api
require_once(ROOT . DS. 'vendor' . DS  . 'mastercard' . DS . 'PaymentCodesHelper.php');
require_once(ROOT . DS. 'vendor' . DS  . 'mastercard' . DS . 'VPCPaymentConnection.php');

class HomeController extends AppController 
{
    private $conn;
    private $transID;
    private $failedDomains = [];
    private $session;

	public function initialize()
    {
    	parent::initialize();
        $this->loadModel('Promo');
        $this->loadModel('Users');
        $this->loadModel('Address');
        $this->loadModel('AddressTypes');
        $this->loadModel('OrderDetails');
        $this->loadModel('Orders');
        $this->loadModel('NameServers');
        $this->loadModel('MscTransaction');
        $this->loadModel('TopLevelDomain');
        $this->loadModel('ImaginetNameServers');
        $this->loadModel('DomainYearDiscounts');
        $this->loadModel('Fine');
        $this->loadModel('Packages');
        $this->loadModel('MmTransaction');
        $this->loadModel('Countries');


        //mastercard initializations
        $this->conn = new \VPCPaymentConnection();
        $this->conn->setSecureSecret(Configure::read("MasterCard.SECRET"));
        $this->transID= Security::hash('IM' . Time::now(), 'sha1', true);

        $this->session = new Session();
        $this->session->start();

    }

	public function beforeFilter(\Cake\Event\Event $event)
    {
        $this->Auth->allow(['index', 'aboutus', 'domain', 'domainSearch', 'ajaxDomainSearch', 'addItemToCart', 'removeDomain', 'viewCart', 'updateYears', 'orderConfirmation', 'insertOldAddress', 'applyPromo', 'domainLookUp', 'transactionResponse', 'hosting', 'faq', 'support', 'sendMail', 'hostingPurchase','testemail']);       
    }

    public function isAuthorized($user)
    {

        return true;
    }



function testemail(){
    

 $encoding = "utf-8";

    // Preferences for Subject field
    $subject_preferences = array(
        "input-charset" => $encoding,
        "output-charset" => $encoding,
        "line-length" => 76,
        "line-break-chars" => "\r\n"
    );
   $from_name="Imaginet Ltd";
   $from_mail="info@imaginet.co.rw";   
   $mail_subject = 'TEST MESSAGE BY IMAGINET SERVER';   
   $mail_to      = 'guma250@gmail.com';
   
   $mail_message = "Hello this is a text message hope it has reahed well in your Inbox\nThank you very much. Brian";
// Mail header
    $header = "Content-type: text/html; charset=".$encoding." \r\n";
    $header .= "From: ".$from_name." <".$from_mail."> \r\n";
    $header .= "MIME-Version: 1.0 \r\n";
    $header .= "Content-Transfer-Encoding: 8bit \r\n";
    $header .= "Date: ".date("r (T)")." \r\n";
    $header .= iconv_mime_encode("Subject", $mail_subject, $subject_preferences);



 mail($mail_to, $mail_subject, $mail_message, $header);

/*$headers = 'From: info@imaginet.rw' . "\r\n" .
    'Reply-To: support@imaginet.rw' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$msg = wordwrap($message,70);
mail($to, $subject, $msg, $headers);

*/
die("done");
 
    
}
    public function hosting($ordering = null, $domain_name=null){
        $packages = $this->Packages->find("all")->where(['status'=>'1']);

        if($this->request->is("post")){
           
            if(!empty($this->request->data["order"]) && $this->request->data["domain"] !== 'upgrade'){
             
                $this->addHostingPackage($this->request->data["package"], $this->request->data["domain"]);
                return $this->redirect(['action'=>'order']);
            }else if($this->request->data["domain"] == 'upgrade'){
                //return back to users portal if this is an upgrade request
                 $this->request->session()->write("hosting_data", $this->request->data);
                return $this->redirect(['controller'=>'users', 'action'=>'hosting-payment']);
            }else{
                $this->request->session()->write("hosting_data", $this->request->data);
                return $this->redirect(['action'=>'hosting-purchase']);
            }
        }

        $this->set(compact('packages', 'ordering', 'domain_name'));
    }

    public function hostingPurchase(){
        $data = $this->request->session()->read("hosting_data");
        
        $package = $this->Packages->find("all")->where(["name"=>$data["package"]])->first();
        

        if($this->request->is("post")){
            //do a lockup for the domain name here
            $domain = $this->request->data["domain_name"];

            $data["domain"] = $domain;
            $this->request->session()->write("hosting_data", $data);

            $lookup = false;
            if($this->isDomainLocal($domain)){
                $lookup = $this->localDomainLookUp($domain);
            }else{
               $lookup = $this->domainLookUp($domain);  
            }

            if(!$lookup){
                //ask user to log in and the redirect user to payment     
                if($this->Auth->user('first_name') === null){
                    $this->redirect(['action'=>'order-confirmation', 2]);
                }else{
                    return $this->redirect(["action"=>'package-payment']);
                }
            }else{
                $this->Flash->error("The domain name you enter does not exist");
            }    
        }

        $this->set(compact('package'));
    }

    public function packagePayment(){
        $data = $this->request->session()->read("hosting_data");
        $package = $this->Packages->find("all")->where(["name"=>$data["package"]])->first();
      
        $this->set(compact('package'));
    }

    public function packagePurchaseFeedback($code){
        $data = $this->request->session()->read("hosting_data");
        $package = $this->Packages->find("all")->where(["name"=>$data["package"]])->first();
        $this->persistHostingPackageOrder($code);

        //send email to customer
        $fullname = $this->Auth->user('first_name') . ' ' . $this->Auth->user('last_name');
        $this->sendHostingPurchaseEmail($fullname, $this->Auth->user('email'), $data['domain'], $package);

        $this->set(compact('code', 'package'));
    }
   
   
    public function beforeRender(Event $event)
    {
        //display items in shopping cart
        $cart = $this->readShoppingCart();
        $total = $this->calculateDomainTotal();
        $cartCount = count($this->readShoppingCart());
        $promoDiscount = $this->getPromoCodeDiscount();
         $countries = $this->Countries->find("list", ["keyField"=>"code", "valueField"=>"country_name"])->order(['country_name'=>'ASC']);
        $this->set(compact('cart', 'total', 'cartCount', 'promoDiscount', 'countries'));
    }

    public function index()
    {
        $tlds = $this->TopLevelDomain->find("all");
        $this->set(compact('tlds')); 
    }

    public function aboutus(){

    }

    public function domain($type)
    {
    	$this->set(compact('type'));
    }
    

    public function domainSearch(){
    	
    	$type = "foreign";
    	$isDomainAvailable = false;
        $searchResults = [];
    	
    	if($this->request->is('post') || $this->request->is('ajax')){

            if($this->request->is('ajax')){
                $this->autoRender = false;

                $results = [];
           
            }

    		$searchTerm = $this->request->data['search_term'];

            $type = $this->request->data["type"];

            $price = 0;

            //check if tlds extension is added to search term
            if($type == "foreign"){
                if(strpos($searchTerm, ".") === false){
                    $searchTerm .= ".com";
                }
            }else{
                if(strpos($searchTerm, ".") === false){
                    $searchTerm .= ".rw";
                }
            }
            
           

            if(!$this->isDomainLocal($searchTerm)){
                if($this->domainLookUp($searchTerm)){
                    $isDomainAvailable = true;
                }
            }else{

                if($this->localDomainLookUp($searchTerm))
                     $isDomainAvailable = true;
             }


           
            //make suggestions for user
            $tlds = $this->TopLevelDomain->find('all')->where(["type"=>$type]);

            $searchText = str_replace(strrchr($searchTerm, "."), "", $searchTerm);

            
            foreach($tlds as $tld){
                if(strpos($this->request->data["search_term"], $tld->tld) !== false){
                   
                    $searchText = substr($this->request->data["search_term"], strpos($this->request->data["search_term"], $tld->tld));
                    $searchText = str_replace($searchText, "", $this->request->data["search_term"]);

                }
            }

          

            //loop through all the tlds and make a lookup
            foreach($tlds as $domain){
                if(strpos($searchText, $domain->tld))
                    str_replace($domain->tld, "", $searchText);

                if(!$this->isDomainLocal($searchText.$domain->tld)){
                    $isAvailable = $this->domainLookUp($searchText.$domain->tld);

                }else{
                    $isAvailable = $this->localDomainLookUp($searchText.$domain->tld);
                     
                }

                if($searchTerm == $searchText.$domain->tld){
                    $price = $domain->price;
                }

                array_push($searchResults, ['domain_name'=>$searchText.$domain->tld, 'is_available'=>$isAvailable, 'price'=>$domain->price]); 
            }


            if($price == 0){
                $top_domains = $this->TopLevelDomain->find("all");
                foreach($top_domains as $domain){
                    if($searchTerm == $searchText.$domain->tld){
                        $price = $domain->price;
                    }
                }

            }
            
            /*
            if($this->request->is('ajax')){
                $results["searchResults"] = $searchResults;
                $results["available"] = $isDomainAvailable;
                $results["domainToPurchase"] = $searchTerm;
                $results["price"] = $price;
                return $this->response->body(json_encode($results));
            }*/

    		
    	}else{
            return $this->redirect(['action'=>'domain', "foreign"]);
        }

    	$this->set(compact('searchResults', 'searchTerm', 'isDomainAvailable', 'type', 'price'));

    }


    /**
    * This method calls another method from the Appcontroller
    * to add domain items to the users session
    */
    public function addItemToCart(){

    	if($this->request->is('ajax')){
    		$this->autoRender = false;

    		//assume failure
    		$status["status"] = "fail";
    		//method called from the AppController
    		$this->addToCart($this->request->getData());

    		$status["status"] = "success";
    		$status["total"] = $this->calculateDomainTotal();
    		$this->response->body(json_encode($status));

       		return $this->response;
    	}

    }

    public function viewCart($years = null){
    	
    	$this->set(compact('years'));
    }


    public function removeDomain($domain_name){
    	//remove domain from cart
    	$this->removeCartItem($domain_name);

    	$this->redirect(['action'=>'view-cart']);

    }

    public function updateYears(){
    	$this->autoRender = false;
    	$status["status"] = "fail";
    	$this->updateDomainPrice($this->request->getData());
    	$status["status"] = "success";
    	$this->response->body(json_encode($status));
    
    	return $this->response;
    }

    public function applyPromo(){
    	if($this->request->is("post")){
    		$promoCode = $this->request->data["promo_code"];
    		$promo = $this->Promo->find("all")->where(["promo_code"=>$promoCode])->toArray();

    		if(count($promo) > 0){
    			$this->addPromo($promo[0]["amount"]);
    		}else{
    			
    			$this->Flash->error(__('Invalid Promo Code'));
    		}
    	}

    	$this->redirect(["action"=>"view-cart"]);
    }

    public function orderConfirmation($is_order=1){
    	 $user = $this->Users->newEntity();


    	 //No need to login or create account if user has already logged in
    	if($this->Auth->user('first_name') !== null){
    		$this->redirect(['action'=>'checkout-address']);
    	}

    	$this->set(compact('user', 'is_order'));
    }
    private function getAddress($type){

    	$userAddress = $this->AddressTypes->find("all")->contain(["Address"])->where(['Address.user_id'=>$this->Auth->user('id'), ["AddressTypes.address_type"=>$type]])->first();
    	
    	return !is_null($userAddress) ? $userAddress->addres : null;
    }

    public function checkoutAddress(){
    	$address = $this->Address->newEntity();
    	//assume user's address is not saved
    	$isAddressSaved = false;
    	//check if user's address is already in the system
    	$userAddress = $this->AddressTypes->find("all")->contain(["Address"])->where(['Address.user_id'=>$this->Auth->user('id'), ["AddressTypes.address_type"=>'default']])->count();


    	if($userAddress > 0){
    		$isAddressSaved = true;
    		
    		$address = $this->getAddress("default");
    		$registrant = $this->getAddress("registrant");
    		$admin = $this->getAddress("admin");
    		
    		$tech = $this->getAddress("tech");
    		$billing = $this->getAddress("billing");

    		//write the addresses to session
    		$this->request->session()->write('address', $address);
            $this->request->session()->write('address_id', $address->id);
    		$this->request->session()->write('registrant', $registrant);
    		$this->request->session()->write('admin', $admin);
    		$this->request->session()->write('tech', $tech);
    		$this->request->session()->write('billing', $billing);

			$this->redirect(['action'=>'billing']);
    	}

    	if($this->request->is('post')){

    		$this->request->data["phone"] = $this->request->data["country_code"] . $this->request->data["phone"];
    		
    		//save address if it's for an account creation
    		if($this->request->data["address_type"] == "default"){
    			$address = $this->Address->patchEntity($address, $this->request->data);
    			$address["user_id"] = $this->Auth->user('id');
    			$this->request->session()->write('address', $address);
    			if($this->Address->save($address)){
                    $this->saveAddressType($address->id, "default");
    				$this->request->session()->write('address', $address);
                    $this->request->session()->write('address_id', $address->id);
					$this->redirect(['action'=>'billing']);
    			}
    		}

    		$address = $this->Address->newEntity($this->request->getData());
    		
    		if(count($address->errors()) == 0){
    			$this->request->session()->write('address', $address);
				$this->redirect(['action'=>'billing']);
    		}else{
    			$this->Flash->error(__('An error occured while saving your contact information'));
    		}
    	}

    	$this->set(compact('isAddressSaved', 'address'));

    }

    public function billing($back = null){

    	//make sure there are items in cart
    	if(count($this->readShoppingCart()) == 0)
    		$this->redirect(['action'=>'domain', 'foreign']);

    	//decide whether to show errorMessages or not
    	$showAdminForm = false;
    	$showRegForm = false;
    	$showTechForm = false;
    	$showBillForm = false;

    	//get data to be set in form if back button on payment page has been clicked
    	$billingInfo = !is_null($back) ? $this->request->session()->read("allAddress") : [];

    	//set the value for any form that needs to be displayed as a result of clicking the 
    	//back button from payment page
    	$showAdminForm = !empty($billingInfo) && array_key_exists("admin", $billingInfo) ? true : false;
    	$showTechForm = !empty($billingInfo) && array_key_exists("tech", $billingInfo) ? true : false;
    	$showRegForm = !empty($billingInfo) && array_key_exists("registrant", $billingInfo) ? true : false;
    	$showBillForm = !empty($billingInfo) && array_key_exists("billing", $billingInfo) ? true : false;

    	//redirect
    	if(!array_key_exists("address", $this->request->session()->read())){
    		return $this->redirect(['action'=>'checkout-address']);
    	}
      
    	$address = $this->request->session()->read("address");

    	//variable to hold new address
    	$newArray = [];
    	if($this->request->is('post')){
    		//variable to hold data of individual address of the four types
    		$addressArray = [];
    		
    		$isRegistrantValid = true;
    		$isBillingValid = true;
    		$isAdminValid = true;
    		$isTechnicalValid = true;



    		//check if a new registrant address has been provided
    		if(!empty($this->request->data["r_company"]) || !empty($this->request->data["r_address_line_1"])){
    			$addressArray = $this->generateAddressArray("r_", $this->request->data);
    			//add address to array
    			$newArray["registrant"] = $this->Address->newEntity($addressArray);

    			$isRegistrantValid = $this->validateAddress($addressArray);
    			if(!$isRegistrantValid)
    				$showRegForm = true;
    		}

    		//check if a new admin address has been provided
    		if(!empty($this->request->data["a_company"]) || !empty($this->request->data["a_address_line_1"])){
    			$addressArray = $this->generateAddressArray("a_", $this->request->data);
    			$newArray["admin"] = $this->Address->newEntity($addressArray);
    			$isAdminValid = $this->validateAddress($addressArray);
    			if(!$isAdminValid)
    				$showAdminForm = true;
    		}

    		//check if a new technical address has been provided
    		if(!empty($this->request->data["t_company"]) || !empty($this->request->data["t_address_line_1"])){
    			$addressArray = $this->generateAddressArray("t_", $this->request->data);
    			$newArray["tech"] = $this->Address->newEntity($addressArray);
    			$isTechnicalValid = $this->validateAddress($addressArray);
    			if(!$isTechnicalValid)
    				$showTechForm = true;
    		}

    		//check if a new billing address has been provided
    		if(!empty($this->request->data["b_company"]) || !empty($this->request->data["b_address_line_1"])){
    			$addressArray = $this->generateAddressArray("b_", $this->request->data);

    			$newArray["billing"] = $this->Address->newEntity($addressArray);

    			$isBillingValid = $this->validateAddress($addressArray);
    			if(!$isBillingValid)
    				$showBillForm = true;
    		}
    		
    		//write address to session if no validation rule is violated
    		if($isAdminValid && $isTechnicalValid && $isRegistrantValid && $isBillingValid){
    			
    			$this->request->session()->write('allAddress', $newArray);

    			return $this->redirect(['action'=>'order']);
    		}
    		  
			$this->Flash->error(__('Please make sure to fill all the fields correctly. All fields are required'));
           
    	}

    	$this->set(compact('address', 'showAdminForm','showBillForm','showTechForm','showRegForm', 'billingInfo', 'newArray'));
    }

    private function validateAddress($address){

    	$address = $this->Address->newEntity($address);

    	if(count($address->errors()) > 0)
    		return false;

    	return true;
    }

    private function generateAddressArray($field_initial, $data){
    	$addressArray = [];
    	$addressArray["company"] = $data[$field_initial."company"];
    	$addressArray["phone"] = $data[$field_initial."country_code"]. $data[$field_initial."phone"];
        $addressArray["user_id"] = $this->Auth->user('id');
    	$addressArray["address_line_1"] = $data[$field_initial."address_line_1"];
    	$addressArray["address_line_2"] = $data[$field_initial."address_line_2"];
    	$addressArray["address_type"] = $data[$field_initial."address_type"];
    	$addressArray["city"] = $data[$field_initial."city"];
    	$addressArray["state"] = $data[$field_initial."state"];
    	$addressArray["zip_code"] = $data[$field_initial."zip_code"];
    	$addressArray["country"] = $data[$field_initial."country"];

    	return $addressArray;
    }


    public function payment(){
    	//make sure there are items in cart
    	if(count($this->readShoppingCart()) == 0)
    		$this->redirect(['action'=>'domain', 'foreign']);



        //all local domains should have two nameservers
        foreach($this->readShoppingCart() as $cart){
            //check if domain is local
            if($this->isDomainLocal($cart["name"])){
                if(count($this->readDomainNameServers($cart["name"])) < 2 && count($cart["pack"]) == 0){
                    $this->Flash->error("Local Domains should have a minimum of two name servers");
                    return $this->redirect(["action"=>"order"]);
                }
            }else if(count($cart["pack"]) == 0 && count($this->readDomainNameServers($cart["name"])) < 2){
                $this->Flash->error("Name servers when added for international domains should be at least two.");
                 return $this->redirect(["action"=>"order"]);
            }
        }
    	
    	if($this->request->is('post')){
    		//make sure payment details are not empty
    		
    		if(!empty($this->request->data["phone_number"])){
    			//do necessary processing here and write payment details to session
    			$payment = $this->request->data;
    			$this->request->session()->write("payment", $payment);
    			$this->redirect(['action'=>'order']);
    		}

    		$this->Flash->error("Please make sure to provide the appropriate payment information");
    	}
    }

    public function order(){
    	
    	//make sure there are items in cart
    	if(count($this->readShoppingCart()) == 0)
    		$this->redirect(['action'=>'domain', 'foreign']);
    	
    	//retrieve name servers for domain in this order
    	$nameServers = $this->getNameServers();
      
    	if(($this->Auth->user('first_name') == null) || count($this->readShoppingCart()) == 0){
    		return $this->redirect(['domain', 'foreign']);
    	}

    	$this->set(compact('nameServers'));
    }

    /**
	* Uses ajax to adds a name server to session for a particular domain
	* being purchased
	*@return true if successful and false otherwise
	*/
    public function addNameServer(){
    	if($this->request->is('ajax')){

    		$this->autoRender = false;
    		$status["status"] = "fail";
    		
    		$domain_name = $this->request->data["domain_name"];
    		//create index for name server by concatenating domain name, name server and ip
    		$server = !empty($this->request->data["name_server"]) ? $this->request->data["name_server"] : '';

            
            if(!$this->verifyLocalNameServer($server)){
                //set status to 0 to depict that name server does not exist
                $status["status"] = '0';
                $this->response->body(json_encode($status));
                return $this->response;
            }
            
           
    		$ip = !empty($this->request->data["ip_address"]) ? $this->request->data["ip_address"] : '';
    		$index = $domain_name.'_'.$server.'_'.$ip;
    		$index = $this->formatIndex(".", "_", $index);
    	
    		$status["index"] = $index;
    		$name_server["name_server"] = $server;
    		$name_server["ip_address"] = $ip;
    		$name_server["domain_name"] = $domain_name;

    		$status["existing"] = is_null($this->request->session()->read($index)) ? false : true;
    		$this->request->session()->write($index.'_nm', $name_server);
    		$status["status"] = true;
    		$this->response->body(json_encode($status));
    		return $this->response;
    	}
    }


    /**
    * Uses ajax to adds the default name server to session for a particular domain
    * being purchased it the user opts for
    *@return true if successful and false otherwise
    */
    public function addHosting($package, $domain, $remove = null, $action="order"){

        $this->addHostingPackage($package, $domain, $remove);
        return $this->redirect(['action'=>$action]);  
    }


    /**
    * Deletes a name server from session
    * @return true if successfull and false otherwise
    **/
     public function deleteNameServer(){
    	if($this->request->is('ajax')){

    		$this->autoRender = false;
    		$status["status"] = "fail";
    		
    		$domain_name = $this->request->data["domain_name"];
    		//create index for name server by concatenating domain name, name server and ip
    		$server = !empty($this->request->data["name_server"]) ? $this->request->data["name_server"] : '';
    		$ip = !empty($this->request->data["ip_address"]) ? $this->request->data["ip_address"] : '';
    		$index = $domain_name.'_'.$server.'_'.$ip;
    		$index = $this->formatIndex(".", "_", $index);

    		$this->request->session()->write($index.'_nm', null);
    		$status["status"] = true;
    		$this->response->body(json_encode($status));
    		return $this->response;
    	}
    }

    //Read domain items from session 
    protected function readDomainNameServers($domain_name){
        $nameServers = [];
        $index = $this->formatIndex(".", "_", $domain_name);
        if(!is_null($this->request->session()->read())){
           
            foreach($this->request->session()->read() as $key => $value){
            //check if the key of the session contains domain

                if (strpos($key, $index) !== false && strpos($key, '_nm') && $value !== null) {
                   
                    array_push($nameServers, ["name" => $value["name_server"], "ip"=>$value["ip_address"]]);
                }
            }
        }
     
        return $nameServers;
    }

    /**
    * Retrieves the available session name servers from session
    * @return an array containing the available name servers if any
    **/
    private function getNameServers(){
    	$nameServers = [];
    	if(!is_null($this->request->session()->read())){
           
            foreach($this->request->session()->read() as $key => $value){
            //check if the key of the session contains domain

                if (strpos($key, '_nm') !== false && $value !== null) {
                    array_push($nameServers, ["name" => $value["name_server"], "ip"=>$value["ip_address"], "domain" => $value["domain_name"]]);

                }
            }
        }

        return $nameServers;
    }


    /**
    * Processes Mastercard Payment
    * @param $action, the method to return to aftr processing the payment
    * if no action is passed, then it is an order being made for a domain
    * purchase and therefore the user is directed to the receipt page after
    * the payment with the appropriate message
    */
    public function processPayment($action=null){

        //set mastercard API URL
        $masterCardAPIUrl='https://migs.mastercard.com.au/vpcpay';
        
        //read data to be sent to mastercard from configuration file
        $paymentData = [];

        $returnUrl = "http://portal.imaginet.rw". Router::url(["controller" => "Home","action" => "processReceipt"]);

        $amount = $this->calculateDomainTotal();

        //check session to read amount to be paid if available, if not we
        //use the default one which is calculating the amount from the shopping cart
        if(!is_null($this->request->session()->read("paymentAmount"))){
            $amount = $this->request->session()->read("paymentAmount");
        }

        //set a parameter representing the action to redirect to after payment is made
        if(!is_null($action)){
            $returnUrl = "http://portal.imaginet.rw". Router::url(["controller" => "Home","action" => "processReceipt", $action]);
        }else if(isset($this->request->data['amount'])){
            //we checking to see if this payment is for a hosting package purchase
            $amount = $this->request->data['amount'];
            $returnUrl = "http://portal.imaginet.rw". Router::url(["controller" => "Home","action" => "processReceipt", 'package-purchase-feedback']);
        }

        $paymentData["vpc_Version"] = Configure::read('MasterCard.VERSION');
        $paymentData["vpc_ReturnURL"] = $returnUrl;
        $paymentData["vpc_Command"] = Configure::read('MasterCard.COMMAND');
        $paymentData["vpc_Merchant"]= Configure::read('MasterCard.MERCHANT');
        $paymentData["vpc_AccessCode"] = Configure::read('MasterCard.ACCESSCODE');
        $paymentData["vpc_Gateway"]='ssl';
        $paymentData["vpc_OrderInfo"]=date("Ymdhis");
        $paymentData["vpc_MerchTxnRef"]=$this->transID;
        $paymentData["vpc_Amount"] = $amount;
        ksort($paymentData);
        
        $this->set(compact('masterCardAPIUrl', 'paymentData'));
    }



    /**
    * Get response data from MasterCard API
    * Renders page based on the returned data
    * @param $action, the method to be redirected to after processing
    * payment details. If null then we just render the processReceipt view
    */
    public function processReceipt($action = null){
        $errorsExist = false;
        // Add VPC post data to the Digital Order
      
        foreach($this->request->query as $key => $value) {
            if (($key!="vpc_SecureHash") && ($key != "vpc_SecureHashType") && 
                ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {

                $this->conn->addDigitalOrderField($key, $value);
            }
        }

        // Obtain a one-way hash of the Digital Order data and
        // check this against what was received.
        $secureHash = $this->conn->hashAllFields();

        if(array_key_exists("vpc_SecureHash", $this->request->query)) 
        {
            if ($secureHash==$_GET["vpc_SecureHash"]) {
                $hashValidated = "CORRECT";
            } else {
                $hashValidated = "INVALID HASH";
                $errorsExist = true;
            }
        } else {
            $hashValidated = "NO HASH RETURNED";
        }

        //get data to be sent to receipt page
        $receiptData = $this->request->query;
        $codesHelper = new \PaymentCodesHelper();


        if($receiptData["vpc_TxnResponseCode"] != "No Value Returned"){
           
            $receiptData["txnResponseCodeDesc"] = $codesHelper->getResultDescription($receiptData["vpc_TxnResponseCode"]);
        }

        if(array_key_exists("vpc_CSCResultCode", $receiptData)){
             if($receiptData["vpc_CSCResultCode"] != "No Value Returned"){
           
                $receiptData["cscResultCodeDesc"] = $codesHelper->getResultDescription($receiptData["vpc_CSCResultCode"]);
            }
        }else{
           $receiptData["cscResultCodeDesc"] = ''; 
        }
    
        if(array_key_exists("vpc_AVSResultCode", $receiptData)){
             if($receiptData["vpc_AVSResultCode"] != "No Value Returned"){
           
                $receiptData["avsResultCodeDesc"] = $codesHelper->getResultDescription($receiptData["vpc_AVSResultCode"]);
            }
        }else{
            $receiptData["avsResultCodeDesc"] = '';
        }
       

        $error='Error';
        
        //commit transaction into Database
        $transaction["amount"] = $receiptData["vpc_Amount"];
        $transaction["trans_no"] = $receiptData["vpc_TransactionNo"];
        $transaction["message"] = $receiptData["vpc_Message"];
        $transaction["user_id"] = $this->Auth->user('id');
        $transaction["trans_code_desc"] = $receiptData["txnResponseCodeDesc"];
        $transaction["card_type"] = $receiptData["vpc_Card"];
        $transaction["trans_token"] = $receiptData["vpc_VerToken"];
        $transaction["receipt_no"] = $receiptData["vpc_ReceiptNo"];
        $transaction["trans_ref"] = $receiptData["vpc_MerchTxnRef"];

        //payment type for hosting upgrade and renewal is written to session
        //A value of null will indicate that this payment is not for hosting renewal or upgrade
        //and therefore we set the default value of RENEWAL or PURCHASE based on whether
        //an action was supplied to this method or not
        $transaction["payment_type"] = $this->request->session()->read('payment_type');

        if(is_null($transaction["payment_type"])){
            $transaction["payment_type"] = !is_null($action) ? 'RENEWAL' : 'PURCHASE';
        }
        
        $transEntity = $this->MscTransaction->newEntity();
        $transEntity = $this->MscTransaction->patchEntity($transEntity, $transaction);
        if(!$this->MscTransaction->save($transEntity)){
             Log::debug('An error occured when saving MasterCard transaction => '. $transEntity);
        }

       
        //return to the necessary action if one is provided
        if(!is_null($action)){
            //write the id of this transaction to session
            //it will be used to update the order id of this transaction for hosting upgrade
            $this->request->session()->write("msc_trans_id", $transEntity->id);
            return $this->redirect(['action'=>$action, $receiptData["vpc_TxnResponseCode"]]);
        }

        //commit order details if payment is successfull
        if($receiptData["vpc_TxnResponseCode"] == '0'){
            $order_id = $this->persistOrderDetails(false);
            $this->MscTransaction->updateAll(['order_id'=>$order_id],['id'=>$transEntity->id]);
            $error="";
            //Email Customer
            
            //send email to customer
            $email = new Email('default');
            $email->template('order_mail','default')
                    ->viewVars([
                            'cart' => $this->readShoppingCart(), 
                            'fullname' => $this->Auth->user('first_name') . ' ' . $this->Auth->user('last_name'),
                            'order_date'=> Time::now(),
                            'total'=>  $this->calculateDomainTotal(),
                            'discount' => $this->getPromoCodeDiscount(),
                            'failed' => false
                    ])
                    ->emailFormat('html')
                    ->from(['info@imaginet.rw' => 'Imaginet Limited'])
                    ->to($this->Auth->user('email'))
                    ->addTo("info@imaginet.rw")
                    ->subject('Imaginet Ltd: Your Purchased Domain')
                    ->send(); 

            $s_cart = $this->readShoppingCart();
            $discount = $this->getPromoCodeDiscount();
            $subtotal = $this->calculateDomainTotal();
            $failed = $this->failedDomains;
            $this->clearShoppingCart();
            $this->failedDomains = [];
        }

        $this->set(compact('receiptData', 'error', 's_cart', 'subtotal', 'discount', 'failed'));      
    }


    /**
    * Processes mobile money payment whenever
    * a user choses mobile money as an option for paying for 
    * their purchased domain
    */
    public function processMobileMoneyPayment($code = null){
        if($this->request->is('post')){

            $phoneNumber = $this->request->data["phone_number"];
            //check if mm payment is for domain purchase or hosting purchase
            //The amount request data depicts that this payment is from the
            //purchase of a hosting package
            if(isset($this->request->data['amount'])){
               $subtotal = $this->request->data['amount']; 
            }else{
               $subtotal = $this->calculateDomainTotal(); 
            }
            
            $discount = $this->getPromoCodeDiscount();
            $paymentXml = $this->mobileMoneyRequestXml($phoneNumber, $subtotal);
            $paymentUrl = Configure::read("MobileMoney.PAYMENT_URL");

            $response = $this->executeMMPayment($paymentUrl, $paymentXml);

            if($response == false){
                return $this->redirect(['action'=>'payment-error']);
            }

            try{
                $response = Xml::toArray(Xml::build($response));
            } catch (\Cake\Utility\Exception\XmlException $e) {
                 return $this->redirect(['action'=>'payment-error']);
            }

            $code = $response['operationResult']['response']['responsecode'];
             
            //array to hold the various status codes when processing an MM payment
            $errorCodes = ['102', '103', '104', '105', '106', '107', '108', '109', '190', '99', '999'];

            if(isset($this->request->data['amount'])){
                $hosting['amount'] = $this->request->data['amount'];
                $hosting['payment_data'] = $response['operationResult']['response'];
                $hosting['phone'] = $phoneNumber;
                $this->request->session()->write('hosting_payment', $hosting);
                return $this->redirect(['action'=>'mm-payment']);
            }

            $mm_amount = $this->calculateDomainTotal();

            if($code == '1000'){
                $order_id = $this->persistOrderDetails(true);
                $this->request->session()->write('order_id', $order_id);
                //$this->clearShoppingCart();
              
            }else if(in_array($code, $errorCodes)){
                //save payment details with their error status and report to 
                //user the status of the payment
                $order_id = "";  
            }

            $this->persistMobileTransaction($response['operationResult']['response'], $mm_amount, $phoneNumber, $order_id,"PURCHASE");
                 
        }

        $this->set(compact('code'));
    }


    public function transactionResponse(){

        $this->autoRender = false;

        if($this->request->is('post')){

            $postData = trim(file_get_contents('php://input'));
             //send feedback
            $feedXml = "<operationResult type=\"paymentcompletedresponse\">";
            $feedXml .= "<response><responsestatus>RECEIVED</responsestatus>";
            $feedXml .= "</response></operationResult>";
            
            
            ob_start();
            echo $feedXml;
            header('Connection: close');
            header('Content-Length: '.ob_get_length());
            ob_end_flush();
            ob_flush();
            flush();
          
            try{
                $response = Xml::toArray(Xml::build($postData));
               
            } catch (\Cake\Utility\Exception\XmlException $e) {
                Log::debug("MM Payment System failure ".$postData);
                return;
            }
           
            $response = $response["operationResult"]["request"];
            $trans = $this->MmTransaction->find("all")->where(["transaction_id"=>$response["transactionid"]], ["response_code"=>'1000'])->first();

            
          
            if($response["responsecode"] == '100'){
                //payment has been successfull
                $this->MmTransaction->updateAll(['response_code'=>'100', 'response_msg'=>'COMPLETE'],['transaction_id'=>$response["transactionid"]]);

                //check if payment response is for a hosting package service
                $hosting = $this->Orders->get($trans->order_id)->status;

                if($hosting == 'HOSTING'){
                    //send hosting purcase confirmation email
                    $order = $this->OrderDetails->find("all")->where(["order_id"=>$trans->order_id])->first();
                    $user = $this->getDomainOwner($order->domain_name);
                    $fname = $user[0]["first_name"];
                    $lname = $user[0]['last_name'];
                    $email = $user[0]['email'];

                    $package = $this->Packages->find("all")->where(["name"=>$order->package_name])->first();

                    $this->sendHostingPurchaseEmail($fname.' '.$lname, $email, $order->domain_name, $package);
                    return;
                }

                //if this domain is already registered then it means it is a renewal transaction
                $renew_order = $this->OrderDetails->find("all")->where(["order_id"=>$trans->order_id])->first();

                if($renew_order->is_registered){

                    $user = $this->getDomainOwner($renew_order->domain_name);
                    $fname = $user[0]["first_name"];
                    $lname = $user[0]['last_name'];
                    $emailAddr = $user[0]['email'];

                    //send renewal email and exit
                    $email = new Email('default');
                    $email->template('renewal_mail','default')
                            ->viewVars([
                                'domain' => $renew_order->domain_name,
                                'fullname' => $fname . ' ' . $lname,
                                'exp_date'=>  $renew_order->expiration_date
                               
                            ])
                            ->emailFormat('html')
                            ->from(['info@imaginet.rw' => 'Imaginet Limited'])
                            ->to($emailAddr)
                            ->addTo("info@imaginet.rw")
                            ->subject('Imaginet Limited: Domain Renewal')
                            ->send(); 
                  
                    return;
                }

                //register domain and send an email of the registration to the user
                $domainDetails = $this->OrderDetails->find('all')->where(['order_id'=>$trans->order_id]);

             
                $registrationAddress = [];

                //get billing address
                $registrationAddress["billing"] = $this->getAddressType("billing", $trans->user_id);
                //get admin address
                $registrationAddress["admin"] = $this->getAddressType("admin", $trans->user_id);

                //get tech address
                $registrationAddress["tech"] = $this->getAddressType("tech", $trans->user_id);

                //get tech address
                $registrationAddress["registrant"] = $this->getAddressType("registrant", $trans->user_id);

                $cart = [];

                //get user
                $user = $this->Users->get($trans->user_id);
                $order = $this->Orders->get($trans->order_id);
               
                foreach($domainDetails as $domain){
                    $pack = [];

                    if(!is_null($domain->package_name)){
                        $packagePrice = $this->Packages->find("all")->where(["name"=>$domain->package_name])->first()->pricing;

                        $pack = ["name"=>$domain->package_name, "price"=>$packagePrice];
                    }
                    
                    //create cart for sending customer email
                    array_push($cart, ["name" => $domain->domain_name, "price"=>$domain->price, "year" => $domain->quantity, "pack"=>$pack]);
                   
                    
                    $domainNameServers = $this->getDomainNameServers($domain->domain_name);
  

                    //register this domain and update domain status to registered
                    $status = 0;
                    if($this->isDomainLocal($domain->domain_name)){
                       $status = $this->registerLocalDomain($domain->domain_name, $registrationAddress, $domainNameServers);
                    }else{
                         $status = $this->registerDomain("Domain Registration", $domain->domain_name, "new", "imaginet", "imaginet@2017", $domain->quantity, 1, $registrationAddress,  $domainNameServers);
                    }
                   
              
                    if($status == '1'){

                        $this->OrderDetails->updateAll(['is_registered'=>1, 'status'=>'ACTIVE'],['domain_name' => $domain->domain_name]);

                        //send an email for the domain registeration
                        
                        $email = new Email('default');
                        $email->template('order_mail','default')
                                ->viewVars([
                                    'cart' => $cart, 
                                    'fullname' => $user->first_name . ' ' . $user->last_name,
                                    'order_date'=> Time::now(),
                                    'total'=>  $order->amount,
                                    'discount' => $order->discount,
                                    'failed' => false
                                ])
                                ->emailFormat('html')
                                ->from(['info@imaginet.rw' => 'Imaginet Limited'])
                                ->to($user->email)
                                ->subject('Imaginet Ltd: Your Purchased Domain')
                                ->addTo("info@imaginet.rw")
                                ->send(); 
                    }else{
                        Log::debug("Failed to registered domain ".$domain. " after successful mobile money payment");
                        $this->failedRegistrationNotification($cart, $user->first_name, $user->last_name, $order->amount, $order->discount, $user->email);
                    }
                }

            }else{
                //update status to failed in DB
                 $this->MmTransaction->updateAll(['response_code'=>$response["responsecode"], 'response_msg'=>'FAILED'],['transaction_id'=>$response["transactionid"]]);
                Log::debug("An error occured during payment approval for ".$postData);

            }   
        }
    }

    /**
    * Builds the request xml to be sent to mtn mobile money payment 
    * API
    * @param $phoneNumber, the phone number to be used for the payment
    * @param $subtotal, the total cost of the domain(s) to be purchased
    * @return $xml, the xml data to be sent to the payment API
    */
    private function mobileMoneyRequestXml($phoneNumber, $subtotal){
        $xml = "<thirdpartypayment type=\"thirdpartypaymentrequest\">";
        $xml .= "<vendor>".Configure::read("MobileMoney.VENDOR")."</vendor>";
        $xml .= "<apikey>".Configure::read("MobileMoney.API_KEY")."</apikey>";
        $xml .= "<userid>".Configure::read("MobileMoney.USER_ID")."</userid>";
        $xml .= "<serviceid>".Configure::read("MobileMoney.SERVICE_ID")."</serviceid>";
        $xml .= "<transaction>";
        $xml .= "<aggreg_transaction_id>".date("Ymdhis")."</aggreg_transaction_id>";
        $xml .= "<accountnumber>".$phoneNumber."</accountnumber>";
        $xml .= "<accountref>".$phoneNumber."</accountref>";
        $xml .= "<amount>".$subtotal."</amount>";
        $xml .= "<paymentreason>Imaginet Domain Purchase Payment</paymentreason>";
        $xml .= "</transaction>";
        $xml .= "</thirdpartypayment>";

        return $xml;

    }

    /**
    * Stores a domain purchase order into the database
    * Registers a domain using OpenSRS API
    * Store the details of each domain purchase
    * Stores address and their types for the domain being purchases
    * @param $isMobileMoney, an optional Parameter to indicate whether 
    * payment type is CC or MM
    **/
    public function persistOrderDetails($isMobileMoney = false){
        //get default account address
        $defaultAddress = $this->request->session()->read("address");
        $defaultAddress_id = $this->request->session()->read("address_id");
        //get new addresses (billing, registrant, technical and admin) that customer would have added 
        $newAddresses = $this->request->session()->read("allAddress");

        //Read shopping cart content
        $shoppingCart = $this->readShoppingCart();
        //Read discount if any
        $discount = $this->getPromoCodeDiscount();
        //Get an existing billing address if any
        $existingBillingAddress = $this->request->session()->read("billing");
        //Get an existing registrant address if any
        $existingRegAddress = $this->request->session()->read("registrant");
        //Get an existing admin address if any
        $existingAdminAddress = $this->request->session()->read("admin");
        //Get an existing technical address if any
        $existingTechAddress = $this->request->session()->read("tech");

        //Assume no billing address has been provided and set the billing address
        //to the default address 
        $billing_address_id = $defaultAddress_id;

        $registrationAddress = [];

        //default user account address
        $accountAddress = $this->createAddressArrayFromModel($defaultAddress);
        $accountAddress = $accountAddress["id"];

        /**
        * If no new address are provided, then store the default address as the address of 
        * the four different types as well
        */
        if(count($newAddresses) == 0){

            //save address type for billing if it does not exist
            
            if(is_null($existingBillingAddress)){
                $this->saveAddressType($defaultAddress_id, "billing");
                 $registrationAddress["billing"] = $accountAddress;
            }else{
                 $registrationAddress["billing"] =$this->createAddressArrayFromModel($existingBillingAddress);
            }

            //save address type for admin if it does not exist
            if(is_null($existingAdminAddress)){
                $this->saveAddressType($defaultAddress_id, "admin");
                 $registrationAddress["admin"] = $accountAddress;
            }else{
                 $registrationAddress["admin"] = $this->createAddressArrayFromModel($existingAdminAddress);
            }

            //save address type for tech if it does not exist
            if(is_null($existingTechAddress)){
                $this->saveAddressType($defaultAddress_id, "tech");
                  $registrationAddress["tech"] = $accountAddress;
            }else{
                 $registrationAddress["tech"] = $this->createAddressArrayFromModel($existingTechAddress);
            }

             //save address type for tech if it does not exist
            if(is_null($existingRegAddress)){
                $this->saveAddressType($defaultAddress_id, "registrant");
                 $registrationAddress["registrant"] = $accountAddress;
                
            }else{
                $registrationAddress["registrant"] = $this->createAddressArrayFromModel($existingRegAddress);
            }

            //setting addresses to be sent to OpenSRS for domain registration
             
        }else{

            //check for any new address that might have been entered by
            //customer and save it if it is new or update it if it doesn't exist
            if(array_key_exists("billing", $newAddresses)){
                 $registrationAddress["billing"] = $this->createAddressArrayFromModel($newAddresses["billing"]);
                //save billing address if it is new but edit it if it is not existing
                if(is_null($existingBillingAddress)){
                    $billing = $newAddresses["billing"];
                   
                    if(!$this->Address->save($billing)){
                        Log::debug('An error occured when saving billing Address => '. $billing);
                    }

                    $billing_address_id = $billing->id;
                    $this->saveAddressType($billing->id, "billing");
                   
                }else{
                    //update address if it's already existing
                    $existing = $this->Address->get($existingBillingAddress->id);
                    $addr = $this->createAddressArrayFromModel($newAddresses["billing"]);
                    $existing = $this->Address->patchEntity($existing, $addr);
                    if(!$this->Address->save($existing)){
                        
                        Log::debug('An error occured when updating billing Address => '. $existing);
                    }else{
                        $billing_address_id = $existing->id;
                       
                    }
                }
            }else{
                //if billing address is not provided and it does not exist as well,
                //then save the default address as the billing address
                $this->saveAddressType($defaultAddress_id, "billing");
                $registrationAddress["billing"] = $accountAddress;
                
            }

            if(array_key_exists("admin", $newAddresses)){
                $registrationAddress["admin"] = $this->createAddressArrayFromModel($newAddresses["admin"]);
                if(is_null($existingAdminAddress)){
                    $admin = $newAddresses["admin"];
                    if(!$this->Address->save($admin)){
                        Log::debug('An error occured when saving Admin Address => '. $admin);
                    }
                    //save new addressType
                    $this->saveAddressType($defaultAddress_id, "admin");
                }else{
                    //update address if it's already existing
                    $existing = $this->Address->get($existingAdminAddress->id);
                    $addr = $this->createAddressArrayFromModel($newAddresses["admin"]);
                    $existing = $this->Address->patchEntity($existing, $addr);
                    if(!$this->Address->save($existing)){
                        
                        Log::debug('An error occured when updating admin Address => '. $existing);
                    }
                }
            }else{
                //if billing address is not provided and it does not exist as well,
                //then save the default address as the billing address
                
                $this->saveAddressType($defaultAddress_id, "admin");
                $registrationAddress["admin"] = $accountAddress;
                  
            }

            if(array_key_exists("tech", $newAddresses)){
                 $registrationAddress["tech"] = $this->createAddressArrayFromModel($newAddresses["tech"]);
                if(is_null($existingTechAddress)){
                    $tech = $newAddresses["tech"];
                    if(!$this->Address->save($tech)){
                        Log::debug('An error occured when saving Tech Address => '. $tech);
                    }
                     $this->saveAddressType($defaultAddress_id, "tech");
                }else{
                    //update address if it's already existing
                    $existing = $this->Address->get($existingTechAddress->id);
                    $addr = $this->createAddressArrayFromModel($newAddresses["tech"]);
                    $existing = $this->Address->patchEntity($existing, $addr);
                    if(!$this->Address->save($existing)){
                        
                        Log::debug('An error occured when updating tech Address => '. $existing);
                    }
                }
            }else{
                //if tech address is not provided and it does not exist as well,
                //then save the default address as the tech address
                $this->saveAddressType($defaultAddress_id, "tech");
                $registrationAddress["tech"] = $accountAddress;
                
            }

            if(array_key_exists("registrant", $newAddresses)){
                 $registrationAddress["registrant"] = $this->createAddressArrayFromModel($newAddresses["registrant"]);
                if(is_null($existingRegAddress)){
                    $reg = $newAddresses["registrant"];
                    if(!$this->Address->save($reg)){
                        Log::debug('An error occured when saving Reg Address => '. $reg);
                    }
                     $this->saveAddressType($defaultAddress_id, "registrant");
                }else{
                    //update address if it's already existing
                    $existing = $this->Address->get($existingRegAddress->id);
                    $addr = $this->createAddressArrayFromModel($newAddresses["registrant"]);
                    $existing = $this->Address->patchEntity($existing, $addr);
                    if(!$this->Address->save($existing)){
                        
                        Log::debug('An error occured when updating Reg Address => '. $existing);
                    }
                }
            }else{
                //if billing address is not provided and it does not exist as well,
                //then save the default address as the billing address
                $this->saveAddressType($defaultAddress_id, "registrant");
                $registrationAddress["registrant"] = $accountAddress;
            }
        }

        //save order
        $order = [];
        $order["user_id"] = $this->Auth->user('id');
        $order["address_id"] = $billing_address_id;
        $order["amount"] = $this->calculateDomainTotal();
        $order["discount"] = $discount;
        

        $orderEntity = $this->Orders->newEntity();
        $orderEntity = $this->Orders->patchEntity($orderEntity, $order);

        if(!$this->Orders->save($orderEntity)){
            ;
             Log::debug('An error occured when saving customer order => '. $orderEntity);
    
        }else{
            //save order details here
            $order_details = [];
            foreach($shoppingCart as $cart){

                //get domain NameServers
                 $domainNameServers = $this->readDomainNameServers($cart["name"]);

                 //use imaginet default nameservers if none is provided
                 if(count($domainNameServers) == 0){
                    $servers  = $this->ImaginetNameServers->find('all')->where(['status'=>'ACTIVE'])->limit(2);

                    foreach($servers as $server){
                         array_push($domainNameServers, ["name" => $server->name_server, "ip"=>$server->ip_address]);
                    }
                 }
                
                //register domains here
                if($isMobileMoney){
                    //wait for approval before registering domain
                    //if payment type is mobile money
                    $status = 0;
                    $order_details["status"] = "PENDING";
                   
                }else{
                    //determing which api to use based on domain being registered

                    if($this->isDomainLocal($cart["name"])){
                       $status = $this->registerLocalDomain($cart["name"], $registrationAddress, $domainNameServers);
                    }else{
                       $status = $this->registerDomain("Domain Registration", $cart["name"], "new", "imaginet", "imaginet@2017", $cart["year"], 1, $registrationAddress,  $domainNameServers);  
                    }
                   
                    if($status != 1){
                        array_push($this->failedDomains, $cart["name"]);
                        Log::debug("Failed to register domain ".$cart["name"]. " successfull CC payment");
                    }
                }
                

                $order_details["domain_name"] = $cart["name"];
                $order_details["order_id"] = $orderEntity->id;
                $order_details["quantity"] = $cart["year"];

                //calculate price based on whether hosting is part of this domain purchase or not
                //We subtract the hosting price from the domain price and save the hosting as a 
                //separate tuple
                $order_details["price"] = count($cart["pack"]) > 0 ? ($cart["price"] - $cart['pack']['price']) : $cart["price"];
                $order_details["is_registered"] = $status;

                
                
                
                //set domain expiration date
                $now = Time::now();
                $now->year($now->year + $cart['year']);
                $order_details ["expiration_date"] = $now;

                $orderDetailsEntity = $this->OrderDetails->newEntity();
                $orderDetailsEntity = $this->OrderDetails->patchEntity($orderDetailsEntity, $order_details);
                

                if(!$this->isDomainExisting($cart['name'])){
                    if($this->OrderDetails->save($orderDetailsEntity)){
                        //if hosting is purchased along side this order, then make sure
                        //to commit it as well

                        if(count($cart["pack"]) > 0){
                            $hosting_details["package_name"] = $cart["pack"]["name"];
                            $hosting_details["price"] = $cart["pack"]["price"];
                            $hosting_details["domain_name"] = $cart["name"];
                            $hosting_details["order_id"] = $orderEntity->id;
                            $hosting_details["is_registered"] = '0';
                            $hosting_details ["expiration_date"] = $now;
                            $hosting_details["quantity"] = $cart["year"];

                            $orderDetailsEntity = $this->OrderDetails->newEntity();
                            $orderDetailsEntity = $this->OrderDetails->patchEntity($orderDetailsEntity, $hosting_details);
                           
                            if(!$this->OrderDetails->save($orderDetailsEntity)){
                                Log::debug("Couldn't commit hosting package ".$orderDetailsEntity." after successfully committing domain order");
                            }
                        }
                   
                        foreach($domainNameServers as $server){
                       
                            $data["server_name"] = $server["name"];
                            $data["ip_address"] = $server["ip"];
                            $data["domain_name"] = $cart["name"];

                            $nameServerEntity = $this->NameServers->newEntity();
                            $nameServerEntity = $this->NameServers->patchEntity($nameServerEntity, $data);
                       
                            if(!$this->NameServers->save($nameServerEntity)){
                                Log::debug('An error occured when saving domain name server => '. $nameServerEntity);
                            }

                        }
                    
                    }else{
                   
                        Log::debug('An error occured when saving customer order => '. $orderEntity);
                    }
                }else{
                    //update domain status
                    $this->OrderDetails->updateAll(['is_registered'=>$status],['domain_name'=>$cart["name"]]);
                }
            }
        }
        return $orderEntity->id;
    }   

    /**
    * Save the type of an address 
    * @param $address_id, the id of the address to store its type
    * @param $address_type, a string indicating the type of address
    * can have one of these possible values (tech, admin, registrant, billing)
    **/
    private function saveAddressType($address_id, $address_type){
       
        $address = $this->AddressTypes->newEntity();
        $data["address_id"] = $address_id;
        $data["address_type"] =$address_type;
        $address = $this->AddressTypes->patchEntity($address, $data);
      
        if($this->AddressTypes->save($address)){

        }else{
            debug($address->errors());
            Log::debug('An error occured when saving address type => '. $address);
        }
    }


    /**
    * Makes a remote lookup for domains using
    * OpenSRS API
    * @param $domainToLookUp, $the domain to lookup for
    */
    public function domainLookUp($domainToLookUp){

        //build the lookup xml to be sent to openSRS
        $xml = $this->buildLookUPAPIXML($domainToLookUp);
       
       //execute the command to get response from opensRS
        $response = $this->executeAPICommand($this->buildDataToSend($xml), $xml);
        try{
            //process response text
            $xml = Xml::toArray(Xml::build($response));
        } catch (\Cake\Utility\Exception\XmlException $e) {
            Log::debug("An error occured when trying to parse payment response XML ".$response);
            return 'error';
        }

        $item = $xml['OPS_envelope']['body']['data_block']['dt_assoc']['item'];
       
        //check whether request is successfull
        if(array_key_exists(6, $item)){
            if($item[6]['@'] == '1'){
                if(array_key_exists("@", $item['4']['dt_assoc']['item'])){
                    if($item['4']['dt_assoc']['item']['@'] == 'available'){
                        //domain is availabe for purchase
                        return true;
                    }
                }
                
                return false;
            }else{
                 Log::debug('An error occured when looking up $domainToLookUp => '. $response);
                return 'error';
            }
        }else{
             Log::debug('An error occured when looking up $domainToLookUp => '. $response);
            return 'error';
        }
    }

    /**
    * Builds an xml form domain lookup to be sent to OpenSRS API for
    * information retrieval
    * @param $domain, the domain name to lookup
    * @return the xml to be sent to OpenSRS API
    */
    private function buildLookUpAPIXML($domain){
        
        
        $body = "<item key='domain'>$domain</item>";

        $xml  = \OpenSRSXmlTemplate::xmlTemplate("LOOKUP", $body, "DOMAIN");
       
        return $xml;
    }
    



    private function renewLocalDomain($domain, $exp_date, $duration){
       
        $xml =  \OpenSRSXmlTemplate::buildLocalDomainRenewalXml($domain, $exp_date, $duration);
       
        $response = $this->executeMMPayment(Configure::read("LocalDomain.API_URL"), $xml);
    
        if(strpos($response, "Error 2302") !== false){
            Log::debug("Error occured renewing local domain: ".$domain.' Details: '. $response);
            return '0';
        }else{
            try{
                $response = Xml::toArray(Xml::build($response));
                if($response['xml']['status'] == '1'){

                    $nextExpirationDate = $respons['xml']['expirydate'];
                    $nextExpirationDate = data('Y-m-d H:i:s', strtotime($nextExpirationDate));
                    $this->updateExpiryDate($domain, $nextExpirationDate);
                    //save expiration date to session for display on view
                    $this->request->session()->write("exp_date", $nextExpirationDate);
                    return '1';
                }
                 Log::debug("Error occured renewing local domain: ".$domain.' Details: '. $response);
                return '0';
            } catch (\Cake\Utility\Exception\XmlException $e) {
                 Log::debug("Exception parsing response xml : ". $e);
                 return '0';
            }
        }  
    }


    private function renewDomain($domain, $currentExpirationDate, $period){
        //get domain renewal specific xml
        $renewalXml = \OpenSRSXmlTemplate::buildDomainRenewalXml($domain, $currentExpirationDate, $period);

        //build the xml to be sent to the api
        $xml = \OpenSRSXmlTemplate::xmlTemplate('renew', $renewalXml, "DOMAIN");

        //execute the command to get response from opensRS
        $response = $this->executeAPICommand($this->buildDataToSend($xml), $xml);
        
        Log::debug($response);

        try{
            //process response text
            $xml = Xml::toArray(Xml::build($response));
        } catch (\Cake\Utility\Exception\XmlException $e) {
            Log::debug("An error occured when trying to parse payment response XML ".$response);
            return '0';
        }
      
        $item = $xml['OPS_envelope']['body']['data_block']['dt_assoc']['item'];

       
        if($item[3]['@key'] == 'is_success'){
            //get next expiration date
            $nextExpirationDate = $item[6]['dt_assoc']['item'][4]['@'];

            $this->updateExpiryDate($domain, $nextExpirationDate);
            //save expiration date to session for display on view
            $this->request->session()->write("exp_date", $nextExpirationDate);
            return '1';
        }else if($item[2]['@'] == 'Domain Already Renewed'){
            return 'already renewed';
        }

        return '0';
       
    }


    public function domainRenewal($trans_code = null){
        //return to payment response if trans_code has been provided
        if(!is_null($trans_code)){
            //delete amount that was paid from session
            $this->request->session()->write("paymentAmount", null);
            $this->MscTransaction->updateAll(['order_id'=>$this->request->session()->read("domain_renewal_id")],['id'=>$this->request->session()->read("msc_trans_id")]);
            return $this->redirect(['controller'=>'Users', 'action'=>'renewal-feedback', '3', $trans_code]);
        }

        if($this->request->is('post')){
            $data = $this->request->getData();

            $this->request->session()->write('r_domain', $data['domain_name']);

            $domain = $this->OrderDetails->find("all")
                        ->where(function ($exp, $q) {
                            return $exp->isNull('package_name');
                        })
                        ->andWhere(['domain_name'=>$data['domain_name']])
                        ->first();

            //write the order id of this domain to session this will be used to update
            //the order id of the master card transaction associated with this domain
             $this->request->session()->write("domain_renewal_id", $domain->order_id);
            //check if domain name is local
            if($this->isDomainLocal($data["domain_name"])){
                $exp = date('m/d/Y', strtotime($domain->expiration_date));
                $renewalResults = $this->renewLocalDomain($domain->domain_name, $exp, $data["renewal_years"]);
            }else{
               $renewalResults = $this->renewDomain($domain->domain_name, $domain->expiration_date->year, $data["renewal_years"]); 
            }

            if($renewalResults == 'already renewed'){
                return $this->redirect(['controller'=>'Users', 'action'=>'renewal-feedback', '1']);
            }else if($renewalResults == '0'){

                return $this->redirect(['controller'=>'Users', 'action'=>'renewal-feedback', '2']);
            }else if($renewalResults  == '1'){

               
                //write amount to be paid to session
                $this->request->session()->write("paymentAmount",  $data["price"]);

                //chose payment method based on posted date
                if(isset($data["mm"])){

                    $paymentXml = $this->mobileMoneyRequestXml($data["phone_number"], $data["price"]);
                    $paymentUrl = Configure::read("MobileMoney.PAYMENT_URL");
                    $response = $this->executeMMPayment($paymentUrl, $paymentXml);

                    Log::debug($response);

                    if($response == false){
                        Log::debug("MM Payment Error when paying for renewed domain ".$data["domain_name"] . " on ".date("Y-m-d h:i:s") . " for user ".$this->request->session()->read()["Auth"]["User"]);

                        return $this->redirect(['controller'=>'Users', 'action'=>'renewal-feedback', "failed"]);
                    }

                    try{
                        //process response text
                        $response = Xml::toArray(Xml::build($response));

                        $this->persistMobileTransaction($response['operationResult']['response'], $data['price'], $data['phone_number'], $domain->order_id, "RENEWAL");

                    } catch (\Cake\Utility\Exception\XmlException $e) {
                        Log::debug("An error occured when trying to parse payment response XML ".$response);
                        return $this->redirect(['controller'=>'Users', 'action'=>'renewal-feedback', "failed"]);
                    }
          
                    $code = $response['operationResult']['response']['responsecode'];

                    return $this->redirect(['controller'=>'Users', 'action'=>'renewal-feedback',  $code]);

                }else{
                    //call method to process cc payment and make sure it returns back to this action 
                    //after processing the payment by providing this action's name
                    return $this->redirect(['action'=>'processPayment', "domain-renewal"]);
                }    
            }
        }
    }

    public function paymentError(){

    }

    private function updateExpiryDate($domain, $exp_date){
        //update domain expiration date in database
        $db_domain = $this->OrderDetails->find('all')->where(['domain_name'=>$domain])->first();
        $db_domain = $this->OrderDetails->patchEntity($db_domain, ['expiration_date'=>$exp_date]);
        if(!$this->OrderDetails->save($db_domain)){
            Log::debug("An error occured while update the expiration date of domain ".$domain. " to ".$exp_date);
        }
    }

    public function faq(){

    }

    public function support(){

    }

    public function sendMail(){

         if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $status['status']="fail";   
           
           
            $data = $this->request->getData();
            $senderName = $data['name'];
            $senderEmail = $data['email'];
            $msg = $data['message'];
            $subject = $data['subject'];
           
            $email = new Email('default');
            $email->template('contact_mail','default')
                    ->viewVars([
                            'msg' => $msg, 
                            'senderName' => $senderName
                    ])
                    ->emailFormat('html')
                    ->from([$senderEmail => 'Imaginet Limited'])
                    ->to('info@imaginet.rw')
                    ->subject($data["subject"])
                    ->send(); 
        
            $status['status'] = "success";

            $this->response->body(json_encode($status));
            return $this->response;
        }
    }


    public function mmPayment(){
        $data = $this->request->session()->read("hosting_data");
        $package = $this->Packages->find("all")->where(["name"=>$data["package"]])->first();
        //get hosting payment details from session
        $paymentDetails = $this->request->session()->read('hosting_payment');

        $code = $paymentDetails['payment_data']['responsecode'];
        //persist order
        $order_id = $this->persistHostingPackageOrder($code);
        $this->request->session()->write("order_id", $order_id);

       
        //persist transaction
        $this->persistMobileTransaction($paymentDetails['payment_data'], $paymentDetails['amount'], $paymentDetails['phone'], $order_id, "PURCHASE");
        
       
        $this->set(compact('code', 'package'));
    }


    private function persistMobileTransaction($response, $amount, $phone, $order_id, $payment_type){
        $data["user_id"] = $this->Auth->user('id');
        $data["order_id"] = $order_id;
        $data["transaction_id"] = $response['transactionid'];
        $data["response_code"] = $response['responsecode'];
        $data["account_number"] = $phone;
        $data["response_msg"] = $response['responsemsg'];
        $data["amount"] = $amount;
        $data["payment_type"] = $payment_type;
        $data["session"] = $this->session->id();
        $mmEntity = $this->MmTransaction->newEntity();
        $mmEntity = $this->MmTransaction->patchEntity($mmEntity, $data);


      
        if(!$this->MmTransaction->save($mmEntity)){

            Log::debug("An error occured while saving mobile money transaction ". $mmEntity);
        }
    }


    private function persistHostingPackageOrder($code){
        $data = $this->request->session()->read("hosting_data");
        $package = $this->Packages->find("all")->where(["name"=>$data["package"]])->first();
        //commit package into db
        $odEntity = $this->Orders->newEntity();
        $orderData["user_id"] = $this->Auth->user('id');
        $orderData["amount"] = $package->pricing;
        $orderData["status"] = 'HOSTING'; //depicts hosting order

        $odEntity = $this->Orders->patchEntity($odEntity, $orderData);

        //code of 0 indicates cc payment and 1000 indicates mobile money payment
        if($code == '0' || $code == '1000'){
            if($this->Orders->save($odEntity)){
                //is registered is set to 0 to depict that hosting is not yet done
                $odData["is_registered"] = 0; 
                $odData["order_id"] = $odEntity->id;
                $odData["domain_name"] = $data["domain"];
                $odData["package_name"] = $package->name;
                $odData["quantity"] = 1;

                //set expiration date of hosting
                $now = Time::now();
                $now->modify('+1 year');
      
                $odData["expiration_date"] = $now;
                $odData["price"] = $package->pricing;
                $oddEntity = $this->OrderDetails->newEntity();
                $oddEntity = $this->OrderDetails->patchEntity($oddEntity, $odData);
           
                if($this->OrderDetails->save($oddEntity)){

                }else{
                    Log::debug("Could not save Order Details after successful hosting purchase ".$odData);
                }
            }else{
                Log::debug("Could not save Order after successful hosting purchase ".$orderData);
            }

            return !is_null($odEntity->id) ? $odEntity->id : 0;
        }
    }


    private function sendHostingPurchaseEmail($fullname, $emailAddr, $domain, $package){
        $email = new Email('default');
        $email->template('hosting_mail','default')
            ->viewVars([
                'domain_name' => $domain, 
                'fullname' => $fullname,
                'order_date'=> Time::now(),
                'package' => $package
            ])
            ->emailFormat('html')
            ->from(['info@imaginet.rw' => 'Imaginet Limited'])
            ->to($emailAddr)
            ->addTo("info@imaginet.rw")
            ->subject('Imaginet Ltd: Your Purchased Hosting Package')
            ->send();
    }

    public function getTransactionStatus(){
        if($this->request->is('ajax')){
            $failed_transaction = $this->MmTransaction->find("all")->where(['session'=>$this->session->id()])->last();
            $result["session"] = $this->session->id();
            $result["transaction"] = $failed_transaction;
            $this->response->body(json_encode($result)); 
            return $this->response;
        }
    }

    public function hostingRenewal($responseCode = null){

        //check to see if this is a return call from cc payment processing
        //and do the necessary processing
        if(!is_null($responseCode)){
            if($responseCode == '0'){
                $data = $this->request->session()->read("hosting_details");
                $amount = $this->request->session()->read("paymentAmount");
              
                 if($data['domain'] == 'upgrade'){
                    $order_id = $this->persistHostingDetails($data, $amount);
                    
                    //update the order id of the MSC transaction to the order id of this hosting upgrade
                    $this->MscTransaction->updateAll(['order_id'=>$order_id],['id'=>$this->request->session()->read("msc_trans_id")]);

                }else{
                  //set is registered to 0 to indicate that it's a renewal which is yet to completed by admin
                    $now = Time::now();
                    $year = $hosting->quantity > 1 ? ' years' : ' year';

                    $string = '+'.$data['years'].$year;
                    $now->modify($string);

                    $this->OrderDetails->updateAll(['is_registered'=>'0', 'quantity'=>$data["years"], 'price'=>$amount, 'package_name'=>$data["name"], 'expiration_date'=>$now],['id'=>$data["order_id"]]);

                }

                $fullname = $this->Auth->user('first_name') . ' ' . $this->Auth->user('last_name');
                $package = $this->Packages->get($data["package_id"]);

                $this->sendHostingPurchaseEmail($fullname, $this->Auth->user('email'), $data['domain'], $package);


            }
            return $this->redirect(['controller'=>'Users', 'action'=>'cc-payment-feedback', $responseCode]);
            
        }

        if($this->request->is("post")){

            $data = $this->request->data;
            $amount = $data["package"] * $data["years"];
            

            //check if payment is cc
            if(isset($this->request->data['cc'])){

                $this->request->session()->write("paymentAmount", $amount);
                if($data["domain"] == 'upgrade'){
                    $this->request->session()->write("payment_type", "HOSTING_UPGRADE");
                }else{
                    $this->request->session()->write("payment_type", "HOSTING_RENEWAL");
                }
               
                $this->request->session()->write("hosting_details", $this->request->data);
                return $this->redirect(['action'=>'processPayment', 'hosting-renewal']);
            }

           
            $paymentXml = $this->mobileMoneyRequestXml($data["phone_number"], $amount);
            $paymentUrl = Configure::read("MobileMoney.PAYMENT_URL");

            $response = $this->executeMMPayment($paymentUrl, $paymentXml);
            Log::debug($response);

            if($response == false){
                $code = '000';
            }

            try{
                $response = Xml::toArray(Xml::build($response));
            } catch (\Cake\Utility\Exception\XmlException $e) {
                $code = '000';
            }

            $code = $response['operationResult']['response']['responsecode'];

            //set the payment type based on whether this is a hosting upgrade or a hosting renewal
            $payment_type = $data['domain'] == 'upgrade' ? 'HOSTING_UPGRADE' : 'HOSTING_RENEWAL';

          

            if($code === '1000'){

                if($data['domain'] == 'upgrade'){
                    $order_id = $this->persistHostingDetails($data, $amount);
                }else{
                  //set is registered to 0 to indicate that it's a renewal which is yet to completed by admin
                    $now = Time::now();
                    $year = $hosting->quantity > 1 ? ' years' : ' year';

                    $string = '+'.$data['years'].$year;
                    $now->modify($string);

                  $this->OrderDetails->updateAll(['is_registered'=>'0', 'quantity'=>$data["years"], 'price'=>$amount, 'package_name'=>$data["name"], 'expiration_date'=>$now],['id'=>$data["order_id"]]);

                }
                   
            }
            //order id of 0 means payment was not successfull and therefore hosting details
            //were never saved and therefore payment transaction get saved with an order id of 0
            $order_id = isset($order_id) ? $order_id : 0;

            $this->persistMobileTransaction($response['operationResult']['response'], $amount, $data["phone_number"], $order_id, $payment_type);
            $this->request->session()->write('package_order', $data);

            return $this->redirect(['controller'=>'Users', 'action'=>'hosting-feedback', $code, $data["package_id"]]);

        }
    }    

    public function customerOrder(){
        $s_cart = $this->readShoppingCart();
        $discount = $this->getPromoCodeDiscount();
        $subtotal = $this->calculateDomainTotal();
           
        $this->clearShoppingCart();
        $order_id = $this->request->session()->read("order_id");
        $transaction = $this->MmTransaction->find("all")->where(["order_id"=>$order_id])->first();
       $this->set(compact('transaction', 's_cart', 'discount', 'subtotal'));
    }

    public function hostingDetails(){
        
        $data = $this->request->session()->read("hosting_data");
        
        $package = $this->Packages->find("all")->where(["name"=>$data["package"]])->first();
        $order_id = $this->request->session()->read("order_id");
        $transaction = $this->MmTransaction->find("all")->where(['order_id'=>$order_id])->first();

        $this->set(compact('package', 'transaction'));
    }

    public function failedRegistrationNotification($cart, $fname, $lname, $amount, $discount, $user_email){

        $email = new Email('default');
        $email->template('order_mail','default')
                ->viewVars([
                    'cart' => $cart, 
                    'fullname' => $fname . ' ' . $lname,
                    'order_date'=> Time::now(),
                    'total'=>  $amount,
                    'discount' => $discount,
                    'failed' => true
                    ])
                    ->emailFormat('html')
                    ->from(['info@imaginet.rw' => 'Imaginet Limited'])
                    ->to($user_email)
                    ->addTo('info@imaginet.rw')
                    ->subject('Imaginet Ltd: Your Purchased Domain')
                    ->send();
    
    }

    public function isDomainExisting($domain){

        $domainCount = $this->OrderDetails->find("all")
                        ->where(function ($exp, $q) {
                            return $exp->isNull('package_name');
                        })
                        ->andWhere(['domain_name'=>$domain])
                        ->count();

        if($domainCount > 0)
            return true;

        return false;
    }


    public function persistHostingDetails($data, $amount){
        $address = $this->getAddressType("billing", $this->Auth->user('id'));
                
        $order = [];
        $order["user_id"] = $this->Auth->user('id');
        $order["address_id"] = $address['id'];
        $order["amount"] = $amount;
        $order["discount"] = 0;
        

        $orderEntity = $this->Orders->newEntity();
        $orderEntity = $this->Orders->patchEntity($orderEntity, $order);

        if(!$this->Orders->save($orderEntity)){
            Log::debug('An error occured when saving customer order => '. $orderEntity);
        }else{
                
            $order = $this->OrderDetails->get($data['order_id'])->toArray();
            $order = $this->OrderDetails->newEntity($order);
            $now = Time::now();
            $now->year($now->year + $data['years']);
           
            $order = $this->OrderDetails->patchEntity($order, ['quantity'=>$data["years"], 'price'=>$amount, 'package_name'=>$data["name"], 'is_registered'=>0, 'expiration_date'=>$now, 'domain_name'=>$order['domain_name'], 'order_id'=>$orderEntity->id, 'created'=>Time::now()]);
             
            if(!$this->OrderDetails->save($order)){
                Log::debug('An error occured when saving hosting renewal => '. $order);
               
            }
        }

        return $orderEntity->id;
    }
   
}


