<?php
namespace App\Controller;

use App\Controller\AppController;

class SettingsController extends AppController
{

    public function profiles() 
    {
        $user_types = 'ADMIN';
        $users = $this->Users->find("all")
        ->where(["user_type"=>$user_types]);

        $users = $this->paginate($users);
        $this->set(compact('users'));
        
    }

    public function editProfile($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) 
            {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function tlds($type = null)
    {
        $tlds = $this->TopLevelDomain->find("all");

        if(!is_null($type))
        {
            $tlds = $tlds->where(["type"=>$type]);
        }

        $tlds = $this->paginate($tlds);

        $this->set(compact('tlds'));
    }

    public function editTld($tld_id)
    {
        $tld = $this->TopLevelDomain->get($tld_id);

        if($this->request->is(["post", "put"]))
        {
            $tld = $this->TopLevelDomain->patchEntity($tld, $this->request->getData());

            if($this->TopLevelDomain->save($tld))
            {
                $this->Flash->success('TLD updated successfully');
            }
            else
            {
                $this->Flash->error('Sorry, an error occured when updating TLD');
            }
            return $this->redirect(['action'=>'tlds']);
        }

        $this->set(compact('tld'));
    }

    public function bankDetails()
    {
        $bankdetails = $this->Bankdetails->find('all')->toArray();
        $this->set('bankdetails', $bankdetails);
    }

    public function editBankDetails($bank_detail_id)
    {
        $bankDetail = $this->Bankdetails->get($bank_detail_id);
        if($this->request->is(["post", "put"]))
        {
            $bankDetail = $this->Bankdetails->patchEntity($bankDetail, $this->request->getData());

            if($this->Bankdetails->save($bankDetail))
            {
                $this->Flash->success('Bank details updated successfully');
            }
            else
            {
                $this->Flash->error('An error occured when updating Bank details');
            }
            return $this->redirect(['action'=>'bankDetails']);
        }

        $this->set(compact('bankDetail'));
    }

    public function imaginetAddress()
    {
        $address_details = $this->ImaginetAddress->find('all')->toArray();
        $this->set('address_details', $address_details);
    }

    public function editAddressDetails($address_id)
    {
        $addressDetails = $this->ImaginetAddress->get($address_id);
        if($this->request->is(["post", "put"]))
        {
            $addressDetails = $this->ImaginetAddress->patchEntity($addressDetails, $this->request->getData());

            if($this->ImaginetAddress->save($addressDetails))
            {
                $this->Flash->success('Address details updated successfully');
            }
            else
            {
                $this->Flash->error('An error occured when updating Address details');
            }
            return $this->redirect(['action'=>'imaginetAddress']);
        }

        $this->set(compact('addressDetails'));
    }
}