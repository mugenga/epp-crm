<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\Core\Configure;
use Cake\Log\Log;

use Cake\Utility\Xml;

require_once(ROOT . DS. 'vendor' . DS  . 'open_srs' . DS . 'OpenSRSXmlTemplate.php');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {

        $this->loadModel('OrderDetails');
        $this->loadModel('Users');
        $this->loadModel('Orders');
        $this->loadModel('Packages');
        $this->loadModel('Fine');
        $this->loadModel('Countries');
        $this->loadModel('Address');
        $this->loadModel('AddressTypes');
        $this->loadModel('NameServers');
        $this->loadModel('MmTransaction');
        $this->loadModel('MscTransaction');
        $this->loadModel('ImaginetNameServers');
        $this->loadModel('TopLevelDomain');
        $this->loadModel('DomainYearDiscounts');
        $this->loadModel('Bankdetails');
        $this->loadModel('ImaginetAddress');

        $this->paginate = [
            'limit' => 10
        ];


        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ]
        ]);




        //set up OpenSRS API
        $this->test_mode = true;

        $this->connection_options = [
            'live' => [
                'api_host_port' => 'https://rr-n1-tor.opensrs.net:55443',
                'api_key' => Configure::read('OpenSRS.API_KEY'),
                'reseller_username' => Configure::read('OpenSRS.USERNAME')
            ],
            'test' => [
                'api_host_port' => 'https://horizon.opensrs.net:55443',
                'api_key' => Configure::read('OpenSRS.API_KEY'),
                'reseller_username' => Configure::read('OpenSRS.USERNAME')
            ]
        ];

        if($this->test_mode) {
            $this->connection_details = $this->connection_options['test'];
        } else {
            $this->connection_details = $this->connection_options['live'];
        }


        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');

        
    }

    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->getType(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        // Login Check
        if($this->request->getSession()->read('Auth.User')){
             $this->set('loggedIn', true);   
        } else {
            $this->set('loggedIn', false); 
        }
    }


    public function isAuthorized($user)
    {
        //$action = $this->request->getParam('action');
        

        //admin has access to everything
        if($this->request->getSession()->read()['Auth']['User']['user_type'] == 'ADMIN')
        {
            return true;
        }

        //return parent::isAuthorized($user);
    }

    
    
    /**
    * A method to check whether a domain is local or foreign
    * @param $domain_name, the domain to be checked
    * @return true or false
    **/
    public function isDomainLocal($domain_name){
        $tlds = $this->TopLevelDomain->find("all")->where(["type"=>"local"]);
        $local = false;
       
        foreach($tlds as $tld){
           
            if(strpos($domain_name, $tld->tld) !== false){
                $local = true;
                break;
            }
        }


        return $local;
    }


    protected function localDomainLookUp($domain_name){
        $xml = $this->localDomainLookUpXml($domain_name);
        //print_r("#############################");
        //echo Configure::read("LocalDomain.API_URL");
        //die();
        $response = $this->executeMMPayment(Configure::read("LocalDomain.API_URL"), $xml);
        
       
        if(strpos($response, "free ()") !== false){
            //domain has not been registered and is available for purchase
            return true;
        }

        return false;
       
    }

     /**
    * A method to create an xml to be sent via curl for domain name lookup
    * @param $domain_name, the domain to be looked up for
    **/
    protected function localDomainLookUpXml($domain_name){
         
        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        $xml .= " <xmldata>";
        $xml .= "<checkavailability/>";
        $xml .= "<request>";
        $xml .= "<domainname>". $domain_name. "</domainname>";
        $xml .= "</request>";
        $xml .= "</xmldata>";

        return $xml;
       
    }


       /**
    * This method executes curl commands for mobile money payment
    * and other related api calls
    * @param $paymentUrl, the url to send the curl request
    * @param $paymentXml, the xml to be sent through curl
    **/
    protected function executeMMPayment($paymentUrl, $paymentXml) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$paymentUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paymentXml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

       
        $response = curl_exec ($ch);
       
        if($response === false){
            Log::debug($paymentUrl);
        }

        
        curl_close ($ch);

        return $response;
    }



        /**
    * Makes a remote lookup for domains using
    * OpenSRS API
    * @param $domainToLookUp, $the domain to lookup for
    */
    public function domainLookUp($domainToLookUp){

        //build the lookup xml to be sent to openSRS
        $xml = $this->buildLookUPAPIXML($domainToLookUp);
       
       //execute the command to get response from opensRS
        $response = $this->executeAPICommand($this->buildDataToSend($xml), $xml);
        try{
            //process response text
            $xml = Xml::toArray(Xml::build($response));
        } catch (\Cake\Utility\Exception\XmlException $e) {
            Log::debug("An error occured when trying to parse payment response XML ".$response);
            return 'error';
        }

        $item = $xml['OPS_envelope']['body']['data_block']['dt_assoc']['item'];
       
        //check whether request is successfull
        if(array_key_exists(6, $item)){
            if($item[6]['@'] == '1'){
                if(array_key_exists("@", $item['4']['dt_assoc']['item'])){
                    if($item['4']['dt_assoc']['item']['@'] == 'available'){
                        //domain is availabe for purchase
                        return true;
                    }
                }
                
                return false;
            }else{
                 Log::debug('An error occured when looking up $domainToLookUp => '. $response);
                return 'error';
            }
        }else{
             Log::debug('An error occured when looking up $domainToLookUp => '. $response);
            return 'error';
        }
    }


     /**
    * Executes OpenSRS API Commands
    * @return $response, response from OpenSRS API
    */
    protected function executeAPICommand($data, $xml){

        $ch = curl_init($this->connection_details['api_host_port']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $data);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $response = curl_exec($ch);
       
        return $response;
    }

     /**
    * puts the data to be sent into a format that can be 
    * sent with curl to OpenSRS API
    * @param $xml, the xml data to be sent
    **/
    protected function buildDataToSend($xml){

        return $data = [
            'Content-Type:text/xml',
            'X-Username:' . $this->connection_details['reseller_username'],
            'X-Signature:' . md5(md5($xml . $this->connection_details['api_key']) .  $this->connection_details['api_key']),
        ];
        
    }


     /**
    * Builds an xml form domain lookup to be sent to OpenSRS API for
    * information retrieval
    * @param $domain, the domain name to lookup
    * @return the xml to be sent to OpenSRS API
    */
    private function buildLookUpAPIXML($domain){
        
        
        $body = "<item key='domain'>$domain</item>";

        $xml  = \OpenSRSXmlTemplate::xmlTemplate("LOOKUP", $body, "DOMAIN");
       
        return $xml;
    }
    


}
