<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AddressTypes Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Addresses
 *
 * @method \App\Model\Entity\AddressType get($primaryKey, $options = [])
 * @method \App\Model\Entity\AddressType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AddressType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AddressType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AddressType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AddressType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AddressType findOrCreate($search, callable $callback = null, $options = [])
 */
class AddressTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('address_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Address', [
            'foreignKey' => 'address_id',
            'joinType' => 'INNER'
        ]);

        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('address_type')
            ->requirePresence('address_type', 'create')
            ->notEmpty('address_type');

         $validator
            ->integer('address_id')
            ->requirePresence('address_id', 'create')
            ->notEmpty('address_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['address_id'], 'Address'));

        return $rules;
    }
}