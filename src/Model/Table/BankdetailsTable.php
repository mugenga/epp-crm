<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bankdetails Model
 *
 * @method \App\Model\Entity\Bankdetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bankdetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Bankdetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bankdetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bankdetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bankdetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bankdetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bankdetail findOrCreate($search, callable $callback = null, $options = [])
 */
class BankdetailsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('bankdetails');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('create', 'id');

        $validator
            ->scalar('bank_name')
            ->maxLength('bank_name', 255)
            ->requirePresence('bank_name', 'create')
            ->notEmptyString('bank_name');

        $validator
            ->scalar('acc_number')
            ->maxLength('acc_number', 255)
            ->requirePresence('acc_number', 'create')
            ->notEmptyString('acc_number');

        $validator
            ->scalar('swift_code')
            ->maxLength('swift_code', 255)
            ->requirePresence('swift_code', 'create')
            ->notEmptyString('swift_code');

        $validator
            ->requirePresence('contact', 'create')
            ->notEmptyString('contact');

        $validator
            ->requirePresence('mobile_money', 'create')
            ->notEmptyString('mobile_money');

        return $validator;
    }
}
