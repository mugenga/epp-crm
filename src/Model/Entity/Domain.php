<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Domain Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $contact_id
 * @property string $domain
 * @property string $extension
 * @property \Cake\I18n\FrozenDate $expiry_date
 * @property \Cake\I18n\FrozenTime $insert_time
 * @property \Cake\I18n\FrozenTime $update_time
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Contact $contact
 */
class Domain extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'contact_id' => true,
        'domain' => true,
        'extension' => true,
        'expiry_date' => true,
        'insert_time' => true,
        'update_time' => true,
        'user' => true,
        'contact' => true
    ];
}
