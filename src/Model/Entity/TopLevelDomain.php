<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TopLevelDomain Entity
 *
 * @property int $id
 * @property string $tld
 * @property float $price
 * @property string $type
 */
class TopLevelDomain extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tld' => true,
        'price' => true,
        'f_price' =>true,
        'type' => true
    ];
}
