<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bankdetail Entity
 *
 * @property int $id
 * @property string $bank_name
 * @property string $acc_number
 * @property string $swift_code
 * @property int $contact
 * @property int $mobile_money
 */
class Bankdetail extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'bank_name' => true,
        'acc_number' => true,
        'swift_code' => true,
        'contact' => true,
        'mobile_money' => true
    ];
}
