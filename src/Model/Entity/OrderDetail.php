<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrderDetail Entity
 *
 * @property int $id
 * @property int $order_id
 * @property string $domain_name
 * @property int $quantity
 * @property float $price
 * @property bool $is_registered
 * @property \Cake\I18n\FrozenTime $expiration_date
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Order $order
 */
class OrderDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'order_id' => true,
        'domain_name' => true,
        'quantity' => true,
        'price' => true,
        'is_registered' => true,
        'expiration_date' => true,
        'package_name' => true,
        'created' => true,
        'modified' => true,
        'order' => true
    ];
}