<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Invoice Entity
 *
 * @property int $id
 * @property int $order_details_id
 * @property int $address_id
 * @property string $location
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $due
 * @property bool $paid
 *
 * @property \App\Model\Entity\OrderDetail $order_detail
 * @property \App\Model\Entity\Addres $addres
 */
class Invoice extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'order_details_id' => true,
        'address_id' => true,
        'location' => true,
        'created' => true,
        'due' => true,
        'paid' => true,
        'order_detail' => true,
        'addres' => true
    ];
}
