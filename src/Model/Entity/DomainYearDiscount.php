<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DomainYearDiscount Entity
 *
 * @property int $id
 * @property int $top_level_domain_id
 * @property int $years
 * @property float $price
 *
 * @property \App\Model\Entity\TopLevelDomain $top_level_domain
 */
class DomainYearDiscount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'top_level_domain_id' => true,
        'years' => true,
        'price' => true,
        'top_level_domain' => true
    ];
}
