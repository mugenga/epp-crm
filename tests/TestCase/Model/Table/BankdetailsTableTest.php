<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BankdetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BankdetailsTable Test Case
 */
class BankdetailsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BankdetailsTable
     */
    public $Bankdetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Bankdetails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Bankdetails') ? [] : ['className' => BankdetailsTable::class];
        $this->Bankdetails = TableRegistry::getTableLocator()->get('Bankdetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bankdetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
