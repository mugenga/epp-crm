<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ImaginetAddressTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ImaginetAddressTable Test Case
 */
class ImaginetAddressTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ImaginetAddressTable
     */
    public $ImaginetAddress;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ImaginetAddress'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ImaginetAddress') ? [] : ['className' => ImaginetAddressTable::class];
        $this->ImaginetAddress = TableRegistry::getTableLocator()->get('ImaginetAddress', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ImaginetAddress);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
